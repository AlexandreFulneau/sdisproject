-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 18 juin 2021 à 13:53
-- Version du serveur :  8.0.21
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sdisbdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorierepertoire`
--

DROP TABLE IF EXISTS `categorierepertoire`;
CREATE TABLE IF NOT EXISTS `categorierepertoire` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorierepertoire`
--

INSERT INTO `categorierepertoire` (`id`, `nom`) VALUES
(1, 'Equipe SAN'),
(2, 'Association'),
(3, 'Vétérinaire');

-- --------------------------------------------------------

--
-- Structure de la table `centres`
--

DROP TABLE IF EXISTS `centres`;
CREATE TABLE IF NOT EXISTS `centres` (
  `idcentre` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idcentre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `centres`
--

INSERT INTO `centres` (`idcentre`, `nom`, `email`) VALUES
(1, 'Poitiers', 'compharm@sdis86.net'),
(2, 'Châtellerault', 'compharm@sdis86.net');

-- --------------------------------------------------------

--
-- Structure de la table `commander`
--

DROP TABLE IF EXISTS `commander`;
CREATE TABLE IF NOT EXISTS `commander` (
  `idcommander` int NOT NULL AUTO_INCREMENT,
  `idcommande` int DEFAULT NULL,
  `idinter` int DEFAULT NULL,
  `idmateriel` int DEFAULT NULL,
  `quantite` int NOT NULL,
  PRIMARY KEY (`idcommander`),
  KEY `idcommande` (`idcommande`),
  KEY `idmateriel` (`idmateriel`),
  KEY `idinter_fk` (`idinter`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commander`
--

INSERT INTO `commander` (`idcommander`, `idcommande`, `idinter`, `idmateriel`, `quantite`) VALUES
(1, 1, 1, 2, 1),
(2, 2, 11, 2, 1),
(3, 2, 11, 5, 2),
(5, 4, 14, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `idcommande` int NOT NULL AUTO_INCREMENT,
  `datecommande` date NOT NULL,
  `demandeur` int DEFAULT NULL,
  `centre` int NOT NULL,
  PRIMARY KEY (`idcommande`),
  KEY `demandeur` (`demandeur`),
  KEY `centre` (`centre`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`idcommande`, `datecommande`, `demandeur`, `centre`) VALUES
(1, '2021-04-08', 1, 2),
(2, '2021-04-09', 1, 2),
(3, '2021-04-09', 8, 2),
(4, '2021-04-15', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `compterendu`
--

DROP TABLE IF EXISTS `compterendu`;
CREATE TABLE IF NOT EXISTS `compterendu` (
  `idcompteRendu` int NOT NULL AUTO_INCREMENT,
  `numIntervention` int DEFAULT NULL,
  `auteur` int DEFAULT NULL,
  `recapitulatif` text NOT NULL,
  PRIMARY KEY (`idcompteRendu`),
  UNIQUE KEY `numIntervention_2` (`numIntervention`),
  KEY `auteur_fk` (`auteur`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `compterendu`
--

INSERT INTO `compterendu` (`idcompteRendu`, `numIntervention`, `auteur`, `recapitulatif`) VALUES
(1, 1, 1, 'Capture de 2 chouettes hulotte dans un conduit de cheminée et transportées au centre de soins de la faune sauvage à Colombier'),
(2, 2, 1, 'Évacuation de 14 NAC de l\'habitation enfumée'),
(3, 3, 1, 'Recherche de 3 poneys en forêt de sévéoles, évadés depuis la nuit précédente. 3 poneys retrouvés mort par percutions automobile et 10 autres récupérés par le propriétaire.'),
(4, 4, 1, ''),
(5, 5, 1, ''),
(6, 6, 1, 'Intervention Annulée'),
(7, 7, 1, 'Intervention pour un chat blessé et coincé dans un grillage sans propriétaire sur place. Nous avons dégagé le chat à l\'aide d\'une pince coupante . Celui-ci a pris la fuite dès sa libération . Chat agressif . Pour info les 2 san (carriot /Gruchy) se sont fait mordre à travers les gants , dossiers accident de service effectué .'),
(8, 8, 1, 'Chat tombé sur un palier situé au milieu du conduit de cheminée en pierre, d\'environ 6 mètres de longueur, suite à multiples tentatives par le haut de la cheminée, dégarnissage du mur de cheminée au RDC d\'un appartement, puis capture au moyen du lassaut par le bas de celle-ci. Chat remis au propriétaire sur les lieux.'),
(9, 9, 1, ''),
(10, 10, 1, 'Recherche d\'un sanglier dans un jardin parti avant notre arrivée.'),
(11, 11, 1, 'nklsdigiidjmdiojj'),
(12, 12, 8, 'njsdngdDF,mlqd'),
(13, 13, 1, ''),
(14, 14, 1, ''),
(23, 30, 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `csp`
--

DROP TABLE IF EXISTS `csp`;
CREATE TABLE IF NOT EXISTS `csp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `statut` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `csp`
--

INSERT INTO `csp` (`id`, `nom`, `statut`) VALUES
(1, 'PLB', 'PRO'),
(2, 'PSE', 'PRO'),
(3, 'CHAT', 'PRO');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201116145524', '2020-11-16 14:55:37', 735),
('DoctrineMigrations\\Version20210315140905', '2021-03-15 15:09:35', 2997);

-- --------------------------------------------------------

--
-- Structure de la table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chemin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dossier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `intervenir`
--

DROP TABLE IF EXISTS `intervenir`;
CREATE TABLE IF NOT EXISTS `intervenir` (
  `idintervenir` int NOT NULL AUTO_INCREMENT,
  `idintervention` int DEFAULT NULL,
  `iduser` int DEFAULT NULL,
  `garde` tinyint(1) NOT NULL,
  `astreinte` tinyint(1) NOT NULL,
  `repos` tinyint(1) NOT NULL,
  PRIMARY KEY (`idintervenir`) USING BTREE,
  KEY `idintervention` (`idintervention`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `intervenir`
--

INSERT INTO `intervenir` (`idintervenir`, `idintervention`, `iduser`, `garde`, `astreinte`, `repos`) VALUES
(1, 1, 6, 0, 0, 1),
(2, 1, 7, 1, 0, 0),
(3, 2, 2, 0, 1, 0),
(4, 2, 8, 0, 0, 1),
(5, 2, 9, 0, 1, 0),
(6, 3, 1, 0, 0, 1),
(7, 3, 2, 1, 0, 0),
(8, 4, 10, 1, 0, 0),
(9, 4, 11, 0, 0, 1),
(10, 4, 17, 1, 0, 0),
(11, 5, 13, 0, 1, 0),
(12, 6, 1, 0, 0, 1),
(13, 6, 8, 1, 0, 0),
(14, 7, 6, 0, 1, 0),
(15, 7, 11, 1, 0, 0),
(16, 8, 7, 0, 1, 0),
(17, 9, 11, 0, 1, 0),
(18, 9, 10, 0, 1, 0),
(19, 10, 10, 0, 0, 1),
(20, 10, 7, 1, 0, 0),
(21, 11, 1, 1, 0, 0),
(22, 11, 2, 0, 0, 1),
(23, 12, 8, 0, 1, 0),
(24, 13, 1, 0, 1, 0),
(25, 14, 1, 1, 0, 0),
(50, 2, 18, 1, 0, 0),
(51, 11, 3, 0, 1, 0),
(52, 2, 15, 0, 0, 1),
(53, 30, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `interventions`
--

DROP TABLE IF EXISTS `interventions`;
CREATE TABLE IF NOT EXISTS `interventions` (
  `id_interventions` int NOT NULL AUTO_INCREMENT,
  `num_intervention` int DEFAULT NULL,
  `date` date NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `ville` int DEFAULT NULL,
  `motif` int DEFAULT NULL,
  PRIMARY KEY (`id_interventions`),
  UNIQUE KEY `num_intervention` (`num_intervention`),
  KEY `ville` (`ville`),
  KEY `motif` (`motif`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `interventions`
--

INSERT INTO `interventions` (`id_interventions`, `num_intervention`, `date`, `adresse`, `ville`, `motif`) VALUES
(1, 1034, '2021-01-21', '0000', 184, 1),
(2, 1110, '2021-01-22', '0000', 40, 6),
(3, 1817, '2021-02-05', '0000', 101, 3),
(4, 1866, '2021-01-21', '0000', 112, 1),
(5, 2301, '2021-02-13', '0000', 1, 4),
(6, 2312, '2021-02-14', '0000', 11, 1),
(7, 3993, '2021-03-16', '0000', 153, 1),
(8, 4717, '2021-03-30', '0000', 61, 1),
(9, 4780, '2021-03-31', '0000', 115, 1),
(10, 5213, '2021-04-08', '0000', 181, 1),
(11, 1, '2021-01-21', '000', 181, 1),
(12, 2, '2021-01-21', '0000', 10, 6),
(13, 5, '2021-01-21', 'al', NULL, NULL),
(14, 8, '2021-01-21', '0000', 181, 4),
(30, 2147483647, '2022-02-11', 'dadazdadada', 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

DROP TABLE IF EXISTS `materiel`;
CREATE TABLE IF NOT EXISTS `materiel` (
  `idmateriel` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `stock` int NOT NULL,
  `type` int NOT NULL,
  PRIMARY KEY (`idmateriel`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `materiel`
--

INSERT INTO `materiel` (`idmateriel`, `nom`, `stock`, `type`) VALUES
(1, 'Combinaison SAN', 19, 2),
(2, 'Pince à tiques 2', 1, 3),
(4, 'Casque bleu SAN', 18, 2),
(5, 'Gants souple', 21, 2),
(6, 'Gants cuir', 21, 2),
(7, 'Sac de transport', 19, 2),
(9, 'Ciseau de type Jesco', 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `motif`
--

DROP TABLE IF EXISTS `motif`;
CREATE TABLE IF NOT EXISTS `motif` (
  `idmotif` int NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`idmotif`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `motif`
--

INSERT INTO `motif` (`idmotif`, `libelle`) VALUES
(1, 'Intervention pour animaux bléssés ou en difficultés'),
(2, 'Intervention pour animaux exotiques'),
(3, 'Autres interventions animaux'),
(4, 'Animal menaçant l\'intervention des SP'),
(5, 'Départ réflexe malaise sur VP ou dans LP'),
(6, 'Feu d\'appartement ou local d\'habitation collective'),
(7, 'Reconnaissance'),
(8, 'Personne ne répondant pas aux appels'),
(9, 'TS à domicile'),
(10, 'Ouverture de porte'),
(11, 'Feu de bâtiment agricole'),
(12, 'AVP avec PL ou Bus'),
(13, 'Ouverture de porte/personne en danger\r\n'),
(14, 'Départ réflexe traumatisme à domicile \r\n'),
(15, 'Victime non protegée ou danger persistant trauma sur VP ou LP\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `idnews` int NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `auteur` int NOT NULL,
  `date` date NOT NULL,
  `contenu` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`idnews`),
  KEY `auteur` (`auteur`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`idnews`, `titre`, `auteur`, `date`, `contenu`) VALUES
(6, '1', 1, '2021-06-16', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeee'),
(7, '2', 1, '2021-06-16', 'aaaaaaaaa aaaaaaaa \r\n aaaaaaaa  aaaaaaaaaaa aaaaaa aaaaaaaaaa aaaaaaaa aaaaaaaa aaaaaaaaa aaa aaaaaaaaa aaaaaaa aaaa aaaaaaa aaaaa aaaaaaaaaaa aaaaaaaa aaaaaaaaa aaaaaaaa aaaaaaa a a aaa aaaaaaa aaaaaaaaaaaa aa a aaaa aaaaaaa aaaaaaaaaaa aaaaaaa aaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaa \r\n aaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaa aaaaaaaaa aaaa aaaaaaaaaaaaaa aa aaaa aaaaaa aaaaaaaaaaa aaaaaaa aaaaaaa aaaaaaaaaaa aaaaaaaaa \r\n aaaaaaaaa aaaaaaa aaaa \r\n\r\naaaaaaaaaaaaaa'),
(8, '3', 1, '2021-06-17', 'ezeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'),
(9, '4', 1, '2021-06-17', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'),
(10, '5', 1, '2021-06-17', 'eeeeeeeeeeeeeeeeeeeeeeee'),
(11, '6', 1, '2021-06-17', 'eeeeeeeeeeeeee'),
(12, 'test', 1, '2021-06-18', 'rereree'),
(13, 'tessssssssssssssssssssssssssssssssssssssssss2', 1, '2021-06-18', 'ttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt'),
(14, 'fef', 1, '2021-06-18', 'efef');

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `idphoto` int NOT NULL AUTO_INCREMENT,
  `idintervention` int DEFAULT NULL,
  `date` date DEFAULT NULL,
  `chemin` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `dossier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idphoto`),
  KEY `idintervention_fk` (`idintervention`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `posseder`
--

DROP TABLE IF EXISTS `posseder`;
CREATE TABLE IF NOT EXISTS `posseder` (
  `idutiliser` int NOT NULL AUTO_INCREMENT,
  `idmateriel` int DEFAULT NULL,
  `idpersonnel` int DEFAULT NULL,
  `numMateriel` int DEFAULT NULL,
  `taille` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idutiliser`),
  KEY `idmateriel` (`idmateriel`),
  KEY `idpersonnel` (`idpersonnel`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `posseder`
--

INSERT INTO `posseder` (`idutiliser`, `idmateriel`, `idpersonnel`, `numMateriel`, `taille`) VALUES
(1, 1, 1, NULL, 'L'),
(2, 4, 1, 2822509, 'Unique'),
(6, 1, 2, NULL, 'M'),
(7, 4, 2, 2822504, 'UNIQUE'),
(8, 5, 2, NULL, '8'),
(9, 6, 2, NULL, '8'),
(10, 7, 2, NULL, 'UNIQUE'),
(11, 1, 3, NULL, 'XL'),
(12, 4, 3, 2822514, 'UNIQUE'),
(13, 5, 3, NULL, '9'),
(14, 6, 3, NULL, '9'),
(15, 7, 3, NULL, 'UNIQUE'),
(23, 9, 1, NULL, NULL),
(24, 1, 1, NULL, NULL),
(25, 9, 1, NULL, NULL),
(26, 4, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `regarder`
--

DROP TABLE IF EXISTS `regarder`;
CREATE TABLE IF NOT EXISTS `regarder` (
  `idregarder` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `news` int NOT NULL,
  `vue` tinyint(1) NOT NULL,
  PRIMARY KEY (`idregarder`),
  KEY `new` (`news`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `regarder`
--

INSERT INTO `regarder` (`idregarder`, `user`, `news`, `vue`) VALUES
(1, 1, 6, 1),
(2, 2, 6, 0),
(3, 3, 6, 0),
(4, 4, 6, 0),
(5, 5, 6, 0),
(6, 6, 6, 0),
(7, 7, 6, 0),
(8, 8, 6, 0),
(9, 9, 6, 0),
(10, 10, 6, 0),
(11, 11, 6, 0),
(12, 12, 6, 0),
(13, 13, 6, 0),
(14, 14, 6, 0),
(15, 15, 6, 0),
(16, 16, 6, 0),
(17, 17, 6, 0),
(18, 18, 6, 0),
(19, 19, 6, 1),
(20, 1, 7, 1),
(21, 2, 7, 0),
(22, 3, 7, 0),
(23, 4, 7, 0),
(24, 5, 7, 0),
(25, 6, 7, 0),
(26, 7, 7, 0),
(27, 8, 7, 0),
(28, 9, 7, 0),
(29, 10, 7, 0),
(30, 11, 7, 0),
(31, 12, 7, 0),
(32, 13, 7, 0),
(33, 14, 7, 0),
(34, 15, 7, 0),
(35, 16, 7, 0),
(36, 17, 7, 0),
(37, 18, 7, 0),
(38, 19, 7, 1),
(39, 1, 8, 1),
(40, 2, 8, 0),
(41, 3, 8, 0),
(42, 4, 8, 0),
(43, 5, 8, 0),
(44, 6, 8, 0),
(45, 7, 8, 0),
(46, 8, 8, 0),
(47, 9, 8, 0),
(48, 10, 8, 0),
(49, 11, 8, 0),
(50, 12, 8, 0),
(51, 13, 8, 0),
(52, 14, 8, 0),
(53, 15, 8, 0),
(54, 16, 8, 0),
(55, 17, 8, 0),
(56, 18, 8, 0),
(57, 19, 8, 0),
(58, 1, 9, 1),
(59, 2, 9, 0),
(60, 3, 9, 0),
(61, 4, 9, 0),
(62, 5, 9, 0),
(63, 6, 9, 0),
(64, 7, 9, 0),
(65, 8, 9, 0),
(66, 9, 9, 0),
(67, 10, 9, 0),
(68, 11, 9, 0),
(69, 12, 9, 0),
(70, 13, 9, 0),
(71, 14, 9, 0),
(72, 15, 9, 0),
(73, 16, 9, 0),
(74, 17, 9, 0),
(75, 18, 9, 0),
(76, 19, 9, 0),
(77, 1, 10, 1),
(78, 2, 10, 0),
(79, 3, 10, 0),
(80, 4, 10, 0),
(81, 5, 10, 0),
(82, 6, 10, 0),
(83, 7, 10, 0),
(84, 8, 10, 0),
(85, 9, 10, 0),
(86, 10, 10, 0),
(87, 11, 10, 0),
(88, 12, 10, 0),
(89, 13, 10, 0),
(90, 14, 10, 0),
(91, 15, 10, 0),
(92, 16, 10, 0),
(93, 17, 10, 0),
(94, 18, 10, 0),
(95, 19, 10, 0),
(96, 1, 11, 1),
(97, 2, 11, 0),
(98, 3, 11, 0),
(99, 4, 11, 0),
(100, 5, 11, 0),
(101, 6, 11, 0),
(102, 7, 11, 0),
(103, 8, 11, 0),
(104, 9, 11, 0),
(105, 10, 11, 0),
(106, 11, 11, 0),
(107, 12, 11, 0),
(108, 13, 11, 0),
(109, 14, 11, 0),
(110, 15, 11, 0),
(111, 16, 11, 0),
(112, 17, 11, 0),
(113, 18, 11, 0),
(114, 19, 11, 0),
(115, 1, 12, 1),
(116, 2, 12, 0),
(117, 3, 12, 0),
(118, 4, 12, 0),
(119, 5, 12, 0),
(120, 6, 12, 0),
(121, 7, 12, 0),
(122, 8, 12, 0),
(123, 9, 12, 0),
(124, 10, 12, 0),
(125, 11, 12, 0),
(126, 12, 12, 0),
(127, 13, 12, 0),
(128, 14, 12, 0),
(129, 15, 12, 0),
(130, 16, 12, 0),
(131, 17, 12, 0),
(132, 18, 12, 0),
(133, 19, 12, 0),
(134, 1, 13, 1),
(135, 2, 13, 0),
(136, 3, 13, 0),
(137, 4, 13, 0),
(138, 5, 13, 0),
(139, 6, 13, 0),
(140, 7, 13, 0),
(141, 8, 13, 0),
(142, 9, 13, 0),
(143, 10, 13, 0),
(144, 11, 13, 0),
(145, 12, 13, 0),
(146, 13, 13, 0),
(147, 14, 13, 0),
(148, 15, 13, 0),
(149, 16, 13, 0),
(150, 17, 13, 0),
(151, 18, 13, 0),
(152, 19, 13, 0),
(153, 1, 14, 1),
(154, 2, 14, 0),
(155, 3, 14, 0),
(156, 4, 14, 0),
(157, 5, 14, 0),
(158, 6, 14, 0),
(159, 7, 14, 0),
(160, 8, 14, 0),
(161, 9, 14, 0),
(162, 10, 14, 0),
(163, 11, 14, 0),
(164, 12, 14, 0),
(165, 13, 14, 0),
(166, 14, 14, 0),
(167, 15, 14, 0),
(168, 16, 14, 0),
(169, 17, 14, 0),
(170, 18, 14, 0),
(171, 19, 14, 0);

-- --------------------------------------------------------

--
-- Structure de la table `repertoire`
--

DROP TABLE IF EXISTS `repertoire`;
CREATE TABLE IF NOT EXISTS `repertoire` (
  `idrepertoire` int NOT NULL AUTO_INCREMENT,
  `grade` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `TelephoneFixe` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `Specialite` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `commentaires` text,
  `categorie` int DEFAULT NULL,
  `adressePrincipale` varchar(255) DEFAULT NULL,
  `villePrincipale` int DEFAULT NULL,
  `adresseSecondaire` varchar(255) DEFAULT NULL,
  `villeSecondaire` int DEFAULT NULL,
  PRIMARY KEY (`idrepertoire`),
  KEY `villePrincipale_fk` (`villePrincipale`),
  KEY `villeSecondaire_fk` (`villeSecondaire`),
  KEY `categorie` (`categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `repertoire`
--

INSERT INTO `repertoire` (`idrepertoire`, `grade`, `nom`, `prenom`, `tel`, `TelephoneFixe`, `email`, `Specialite`, `site`, `commentaires`, `categorie`, `adressePrincipale`, `villePrincipale`, `adresseSecondaire`, `villeSecondaire`) VALUES
(1, 'Lt', 'CHAILLOUX', 'Laurent', '0625625875', NULL, 'laurent.chailloux@sdis-vendee.fr', NULL, 'SDIS85', 'CTD SAN85', 1, NULL, NULL, NULL, NULL),
(2, 'A/C', 'ROBERT', 'Stéphane', '0616511333', NULL, 'anim79sp@gmail.com', NULL, 'SDIS79', 'CTD SAN79', 1, NULL, NULL, NULL, NULL),
(3, 'Mr', 'THURIET', 'Simon', '0609497219', '0549464748', 'volenscene@wanadoo.com', 'Rapaces/oiseaux', 'Les géants du ciel', 'Gérant', 1, NULL, NULL, NULL, NULL),
(4, 'Melle', 'MESNARD', 'Amélie', '0606848802', '0549464748', 'volenscene@wanadoo.com', 'Rapaces/oiseaux', 'Les géants du ciel', 'Assistante de direction', 1, NULL, NULL, NULL, NULL),
(5, 'Mr', 'CHAUVEAU', 'Sébastien', '0627025622', '0549520150', 'Sebastien.Chauveau@oncfs.gouv.fr', NULL, 'ONCFS Direction Vienne 86', 'Chef de service DR Vienne ONCFS', 1, NULL, NULL, NULL, NULL),
(6, 'Mme', 'BOURDEAU', 'Lydia', '0609852798', '0620505934', 'csfsp86@gmail.com', NULL, 'Centre de Soins de la Faune Sauvage 86', '*Numéro de la maman (fixe)', 1, NULL, 74, NULL, NULL),
(7, 'Melle', 'BOSC', 'Stéphanie', '649321688', NULL, 'stephanie.bosc@grandpoitiers.fr', 'Véto', 'Vétérinaire bois de stpierre Smarves', NULL, 1, NULL, NULL, NULL, NULL),
(8, 'ASSO', NULL, NULL, NULL, '549885522', 'vienne@lpo.fr', 'ASSO', 'LPO Vienne', '25 rue victor grignard Poitiers', 1, NULL, NULL, NULL, NULL),
(9, 'ASSO', NULL, NULL, NULL, '549889904', 'contact@vienne-nature.fr', 'ASSO', 'Vienne nature', '14 rue jean moulin Fontaine le comte', 1, NULL, NULL, NULL, NULL),
(10, 'ASSO', NULL, NULL, NULL, '549889457', 'contact@spa-poitiers.fr', 'ASSO', 'SPA Poitiers', '\"LaFolie\" la grange des prés rue de la poupinière Poitiers', 1, NULL, NULL, NULL, NULL),
(11, 'ASSO', NULL, NULL, NULL, '549216111', NULL, 'ASSO', 'ASA Chatellerault', '\"Valette\" 1 rue charles Darwin chatellerault', 1, NULL, NULL, NULL, NULL),
(12, 'Ville', NULL, NULL, NULL, '549473253', 'fourrierespa86@voila.fr', NULL, 'Fourriere grand poitiers', '\"LaFolie\" la grange des prés rue de la poupinière Poitiers', 1, NULL, NULL, NULL, NULL),
(13, 'Mr', 'MAGAUD', 'Geoffroy', NULL, '549211116', 'geoffroy.magaud@mfr.asso.fr', 'équins', 'MFR fonteveille', 'DIRECTEUR 224 route de richelieu Chatellerault', 1, NULL, NULL, NULL, NULL),
(14, 'Melle', 'MARQUET', 'Bertille', '0661489504', NULL, 'vet@la-vallee-des-singes.fr', 'Vétérinaire', 'La Vallée des singes', 'Vétérinaire LD Legureau Romagne', 1, 'LD Legureau', 197, NULL, NULL),
(15, 'cdt', 'BIDARD', 'Marc', '0611175147', '0130077000', NULL, NULL, 'SDIS 78', 'CTD SAN 78', 1, NULL, NULL, NULL, NULL),
(16, 'S/C', 'DELZENNE', 'Guillaume', '0675657332', NULL, 'guillaume.delzenne@orange.fr', 'CTD', 'SDIS77', 'Responsable matériel SAN 77', 1, NULL, NULL, NULL, NULL),
(17, 'A/C', 'DEULEUZE', 'Céline', '0638238316', NULL, 'deleuze.celine@sdis77.fr', 'CTD', 'SDIS77', 'CTD SAN 77', 1, NULL, NULL, NULL, NULL),
(18, 'Melle', 'HUBLER', 'Charlotte', '0750357974', NULL, 'repticf@gmail.com', 'reptiles', 'Repti conseils & formation', 'SMS \"URGENT POMPIER\" pour identification reptiles  07 50 35 79 74', 1, NULL, NULL, NULL, NULL),
(19, 'Mme', 'LEBEAU', 'Élise', '06 87 91 05 57', '', 'vetpleuel@orange.fr', NULL, 'Vétérinaire  Pleumartin', NULL, 1, NULL, NULL, NULL, NULL),
(20, 'Lt', 'LYVER', 'Mickaël', '0631130866', NULL, 'lyver.m@sdis16.fr', 'CTD SAN', 'SDIS16', 'CTD GSA 16', 1, NULL, NULL, NULL, NULL),
(21, 'Melle', 'HUBLER', 'Charlotte', '0750357974 0661985872', NULL, 'repticf@gmail.com', 'reptiles', 'Repti conseils & formation', 'SMS \"URGENT POMPIER\" pour identification reptiles  07 50 35 79 74', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `typemateriel`
--

DROP TABLE IF EXISTS `typemateriel`;
CREATE TABLE IF NOT EXISTS `typemateriel` (
  `idtypemateriel` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`idtypemateriel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `typemateriel`
--

INSERT INTO `typemateriel` (`idtypemateriel`, `nom`) VALUES
(1, 'Consommable'),
(2, 'E.P.I'),
(3, 'Matériels Opérationnels');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prenom` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email` varchar(180) NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '$2y$10$mmC6gzjR5YrCZcvMyBlT4u7J5l/j0sut85KkGDnD2FEAHTGbDSdSe',
  `telephone` varchar(255) DEFAULT NULL,
  `fonction` varchar(255) NOT NULL,
  `id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `prenom`, `nom`, `email`, `roles`, `password`, `telephone`, `fonction`, `id`) VALUES
(1, 'Alain', 'LAINÉ', 'alain.laine@sdis86.net', '[\"ROLE_ADMIN\"]', '$2y$13$VaqYeRZvNsmZbphHbHmC5Ovr.Qx30BHDNtA/9BIDfla3w8cmYc.jm', '0679063291', 'conseiller technique departemental', 1),
(2, 'Jean-philippe', 'DESPLOBAIN', 'jean-philippe.desplobain@sdis86.net', '[\"ROLE_ADMIN\"]', '$2y$13$8ar52Rk348fbqsnUVTHxp.Bg.rk2dBJiVxdzC1lFvIDezCEPMcGcu', '0609994188', 'conseiller technique adjoint', 1),
(3, 'Anthony', 'CAVILLIER', 'anthony.cavillier@sdis86.net', '[\"ROLE_CHEF\"]', '$2y$13$qL4nFyfVGB/YoPevmhjpGO.1yT0vlwDiDIjXxGc54bAR1Me55zmd.', '0646386480', 'chef d’unite', 1),
(4, 'Pascal', 'DAVID', 'pascal.david@sdis86.net', '[\"ROLE_CHEF\"]', '$2y$13$9VBHE3mvgOndHCT4C.hGDexZJc0izFpoxjQYWK/g14K9waU8fLFqW', '0676094047', 'chef d’unite', 1),
(5, 'Christopher', 'AVRILLON', 'christopher.avrillon@sdis86.net', '[\"ROLE_CHEF\"]', '$2y$13$Aq12lYaDN1WWxuV4AbV5UeFvo6TRCBknxu/y13vQEFcCwI1SwrIY.', '0684296177', 'chef d’unite', 1),
(6, 'Anthony', 'CARRIOT', 'anthony.carriot@sdis86.net', '[\"ROLE_CHEF\"]', '$2y$13$ec0jvdVJ6Z05AKf.d/CR3uQuL3fPRz8BkayK0ss4ueDwElHYOmspW', '0624501994', 'chef d’unite', 1),
(7, 'Damien', 'MARCHAND', 'damien.marchand@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$YefZwhp0FgORglPuM/Z1wugw.deOkRrOZOCyzCZG8e99sOeU/6/0m', '0667164161', 'equipier', 1),
(8, 'Franck', 'DAIGRE', 'franck.daigre@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$dVscd7dkvk7juJNy4wpsW.wsbi4L/PNrASODquaoYFCWK7pEpl7g.', '0651533046', 'equipier', 1),
(9, 'Romain', 'MASSONNET', 'romain.massonnet@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$DdURdf8CfjYhu9YsF/AcceUdXmPB.t0Pp4y8mQux7rG3Dp2kbGmk2', '0658450310', 'equipier', 1),
(10, 'Hugues', 'BILLY', 'hugues.billy@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$FI5njWrclUnv/M8H5gv0K.EKAQYGVvh2M4qNVGfAJqPXoPPVq.5zi', '0677338575', 'equipier', 1),
(11, 'Samuel', 'GRUCHY', 'samuel.gruchy@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$RFpjKjxMHTAWNKLayyS3CuRBeudZpPwk3TEJCSoQMURSuWHWByA2m', '0685638967', 'equipier', 1),
(12, 'Clément', 'TEXIER', 'clement.texier@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$KsKki0mq.qZU3Nmk0RmcqOnjYk0nPfRqRV/doJdVal.mvajVKMwQS', '0645137661', 'equipier', 1),
(13, 'Kévin', 'GUILLEBAULT', 'kevin.guillebault@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$bQ4wv8P0q0xYYBVXvzq8FuyXvu2HId35VRyY5PvGZQZd2ln3RkpJi', '0698321152', 'equipier', 1),
(14, 'Jocelyn', 'ADAM', 'jocelin.adam@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$Lde/4CEd4DeaxuGuWk3rzuteaEavkv6St3ZVvbomPTD9afaHf3OqS', '0675689340', 'equipier', 1),
(15, 'Davy', 'BONNEAU', 'davy.bonneau@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$5ncwci24K3D33T9HkhKvTupHEVAgTRR7OiFyAVpgaywmhzT9X5.Ki', '0682191543', 'equipier', 1),
(16, 'Michel', 'LAFEUILLE', 'michel.lafeuille@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$2YLoKOBDpw7sAyxXgAkM4etEra5J4qHRX5IXImGaE1ZDtrTX3MCj.', '0668569667', 'equipier', 1),
(17, 'Benjamin', 'MORAUD', 'benjamin.moraud@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$A6rgVEUThqq8ml/CpbvqA.Ro24Vsh4F0aMHuW/xQZUM.CAWWsF/H.', '0625743936', 'equipier', 1),
(18, 'Sébastien', 'STEINMETZ', 'sebastien.steinmetz@sdis86.net', '[\"ROLE_EQUIPIER\"]', '$2y$13$ugDxYo5gGxlD8rnkE9MBCOjovCpGTyapliw7d6W8/179CHe350MXO', '0687975962', 'equipier', 1),
(19, 'test', 'test', 'equipier.equipier@equipier.fr', '[\"ROLE_EQUIPIER\"]', '$2y$10$1uzGF5yCFE9wz30kFuczcuDTezenvPAlYeVaqV92sfKfdrNe13Upm', '0523651254', 'ah', 1);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `idville` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`idville`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`idville`, `nom`) VALUES
(1, 'Antigny'),
(2, 'Angliers'),
(3, 'Aslonnes'),
(4, 'Angles-sur-l’Anglin'),
(5, 'Adriers'),
(6, 'Archigny'),
(7, 'Antran'),
(8, 'Amberre'),
(9, 'Anché'),
(10, 'Arçay'),
(11, 'Asnois'),
(12, 'Aulnay'),
(13, 'Availles-en-Châtellerault'),
(14, 'Availles-Limouzine'),
(15, 'Avanton'),
(16, 'Ayron'),
(17, 'Basses'),
(18, 'Beaumont Saint-Cyr'),
(19, 'Bellefonds'),
(20, 'Benassay'),
(21, 'Berrie'),
(22, 'Berthegon'),
(23, 'Béruges'),
(24, 'Béthines'),
(25, 'Beuxes'),
(26, 'Biard'),
(27, 'Bignoux'),
(28, 'Blanzay'),
(29, 'Bonnes'),
(30, 'Bonneuil-Matours'),
(31, 'Bouresse'),
(32, 'Bourg-Archambault'),
(33, 'Bournand'),
(34, 'Brigueil-le-Chantre'),
(35, 'Brion'),
(36, 'Brux'),
(37, 'La Bussière'),
(38, 'Buxerolles'),
(39, 'Buxeuil'),
(40, 'Ceaux-en-Couhé'),
(41, 'Ceaux-en-Loudun'),
(42, 'Celle-Lévescault'),
(43, 'Cenon-sur-Vienne'),
(44, 'Cernay'),
(45, 'Chabournay'),
(46, 'Chalais'),
(47, 'Chalandray'),
(48, 'Champagné-le-Sec'),
(49, 'Champagné-Saint-Hilaire'),
(50, 'Champigny en Rochereau'),
(51, 'Champniers'),
(52, 'La Chapelle-Béton'),
(53, 'La Chapelle-Montreuil'),
(54, 'La Chapelle-Moulière'),
(55, 'Chapelle-Viviers'),
(56, 'Charroux'),
(57, 'Chasseneuil-du-Poitou'),
(58, 'Chatain'),
(59, 'Château-Garnier'),
(60, 'Château-Larcher'),
(61, 'Châtellerault'),
(62, 'Châtillon'),
(63, 'Chaunay'),
(64, 'La Chaussée'),
(65, 'Chauvigny'),
(66, 'Chenevelles'),
(67, 'Cherves'),
(68, 'Chiré-en-Montreuil'),
(69, 'Chouppes'),
(70, 'Cissé'),
(71, 'Civaux'),
(72, 'Civray'),
(73, 'Cloué'),
(74, 'Colombiers'),
(75, 'Couhé'),
(76, 'Coulombiers'),
(77, 'Coulonges'),
(78, 'Coussay'),
(79, 'Coussay-les-Bois'),
(80, 'Craon'),
(81, 'Croutelle'),
(82, 'Cuhon'),
(83, 'Curçay-sur-Dive'),
(84, 'Curzay-sur-Vonne'),
(85, 'Dangé-Saint-Romain'),
(86, 'Dercé'),
(87, 'Dienné'),
(88, 'Dissay'),
(89, 'Doussay'),
(90, 'La Ferrière-Airoux'),
(91, 'Fleix'),
(92, 'Fleuré'),
(93, 'Fontaine-le-Comte'),
(94, 'Frozes'),
(95, 'Gençay'),
(96, 'Genouillé'),
(97, 'Gizay'),
(98, 'Glénouze'),
(99, 'Gouex'),
(100, 'La Grimaudière'),
(101, 'Guesnes'),
(102, 'Haims'),
(103, 'Ingrandes'),
(104, 'L\'Isle-Jourdain'),
(105, 'Iteuil'),
(106, 'Jardres'),
(107, 'Jaunay-Marigny'),
(108, 'Jazeneuil'),
(109, 'Jouhet'),
(110, 'Journet'),
(111, 'Joussé'),
(112, 'Lathus-Saint-Rémy'),
(113, 'Latillé'),
(114, 'Lauthiers'),
(115, 'Lavausseau'),
(116, 'Lavoux'),
(117, 'Leigné-les-Bois'),
(118, 'Leignes-sur-Fontaine'),
(119, 'Leigné-sur-Usseau'),
(120, 'Lencloître'),
(121, 'Lésigny'),
(122, 'Leugny'),
(123, 'Lhommaizé'),
(124, 'Liglet'),
(125, 'Ligugé'),
(126, 'Linazay'),
(127, 'Liniers'),
(128, 'Lizant'),
(129, 'Loudun'),
(130, 'Luchapt'),
(131, 'Lusignan'),
(132, 'Lussac-les-Châteaux'),
(133, 'Magné'),
(134, 'Maillé'),
(135, 'Mairé'),
(136, 'Maisonneuve'),
(137, 'Marçay'),
(138, 'Marigny-Chemereau'),
(139, 'Marnay'),
(140, 'Martaizé'),
(141, 'Massognes'),
(142, 'Maulay'),
(143, 'Mauprévoir'),
(144, 'Mazerolles'),
(145, 'Mazeuil'),
(146, 'Messemé'),
(147, 'Mignaloux-Beauvoir'),
(148, 'Migné-Auxances'),
(149, 'Millac'),
(150, 'Mirebeau'),
(151, 'Moncontour'),
(152, 'Mondion'),
(153, 'Montamisé'),
(154, 'Monthoiron'),
(155, 'Montmorillon'),
(156, 'Montreuil-Bonnin'),
(157, 'Monts-sur-Guesnes'),
(158, 'Morton'),
(159, 'Moulismes'),
(160, 'Moussac'),
(161, 'Mouterre-Silly'),
(162, 'Mouterre-sur-Blourde'),
(163, 'Naintré'),
(164, 'Nalliers'),
(165, 'Nérignac'),
(166, 'Neuville-de-Poitou'),
(167, 'Nieuil-l\'Espoir'),
(168, 'Nouaillé-Maupertuis'),
(169, 'Nueil-sous-Faye'),
(170, 'Orches'),
(171, 'Les Ormes'),
(172, 'Ouzilly'),
(173, 'Oyré'),
(174, 'Paizay-le-Sec'),
(175, 'Payré'),
(176, 'Payroux'),
(177, 'Persac'),
(178, 'Pindray'),
(179, 'Plaisance'),
(180, 'Pleumartin'),
(181, 'Poitiers'),
(182, 'Port-de-Piles'),
(183, 'Pouançay'),
(184, 'Pouant'),
(185, 'Pouillé'),
(186, 'Pressac'),
(187, 'Prinçay'),
(188, 'La Puye'),
(189, 'Queaux'),
(190, 'Quinçay'),
(191, 'Ranton'),
(192, 'Raslay'),
(193, 'La Roche-Posay'),
(194, 'La Roche-Rigault'),
(195, 'Roches-Prémarie-Andillé'),
(196, 'Roiffé'),
(197, 'Romagne'),
(198, 'Rouillé'),
(199, 'Saint-Benoît'),
(200, 'Saint-Christophe'),
(201, 'Saint-Clair'),
(202, 'Sainte-Radegonde'),
(203, 'Saint-Gaudent'),
(204, 'Saint-Genest-d’Ambière'),
(205, 'Saint-Georges-Lès-Baillargeaux'),
(206, 'Saint-Germain'),
(207, 'Saint-Gervais-les-Trois-Clochers'),
(208, 'Saint-Jean-de-Sauves'),
(209, 'Saint-Julien-l\'Ars'),
(210, 'Saint-Laon'),
(211, 'Saint-Laurent-de-Jourdes'),
(212, 'Saint-Léger-de-Montbrillais'),
(213, 'Saint-Léomer'),
(214, 'Saint-Macoux'),
(215, 'Saint-Martin-l\'Ars'),
(216, 'Saint-Maurice-la-Clouère'),
(217, 'Saint-Pierre-de-Maillé'),
(218, 'Saint-Pierre-d’Exideuil'),
(219, 'Saint-Rémy-sur-Creuse'),
(220, 'Saint-Romain'),
(221, 'Saint-Sauvant'),
(222, 'Senillé-Saint-Sauveur'),
(223, 'Saint-Savin'),
(224, 'Saint-Saviol'),
(225, 'Saint-Secondin'),
(226, 'Saires'),
(227, 'Saix'),
(228, 'Sammarçolles'),
(229, 'Sanxay'),
(230, 'Saulgé'),
(231, 'Savigné'),
(232, 'Savigny-Lévescault'),
(233, 'Savigny-sous-Faye'),
(234, 'Scorbé-Clairvaux'),
(235, 'Sérigny'),
(236, 'Sèvres-Anxaumont'),
(237, 'Sillars'),
(238, 'Smarves'),
(239, 'Sommières-du-Clain'),
(240, 'Sossais'),
(241, 'Surin'),
(242, 'Tercé'),
(243, 'Ternay'),
(244, 'Thollet'),
(245, 'Thurageau'),
(246, 'Thuré'),
(247, 'La Trimouille'),
(248, 'Les Trois-Moutiers'),
(249, 'Usseau'),
(250, 'Usson-du-Poitou'),
(251, 'Valdivienne'),
(252, 'Varennes'),
(253, 'Vaux'),
(254, 'Vaux-sur-Vienne'),
(255, 'Vellèches'),
(256, 'Saint Martin la Pallu'),
(257, 'Vernon'),
(258, 'Verrières'),
(259, 'Verrue'),
(260, 'Vézières'),
(261, 'Vicq-sur-Gartempe'),
(262, 'Le Vigeant'),
(263, 'La Villedieu-du-Clain'),
(264, 'Villemort'),
(265, 'Villiers'),
(266, 'Vivonne'),
(267, 'Vouillé'),
(268, 'Voulême'),
(269, 'Voulon'),
(270, 'Vouneuil-sous-Biard'),
(271, 'Vouneuil-sur-Vienne'),
(272, 'Vouzailles'),
(273, 'Yversay');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commander`
--
ALTER TABLE `commander`
  ADD CONSTRAINT `commander_ibfk_1` FOREIGN KEY (`idcommande`) REFERENCES `commandes` (`idcommande`),
  ADD CONSTRAINT `commander_ibfk_2` FOREIGN KEY (`idmateriel`) REFERENCES `materiel` (`idmateriel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idinter_fk` FOREIGN KEY (`idinter`) REFERENCES `interventions` (`id_interventions`);

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`demandeur`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `commandes_ibfk_2` FOREIGN KEY (`centre`) REFERENCES `centres` (`idcentre`);

--
-- Contraintes pour la table `compterendu`
--
ALTER TABLE `compterendu`
  ADD CONSTRAINT `FK_2F62FA3E55AB140` FOREIGN KEY (`auteur`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_2F62FA3EED713045` FOREIGN KEY (`numIntervention`) REFERENCES `interventions` (`id_interventions`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `intervenir`
--
ALTER TABLE `intervenir`
  ADD CONSTRAINT `intervenir_ibfk_1` FOREIGN KEY (`idintervention`) REFERENCES `interventions` (`id_interventions`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `intervenir_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `interventions`
--
ALTER TABLE `interventions`
  ADD CONSTRAINT `interventions_ibfk_2` FOREIGN KEY (`motif`) REFERENCES `motif` (`idmotif`),
  ADD CONSTRAINT `interventions_ibfk_3` FOREIGN KEY (`ville`) REFERENCES `ville` (`idville`);

--
-- Contraintes pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD CONSTRAINT `materiel_ibfk_1` FOREIGN KEY (`type`) REFERENCES `typemateriel` (`idtypemateriel`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`auteur`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `idintervention_fk` FOREIGN KEY (`idintervention`) REFERENCES `interventions` (`id_interventions`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `posseder`
--
ALTER TABLE `posseder`
  ADD CONSTRAINT `posseder_ibfk_1` FOREIGN KEY (`idmateriel`) REFERENCES `materiel` (`idmateriel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `posseder_ibfk_2` FOREIGN KEY (`idpersonnel`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `regarder`
--
ALTER TABLE `regarder`
  ADD CONSTRAINT `regarder_ibfk_1` FOREIGN KEY (`news`) REFERENCES `news` (`idnews`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `regarder_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `repertoire`
--
ALTER TABLE `repertoire`
  ADD CONSTRAINT `repertoire_ibfk_1` FOREIGN KEY (`villePrincipale`) REFERENCES `ville` (`idville`),
  ADD CONSTRAINT `repertoire_ibfk_2` FOREIGN KEY (`villeSecondaire`) REFERENCES `ville` (`idville`),
  ADD CONSTRAINT `repertoire_ibfk_3` FOREIGN KEY (`categorie`) REFERENCES `categorierepertoire` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `csp` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
