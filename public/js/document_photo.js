const select_folder_value = $('#folder-select_1').html();
const select_intervention_value = $('#intervention-select_1') ? $('#intervention-select_1').html() : null;
let nb_form = 1;

$(document).ready(function () {

    let current_url = document.location.href;
    let current_url_split = current_url.split('/');
    let current_url_name = current_url_split[current_url_split.length - 2]
    let filepath_split = null;
    let filename_array = [];
    let current_filename = null;
    let label = null;

    filename_array[0] = "";

    $(document).on("click", ".folder_icon", function () {

        let id_form = this.parentElement.id.split('_');
        let num_form = id_form[id_form.length - 1];

        let divInput = $('#folder-input-div_' + num_form);
        let divSelectFolder = $("#folder-select-div_" + num_form);
        let divSelectIntervention = $("#intervention-select-div_" + num_form);
        let activeDiv = this.parentElement.id;


        if (activeDiv === divInput[0].id) {
            divInput.hide();
            divSelectFolder.show();
            divSelectIntervention.hide();
        } else if (activeDiv === divSelectFolder[0].id) {
            if (current_url_name === 'album') {
                divInput.hide();
                divSelectFolder.hide();
                divSelectIntervention.show();
            } else {
                divInput.show();
                divSelectFolder.hide();
            }
        } else if (activeDiv === divSelectIntervention[0].id) {
            divInput.show();
            divSelectFolder.hide();
            divSelectIntervention.hide();
        }

    });

    $(document).on("change", ".input_file", function () {

        let file_input = this.id.split('_');
        let id_file_input = file_input[file_input.length - 1];

        filepath_split = this.value.split('\\');
        filename_array[id_file_input - 1] = filepath_split[filepath_split.length - 1];
        label = $(".label_file_" + id_file_input);

        label.html(filename_array[id_file_input - 1]);

    });

    $(document).on("click", ".file-btn", function () {

        let child_list = this.id.split('_')
        let id_child = child_list[child_list.length - 1];
        let active_child = $("#child_" + id_child);

        if (filename_array[id_child - 1]) {

            if (id_child != nb_form || nb_form == 1)
                active_child.html('<span>' + filename_array[id_child - 1] + '</span>');
            else
                active_child.replaceWith('<li id="child_' + id_child + '"><span>' + filename_array[id_child - 1] + '</span><br/><button class="btn btn-info delete-document"><img src="../icones/delete_document.png"" width="20px" height="20px"  alt="Supprimer"/></button></li>')
        }

    });

    $(document).on("click", "#doc-list span", function () {

        let child_list = this.parentElement.id.split('_')
        let id_child = child_list[child_list.length - 1];

        hideAllFormExceptActiveForm(nb_form, id_child);

        if (current_url_name === 'album')
            $(".box-title h1").html("Photo n°" + id_child)
        else
            $(".box-title h1").html("Document n°" + id_child)

    });

    $(document).on("click", "#doc-list button", function () {
        let child_list = this.parentElement.id.split('_');
        let id_child = child_list[child_list.length - 1];
        let new_active_child_split_id = $('.form-list').children()[0].id.split('_');
        let new_active_child_id = new_active_child_split_id[new_active_child_split_id.length - 1]

        $('#document-form_' + id_child).remove();
        $('#child_' + id_child).remove();

        if (id_child - 1 > 1)
            $('#child_' + (id_child - 1)).append('<br/><button class="btn btn-info delete-document"><img src="../icones/delete_document.png"" width="20px" height="20px"  alt=" Supprimer"/></button>');

        nb_form -= 1;

        hideAllFormExceptActiveForm(nb_form, new_active_child_id);

        if (current_url_name === 'album')
            $(".box-title h1").html("Photo n°" + new_active_child_id);
        else
            $(".box-title h1").html("Document n°" + new_active_child_id);

    });

    $(".add-document").on("click", function () {

        nb_form++;

        hideAllFormExceptActiveForm(nb_form, nb_form);

        $(".box-title h1").html("Document n°" + nb_form)

        let html = getHtmlFormDocument(nb_form);

        $('.form-list').append(html);

        $("#child_" + (nb_form - 1) + ' button').remove();
        $("#child_" + (nb_form - 1) + ' br').remove();
        $("#doc-list").append('<li id="child_' + nb_form + '"><span>Document n°' + nb_form + '</span><br/><button class="btn btn-info delete-document"><img src="../icones/delete_document.png"" width="20px" height="20px"  alt="Supprimer"/></button></li>');
    })


    $(".add-photo").on("click", function () {

        nb_form++;

        hideAllFormExceptActiveForm(nb_form, nb_form);

        $(".box-title h1").html("Photo n°" + nb_form)

        let html = getHtmlFormAlbum(nb_form);

        $('.form-list').append(html);

        $("#child_" + (nb_form - 1) + ' button').remove();
        $("#doc-list").append('<li id="child_' + nb_form + '"><span>Photo n°' + nb_form + '</span><br/><button class="btn btn-info delete-document"><img src="../icones/delete_document.png"" width="20px" height="20px"  alt="Supprimer"/></button></li>');
    })


    $("#submit-form").on("click", function (e) {
        e.preventDefault(); //empêcher une action par défaut

        let form = $('#documents-list');
        let form_url = form.attr("action"); //récupérer l'URL du formulaire
        let form_method = form.attr("method"); //récupérer la méthode GET/POST du formulaire
        let form_data = new FormData(form.get(0)); //Encoder les éléments du formulaire pour la soumission
        let loading = $('#loading').show();

        loading.show();

        $.ajax({
            url: form_url,
            type: form_method,
            data: form_data,
            enctype: 'multipart/form-data',
            contentType: false,
            dataType: 'json',
            processData: false,
            cache: false,
            success:
                function (donnees, status, xhr) {
                    let errors = JSON.parse(donnees);
                    let errors_id_form = [];

                    $.each(errors, function (i) {

                        $('#child_' + i).addClass('child-error')

                        if (errors[i]['1'])
                            $('.error-file_' + (i)).html('<span class="error">' + errors[i]['1'] + '</span>')
                        else
                            $('.error-file_' + (i)).html("")

                        if (errors[i]['2'])
                            $('.error-name_' + (i)).html('<span class="error">' + errors[i]['2'] + '</span>')
                        else
                            $('.error-name_' + (i)).html("")


                        if (errors[i]['3'])
                            $('.error-folder_' + (i)).html('<span class="error">' + errors[i]['3'] + '</span>')
                        else
                            $('.error-folder_' + (i)).html("")


                        errors_id_form.push(i);
                    });

                    for (let i = 1; i <= nb_form; i++) {

                        if (!errors_id_form.includes(i.toString())) {

                            $('.error-file_' + (i)).html("")
                            $('.error-name_' + (i)).html("")
                            $('.error-folder_' + (i)).html("")
                            $('#child_' + i).removeClass('child-error')
                        }
                    }
                    loading.hide();

                    if (errors.length === 0) {
                        if (current_url_name === 'album')
                            window.location.replace(Routing.generate('album'));
                        else
                            window.location.replace(Routing.generate('docs'));
                    }
                },
            error:
                function (xhr, status, error) {

                }
        })
    });

})


function hideAllFormExceptActiveForm(nbForm, activeForm) {

    let divForm = "";

    for (let i = 1; i <= nbForm; i++) {

        divForm = $('#document-form_' + i);

        if (i != activeForm) {
            divForm.hide();

        } else {
            divForm.show();
        }
    }

}

function getHtmlFormDocument(index){

    let html = "" ;

    html += '<div id="document-form_' + index + '">'
    html += '    <div class="text-center error-file_' + index + '" ></div>'
    html += '    <div class="box-input">'
    html += '       <div class="file-input">'
    html += '           <label for="file_' + index + '"><img src="../icones/upload.png" alt="">&nbsp<span'
    html += '                   class="label_file_' + index + '">Choissisez votre document (.pdf)</span></label><br/>'
    html += '           <input id="file_' + index + '" class="input_file" type="file" name="file-path_' + index + '" accept=".pdf">'
    html += '       </div>'
    html += '    </div>'
    html += '    <div class="box-input">'
    html += '        <div class="error-name_' + index + '" ></div>'
    html += '        <div>'
    html += '           <label for="name_' + index + '">Nom du document</label><br/>'
    html += '           <input id="name_' + index + '" type="text" name="file-name_' + index + '" >'
    html += '        <div>'
    html += '    </div>'
    html += '    <div class="box-input">'
    html += '        <div class="error-folder_' + index + '" ></div>'
    html += '        <div id="folder-input-div_' + index + '">'
    html += '           <label for="folder_input_' + index + '">Nouveau dossier</label>'
    html += '           <img class="folder_icon" src="../icones/bouton_dossier.png" alt=""><br/>'
    html += '           <input id="folder_input_' + index + '" type="text" name="folder-input_' + index + '">'
    html += '        </div>'
    html += '        <div id="folder-select-div_' + index + '"class="folder-select" >'
    html += '           <label for="folder_select_' + index + '">Dossiers existants</label>'
    html += '           <img class="folder_icon" src="../icones/bouton_dossier.png" alt=""><br/>'
    html += '           <select id="folder_select_' + index + '" name="folder-select_' + index + '">'
    html +=             select_folder_value
    html += '           </select>'
    html += '        </div>'
    html += '    </div>'
    html += '    <div class="link-group">'
    html += '        <button id="file-btn_' + index + '" type="button" class="btn btn-info link-btn file-btn"><img src="../icones/check-circle.svg"'
    html += '                width="50px" height="20px" alt="Retour"/>'
    html += '        </button>'
    html += '    </div>'
    html += '</div>'

    return html ;
}

function getHtmlFormAlbum(index){

    let html = "";

    html += '<div id="document-form_' + index + '">'
    html += '    <div class="text-center error-file_' + index + '" ></div>'
    html += '    <div class="box-input">'
    html += '       <div class="file-input file_' + index + '">'
    html += '           <label for="file_' + index + '"><img src="../icones/upload.png" alt="">&nbsp<span'
    html += '                   class="label_file_' + index + '">Choissisez votre image (.jpg, .png)</span></label><br/>'
    html += '           <input id="file_' + index + '" class="input_file" type="file" name="file-path_' + index + '" accept=".png,.jpg">'
    html += '       </div>'
    html += '    </div>'
    html += '    <div class="box-input">'
    html += '        <div class="error-name_' + index + '" ></div>'
    html += '        <div class="name_' + index + '">'
    html += '           <label for="name_' + index + '">Nom de l\'image</label><br/>'
    html += '           <input id="name_' + index + '" type="text" name="file-name_' + index + '">'
    html += '        <div>'
    html += '    </div>'
    html += '    <div class="box-input">'
    html += '        <div class="error-folder_' + index + '" ></div>'
    html += '        <div id="folder-input-div_' + index + '">'
    html += '           <label for="folder_input_' + index + '">Nouveau dossier</label>'
    html += '           <img class="folder_icon" src="../icones/bouton_dossier.png" alt=""><br/>'
    html += '           <input id="folder_input_' + index + '" type="text" name="folder-input_' + index + '">'
    html += '        </div>'
    html += '        <div id="folder-select-div_' + index + '" class="folder-select" >'
    html += '           <label for="folder_select_' + index + '">Dossiers existants</label>'
    html += '           <img class="folder_icon" src="../icones/bouton_dossier.png" alt=""><br/>'
    html += '           <select id="folder_select_' + index + '" name="folder-select_' + index + '">'
    html += select_folder_value
    html += '           </select>'
    html += '        </div>'
    html += '        <div id="intervention-select-div_' + index + '" class="folder-select" >'
    html += '           <label for="folder_select_' + index + '">Interventions</label>'
    html += '           <img class="folder_icon" src="../icones/bouton_dossier.png" alt=""><br/>'
    html += '           <select id="intervention_select_' + index + '" name="intervention-select_' + index + '">'
    html += select_intervention_value
    html += '           </select>'
    html += '        </div>'
    html += '    </div>'
    html += '    <div class="link-group">'
    html += '        <button id="file-btn_' + index + '" type="button" class="btn btn-info link-btn file-btn"><img src="../icones/check-circle.svg"'
    html += '                width="50px" height="20px" alt="Retour"/>'
    html += '        </button>'
    html += '    </div>'
    html += '</div>'

    return html ;
}

function updateIndexFormImage(){

    let forms = $('.form-list').children();
    let list_child = $('#doc-list').children();
    let index_form = 1 ;

    if (nb_form > 1){

        for (let i = 2; i <= nb_form ; i++) {

            $('#' + forms[index_form].id).replaceWith(getHtmlFormAlbum(i));

            $('#' + list_child[index_form].id).replaceWith('<li id="child_' + i + '"><span>Photo n°' + i + '</span><br/><button class="btn btn-info delete-document"><img src="../icones/delete_document.png"" width="20px" height="20px"  alt="Supprimer"/></button></li>');

            index_form++;
        }
    }


}


