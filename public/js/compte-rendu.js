
$(document).ready(function(){

    $(document).on('click', '.checkbox', function (){
        let cutID = this.id.split('_')
        let index = cutID[2];
        let string = cutID[3]; 
        let baseId = cutID[0] + '_' + cutID[1] + '_' + index + '_';
     
        if(string == "garde"){
            
            document.querySelector('#' + baseId + 'astreinte').checked = false;
            document.querySelector('#' + baseId + 'repos').checked = false;
        }
        else if(string == "astreinte"){
            document.querySelector('#' + baseId + 'garde').checked = false;
            document.querySelector('#' + baseId + 'repos').checked = false;
        }
        else{
            document.querySelector('#' + baseId + 'garde').checked = false;
            document.querySelector('#' + baseId + 'astreinte').checked = false;
        }
        
    })

})

const newItem = (e) => {
    const collectionHolder = document.querySelector(e.currentTarget.dataset.collection);
   
    const item = document.createElement("div");

    item.innerHTML += collectionHolder.dataset.prototype.replace(/__name__/g, collectionHolder.dataset.index );

    item.querySelectorAll('.btn-remove').forEach(btn => btn.addEventListener("click", () => item.remove()))
    
    collectionHolder.appendChild(item);

    collectionHolder.dataset.index++;


};


document.querySelectorAll('.btn-remove').forEach(btn => btn.addEventListener('click', (e) => e.currentTarget.closest(".item").remove()));

document.querySelectorAll('.btn-new').forEach(img => img.addEventListener('click', newItem));



