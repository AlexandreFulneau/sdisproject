window.onload = () => {

   cspVisible();
   document.getElementById('status').onchange = function() {cspVisible()};
}


function cspVisible(){

    var select = document.getElementById('status');
    var choice = select.selectedIndex;
    var selected_choice = select.options[choice].value;
    var csppro_div = document.getElementById('csp_pro');
    var cspvol_div = document.getElementById('csp_vol');
    var cspveto_div = document.getElementById('csp_veto');
    var cspaffi_div = document.getElementById('csp_affi');


    if (selected_choice == "pro"){
        csppro_div.style.display = "block";
        cspvol_div.style.display = "none";
        cspveto_div.style.display = "none";
        cspaffi_div.style.display = "block";
      
    }
    else if(selected_choice == "provol"){
        csppro_div.style.display = "block";
        cspvol_div.style.display = "block";
        cspveto_div.style.display = "none";
        cspaffi_div.style.display = "none";
    }
    else if(selected_choice == "vol"){
        csppro_div.style.display = "none";
        cspvol_div.style.display = "block";
        cspveto_div.style.display = "none";
        cspaffi_div.style.display = "none";
    }
    else if(selected_choice == "veterinaire"){
        csppro_div.style.display = "none";
        cspvol_div.style.display = "none";
        cspveto_div.style.display = "block";
        cspaffi_div.style.display = "none";
    }
    else{
        csppro_div.style.display = "none";
        cspvol_div.style.display = "none";
        cspveto_div.style.display = "none";
        cspaffi_div.style.display = "none";
    }

}