var map = L.map('map').setView([46.725681, 0.459478], 9);

L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap',
    minZoom:10,
    maxZoom: 16,
    
}).addTo(map);


const users = document.querySelectorAll(".user");
const centresSecours = document.querySelectorAll(".cs");

Array.from(users).forEach((element, index) => 
{

    const userInfos = element.textContent.split('_');
    var icon = L.icon({
        iconUrl: "icones/maison.png",
        iconSize: [50, 50],
        iconAnchor: [25,50],
        popupAnchor: [0, -50]
    })

    var marker = L.marker([userInfos[1],userInfos[2]],{icon: icon}).addTo(map);
    marker.bindPopup("<b> Domicile de " + userInfos[0] +"</b>")

});

Array.from(centresSecours).forEach((element, index) => 
{

    const csInfos = element.textContent.split('_');
    
    var icon = L.icon({
        iconUrl: "icones/caserne.png",
        iconSize: [50, 50],
        iconAnchor: [25, 50],
        popupAnchor: [0, -50]
    })

    var marker = L.marker([csInfos[2],csInfos[3]],{icon: icon}).addTo(map);
    marker.bindPopup("<b> " + csInfos[1] + ' ' +  csInfos[0] +"</b>")

});


