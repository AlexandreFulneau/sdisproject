$(document).ready(function(){
    $(".lm-li").on('click', function(){
        $('.lm-li').removeClass('active'),
            $('.lm-li-fst').removeClass('active-fst'),
            $('.lm-li-lst').removeClass('active-lst'),
            $(this).addClass('active');
    });

    $(".lm-li-fst").on('click', function(){
        $('.lm-li').removeClass('active'),
            $('.lm-li-fst').removeClass('active-fst'),
            $('.lm-li-lst').removeClass('active-lst'),
            $(this).addClass('active-fst');
    });

    $(".lm-li-lst").on('click', function(){
        $('.lm-li').removeClass('active'),
            $('.lm-li-fst').removeClass('active-fst'),
            $('.lm-li-lst').removeClass('active-lst'),
            $(this).addClass('active-lst');
    });

    $("#ville_inter").on('click',function (){

        var url = Routing.generate('ville_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#content").removeData().html(data.content) ;
            }
        });
    })

    $(document).on('click','.edit_v',function (){

        var url = Routing.generate('ville_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })


    $("#motif_inter").on('click',function (){

        var url = Routing.generate('motif_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_m',function (){

        var url = Routing.generate('motif_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#type_materiel").on('click',function (){

        var url = Routing.generate('type_materiel_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_tm',function (){

        var url = Routing.generate('type_materiel_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#centres_secours").on('click',function (){

        var url = Routing.generate('centres_secours_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_cs',function (){

        var url = Routing.generate('centres_secours_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })


    $("#fonction_user").on('click',function (){

        var url = Routing.generate('fonction_user_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_fu',function (){

        var url = Routing.generate('fonction_user_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#grade_user").on('click',function (){

        var url = Routing.generate('grade_user_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_gu',function (){

        var url = Routing.generate('grade_user_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#categories_rep").on('click',function (){

        var url = Routing.generate('categories_repertoire_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_cr',function (){

        var url = Routing.generate('categories_repertoire_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#nom_equipement").on('click',function (){

        var url = Routing.generate('nom_equipement_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_ne',function (){

        var url = Routing.generate('nom_equipement_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#lieux_interventions").on('click',function (){

        var url = Routing.generate('lieux_interventions_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_li',function (){

        var url = Routing.generate('lieux_interventions_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#technique").on('click',function (){

        var url = Routing.generate('technique_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_t',function (){

        var url = Routing.generate('technique_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#statut_fmpa").on('click',function (){

        var url = Routing.generate('statut_fmpa_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_st',function (){

        var url = Routing.generate('statut_fmpa_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#filiere").on('click',function (){

        var url = Routing.generate('filiere_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_fi',function (){

        var url = Routing.generate('filiere_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })

    $("#CTD").on('click',function (){

        var url = Routing.generate('CTD_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_ctd',function (){

        var url = Routing.generate('CTD_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })
    
    $("#type_convention").on('click',function (){

        var url = Routing.generate('type_convention_new',{'ajax': true});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){
                $("#content").removeData().html(data.content);
            }
        });
    })

    $(document).on('click','.edit_tc',function (){

        var url = Routing.generate('type_convention_edit',{'id' : this.value, 'ajax': true,});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data){

                $("#form_edit").html(data.content) ;
            }
        });
    })
});