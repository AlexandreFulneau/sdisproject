var timer;

$(document).on('change','#stock', function (){
    clearTimeout(timer);
    let id = $(this).next().val();
    let stock = $(this).val();

    timer = setTimeout(function () {
        let url = Routing.generate('materiel_modifStock',{'id' : id, 'stock': stock});

        console.log(url)
        $.ajax({
            type: "POST",
            url: url,
            success: function (data) {
            }
        });
    }, 1000);
})


$(document).on('change','#type', function (){
    clearTimeout(timer);
    let id = $(this).next().val();
    let type = $(this).val();

    timer = setTimeout(function () {
        let url = Routing.generate('materiel_modifType',{'id' : id, 'type': type});

        $.ajax({
            type: "POST",
            url: url,
            success: function (data) {
            }
        });
    }, 1000);
})

function verify(){
    $('.flash-info').remove();

    var trouve = false;
    var leMat = $('#inputMat').val();

    $('.nomMat').each(function(){

        if(leMat === $(this).val()){
            $('.erreur').remove();
            $('#inputMat').before('<p style="color: #B01924;" class="erreur text-center font-weight-bold">Ce matériel existe déjà.</p>');
            $('#inputMat').css({
                "border-width": "2px",
                "border-style": "solid",
                "border-color": "#191919"
            });
            trouve = true;
        }
    });

    if(trouve === false){
        $('#formMat').submit();
    }
}

// Refuser touche entrée sur input car déjà sur rechercher
function refuserToucheEntree(event)
{
    // Compatibilité IE / Firefox
    if(!event && window.event) {
        event = window.event;
    }
    // IE
    if(event.keyCode === 13) {
        event.returnValue = false;
        event.cancelBubble = true;
    }
    // DOM
    if(event.which === 13) {
        event.preventDefault();
        event.stopPropagation();
    }
}

// Si appuie sur icone loupe, on recherche
function research(){

    var found = window.find($('#inputRechercher').val());

    if(!found)
        alert("Ce matériel n'existe pas.");

}

// Si touche entrée dans barre recherche alors on recherche
function search(event){
    // Compatibilité IE / Firefox
    if(!event && window.event) {
        event = window.event;
    }
    // DOM
    if(event.which === 13) {
        research();
    }
}

$(document).ready(function(){

    let form_consommable = $('#form-consommable');
    let form_equipement = $('#form-equipement');
    let group_form = $('#group-form');
    let group_form_visible = false;
    let active_form = form_consommable

    $(document).on('click', '.equipement', function (){

        window.location.href = Routing.generate('showEquipments',{'name': this.id});
    })


    $(document).on('click','.choice-menu div img', function (){

        if (active_form === form_consommable) {
            form_consommable.hide();
            form_equipement.show();
            active_form = form_equipement;
        }
        else{
            form_consommable.show();
            form_equipement.hide();
            active_form = form_consommable;
        }

    })

    $(document).on('click','.choice-menu div span', function (){

        if (group_form_visible === false){
            group_form.fadeIn(500);
            group_form_visible = true;
            $('.choice-menu div span').html('Masquer');
        }
        else{
            group_form.fadeOut(500);
            group_form_visible = false;
            $('.choice-menu div span').html('Afficher');
        }

    })

    $(document).on('click','#scan-qr-code',function (){

        
    })

    $(document).on('change','#equipement_operationnel_oxygeneFilter',function() {

        var $form = $('#form-equipement form');
        var data = getFormEquipmentValues();

        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(data) {

                $form.replaceWith(data['content'])

            }
        });
    });

    $(document).on('change','#equipement_operationnel_csFilter',function() {

        var $form = $('#form-equipement form');
        var data = getFormEquipmentValues();

        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(data) {

                $form.replaceWith(data['content'])

            }
        });
    });

    $(document).on('change','#equipement_operationnel_qrcodeFilter',function() {

        let $form = $('#form-equipement form');
        let data = getFormEquipmentValues();

        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(data) {

                $form.replaceWith(data['content'])

            }
        });
    });

    let modalForm = null;
    let modalVerifications = null;
    let span = null;


    if (document.getElementById("modalForm")){
        modalForm = document.getElementById("modalForm");
    }

    if (document.getElementById("modalVerification")){
        modalVerifications = document.getElementById("modalVerification");
    }


        $(document).on('click','#close', function () {

            modalForm.style.display = "none";
            modalVerifications.style.display = "none";
        })


    $(document).on('click','.verification',function() {

        let $url = Routing.generate('addVerification',{'id': this.value,'ajax': true});

        $.ajax({
            type: "POST",
            url : $url,
            success: function(data) {
                $('#form-verification').html(data);
                modalForm.style.display = "block";
            }
        });
    });

    $(document).on('click','.verifications-archive',function() {

        let $url = Routing.generate('showVerifications',{'id': this.value});

        $.ajax({
            type: "POST",
            url : $url,
            success: function(data) {
                $('#historique-verification').html(data);
                modalVerifications.style.display = "block";
            }
        });
    });

    function getFormEquipmentValues(){

        let name = $('#equipement_operationnel_nom');
        let num = $('#equipement_operationnel_numero');
        let createdDate = $('#equipement_operationnel_dateCreation');
        let period = $('#equipement_operationnel_periodicite');
        let lifeDuration = $('#equipement_operationnel_dureeVie');
        let type = $('#equipement_operationnel_type');
        let oxygeneFilter = $('#equipement_operationnel_oxygeneFilter');
        let csFilter = $('#equipement_operationnel_csFilter');
        let qrCodeFilter = $('#equipement_operationnel_qrcodeFilter');
        let data = {}

        data[name.attr('name')] = name.val() ;
        data[num.attr('name')] = num.val() ;
        data[createdDate.attr('name')] = createdDate.val() ;
        data[period.attr('name')] = period.val() ;
        data[lifeDuration.attr('name')] = lifeDuration.val() ;
        data[type.attr('name')] = type.val() ;
        data[csFilter.attr('name')] = csFilter.prop('checked');
        data[oxygeneFilter.attr('name')] = oxygeneFilter.prop('checked');
        data[qrCodeFilter.attr('name')] = qrCodeFilter.prop('checked');

        return data;
    }


})

