$(document).on('click','#add-material', function (){

    let idinter = $('#intervention').val();
    let selectedMateriel = $("select option:selected");
    let materialName = selectedMateriel.html();
    let materialID = selectedMateriel.val();
    let quantite = $("#inputNbMateriel").val();
    let justif = $("#inputJustification").val();
    justif = justif.charAt(0).toUpperCase() + justif.slice(1);

    if (Number.isInteger(parseInt(quantite, 10)) && selectedMateriel.val() !== "") {
        $('#init').html("");


        let $html = ""
        $html += '<tr>'
        $html += '  <td data-label="Matériel" class="font-weight-bold">' + materialName + '<input type="hidden" name="materials[]" value="' + materialID + '"></td>'
        $html += '  <td data-label="Commande">' + quantite + '<input type="hidden" name="quantities[]" value="' + quantite + '"></td>'
        $html += '  <td data-label="Justification">' + justif + '<input type="hidden" name="justifications[]" value="' + justif + '"></td>'
        $html += '  <td>'
        $html += '      <button id="delete-material" type="button" class="btn btn-info link-btn" >'
        $html += '          <img src="../../icones/trash.svg" alt="">'
        $html += '      </button>'
        $html += '   </td>'
        $html += '</tr>'

        $('#tableMateriel tr:last').after($html);
        $('select').val("");
        $('#inputNbMateriel').val("");
        $('#inputJustification').val("");
    }


})


$(document).on('click','#delete-material', function (){
    $(this).parent().parent().remove();

})


$('#submit-order').on('click',function (){

    $('.error').remove();
    if ($('tbody').children('tr').length > 1) {
        $('.submit').submit();
    } else {
        $('#error').html('<p class="error font-weight-bold text-danger text-center w-100">Vous devez choisir du matériel.</p>');
    }

})

