var minYear = null;
var maxYear = null;

window.onload = () => {
    const filter = document.querySelector('#filter')

    if (filter != null) {

        addFilter(filter);

    }

    const select = document.querySelector('#selectFilter')

    if (select != null) {
        select.addEventListener("change", (event) => {

            if (select.value == "range") {

                document.getElementById('container-range').style.display = 'inline';
            } else {
                document.getElementById('container-range').style.display = 'none';
            }

            if (select.value == "year") {

                document.getElementById('container-year').style.display = 'inline';
            } else {
                document.getElementById('container-year').style.display = 'none';
            }

        })

        const btn_filter = document.getElementById("btn-filter-range");

        const select_range_min = document.getElementById("minYear");
        const select_range_max = document.getElementById("maxYear");

        select_range_min.addEventListener("change", (event) => {
            minYear = select_range_min.value;
        })

        select_range_max.addEventListener("change", (event) => {
            maxYear = select_range_max.value;
        })

        btn_filter.addEventListener("click", (event) => {


            const param = new URLSearchParams();

            param.append('filter_range_min', minYear);
            param.append('filter_range_max', maxYear);

            const Url = new URL(window.location.href);

            if (document.getElementById("interStats")) {

                addFilterOnInterStatsPDF(select_range_min, select_range_max);
            }

            if (document.getElementById("communeStats")) {

                addFilterOnCommuneStatsPDF(select_range_min, select_range_max);
            }

            if (document.getElementById("techniqueStats")) {

                addFilterOnTechniqueStatsPDF(select_range_min, select_range_max);
            }

            if (document.getElementById("materielStats")) {

                addFilterOnMaterielStatsPDF(select_range_min, select_range_max);
            }

            if (document.getElementById("allStats")) {

                addFilterOnAllStatsPDF(select_range_min, select_range_max);
            }

            fetch(Url.pathname + "?" + param.toString() + "&ajax=1", {
                headers: {
                    "X-Requested-With": "XMLHttpRequest"
                }
            }).then(
                response => response.json()
            ).then(
                data => {
                    const content = document.querySelector("#content");
                    content.innerHTML = data.content;
                }
            ).catch(e => alert(e));

        })
    }
}

function addFilter(filter) {

    if (filter != null) {
        filter.addEventListener("change", (event) => {
            const param = new URLSearchParams();
            var filterValue = filter.value;

            param.append('filter', filterValue);

            const Url = new URL(window.location.href);

            if (document.getElementById("pdfStock")) {

                addFilterOnPDFStock(filter);
            }

            if (document.getElementById("interStats")) {

                addFilterOnInterStatsPDF(filter);
            }

            if (document.getElementById("communeStats")) {

                addFilterOnCommuneStatsPDF(filter);
            }

            if (document.getElementById("techniqueStats")) {

                addFilterOnTechniqueStatsPDF(filter);
            }

            if (document.getElementById("materielStats")) {

                addFilterOnMaterielStatsPDF(filter);
            }

            if (document.getElementById("allStats")) {

                addFilterOnAllStatsPDF(filter);
            }

            if (document.getElementById("repertoirePDF")) {

                addFilterOnRepertoirePDF(filter);
            }

            fetch(Url.pathname + "?" + param.toString() + "&ajax=1", {
                headers: {
                    "X-Requested-With": "XMLHttpRequest"
                }
            }).then(
                response => response.json()
            ).then(
                data => {
                    const content = document.querySelector("#content");
                    content.innerHTML = data.content;
                }
            ).catch(e => alert(e));

        });
    }
}

function addFilterOnPDFStock(filter) {

    var url_generate = Routing.generate('downloadrecap', {'filter': encodeURIComponent(filter.value)});

    document.getElementById("pdfStock").href = url_generate;

}

function addFilterOnInterStatsPDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfStats', {
        'type': "inter",
        'filter_range_min': filterOne,
        'filter_range_max': filterTwo
    });
    document.getElementById("interStats").href = url_generate;
}

function addFilterOnCommuneStatsPDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfStats', {
        'type': "commune",
        'filter_range_min': filterOne,
        'filter_range_max': filterTwo
    });
    document.getElementById("communeStats").href = url_generate;
}

function addFilterOnTechniqueStatsPDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfStats', {
        'type': "technique",
        'filter_range_min': filterOne,
        'filter_range_max': filterTwo
    });
    document.getElementById("techniqueStats").href = url_generate;
}

function addFilterOnMaterielStatsPDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfStats', {
        'type': "materiel",
        'filter_range_min': filterOne,
        'filter_range_max': filterTwo
    });
    document.getElementById("materielStats").href = url_generate;
}

function addFilterOnAllStatsPDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfStats', {
        'type': "all",
        'filter_range_min': filterOne,
        'filter_range_max': filterTwo
    });
    document.getElementById("allStats").href = url_generate;
}

function addFilterOnRepertoirePDF(filterOne = null, filterTwo = null) {

    if (filterOne == null)
        filterOne = "none";
    else
        filterOne = filterOne.value;

    if (filterTwo == null)
        filterTwo = "none";
    else
        filterTwo = filterTwo.value;

    var url_generate = Routing.generate('pdfRepertoire', {
        'filter': filterOne
    });
    document.getElementById("repertoirePDF").href = url_generate;
}