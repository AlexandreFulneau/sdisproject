<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220922210223 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE convention (id INT AUTO_INCREMENT NOT NULL, type_convention_id INT NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, site VARCHAR(255) NOT NULL, adresse_site VARCHAR(255) NOT NULL, nom_convention VARCHAR(255) DEFAULT NULL, chemin_convention VARCHAR(255) DEFAULT NULL, nom_fiche_preparatoire VARCHAR(255) DEFAULT NULL, chemin_fiche_preparatoire VARCHAR(255) DEFAULT NULL, nom_fiche_preparatoire_vierge VARCHAR(255) DEFAULT NULL, chemin_fiche_preparatoire_vierge VARCHAR(255) DEFAULT NULL, INDEX IDX_8556657ED2415CF9 (type_convention_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_convention (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE convention ADD CONSTRAINT FK_8556657ED2415CF9 FOREIGN KEY (type_convention_id) REFERENCES type_convention (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE convention DROP FOREIGN KEY FK_8556657ED2415CF9');
        $this->addSql('DROP TABLE convention');
        $this->addSql('DROP TABLE type_convention');
    }
}
