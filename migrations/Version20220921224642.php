<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220921224642 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorierepertoire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, classement INT DEFAULT 1, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE centres (idcentre INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(idcentre)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE centres_secours (id INT AUTO_INCREMENT NOT NULL, type INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, statut VARCHAR(255) NOT NULL, INDEX type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commander (idcommander INT AUTO_INCREMENT NOT NULL, idcommande INT DEFAULT NULL, idmateriel INT DEFAULT NULL, id_equipement INT DEFAULT NULL, idinter INT DEFAULT NULL, quantite INT NOT NULL, justification TEXT DEFAULT NULL, INDEX id_equipement (id_equipement), INDEX idcommande (idcommande), INDEX idinter_fk (idinter), INDEX idmateriel (idmateriel), PRIMARY KEY(idcommander)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commandes (idcommande INT AUTO_INCREMENT NOT NULL, demandeur INT DEFAULT NULL, centre INT DEFAULT NULL, datecommande DATE NOT NULL, INDEX centre (centre), INDEX demandeur (demandeur), PRIMARY KEY(idcommande)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ctd (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE documents (id INT AUTO_INCREMENT NOT NULL, chemin VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, dossier VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipement_operationnel (id INT AUTO_INCREMENT NOT NULL, nom INT DEFAULT NULL, type INT DEFAULT NULL, periodicite INT DEFAULT NULL, duree_vie INT DEFAULT NULL, centres_secours INT DEFAULT NULL, numero VARCHAR(255) DEFAULT NULL, date_creation DATE NOT NULL, date_prochaine_verification DATE NOT NULL, min_pression INT DEFAULT NULL, pression INT DEFAULT NULL, QR_code TEXT DEFAULT NULL, disponible TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX centres_secours (centres_secours), INDEX duree_vie (duree_vie), INDEX nom (nom), INDEX periodicite (periodicite), INDEX type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, personnel_id INT NOT NULL, centre_id INT NOT NULL, ctd_id INT NOT NULL, filiere_id INT NOT NULL, nb_interventions INT NOT NULL, nb_heures_fmpa INT NOT NULL, objectif_annee_en_cours LONGTEXT NOT NULL, competence_operationnelle LONGTEXT NOT NULL, implication LONGTEXT NOT NULL, critere_annee_en_cours LONGTEXT NOT NULL, bilan LONGTEXT NOT NULL, objectif_annee_suivante LONGTEXT NOT NULL, condition_reussite_annee_suivante LONGTEXT NOT NULL, critere_annee_suivante LONGTEXT NOT NULL, delais_realisation_annee_suivante VARCHAR(255) NOT NULL, commentaire LONGTEXT NOT NULL, INDEX IDX_1323A5751C109075 (personnel_id), INDEX IDX_1323A575463CD7C3 (centre_id), INDEX IDX_1323A5751EB4E62E (ctd_id), INDEX IDX_1323A575180AA129 (filiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fiche_individuelle_fmpa (id INT AUTO_INCREMENT NOT NULL, personnel_id INT NOT NULL, statut_id INT NOT NULL, fmpa_id INT NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, nb_heures INT NOT NULL, sujet VARCHAR(255) NOT NULL, lieux VARCHAR(255) NOT NULL, temps_immersion TIME DEFAULT NULL, INDEX IDX_238A0CF81C109075 (personnel_id), INDEX IDX_238A0CF8F6203804 (statut_id), INDEX IDX_238A0CF82228D6AE (fmpa_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filiere (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fmpa (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fonction_user (id_fonction INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, classement INT DEFAULT 1, PRIMARY KEY(id_fonction)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE grade_user (id_grade INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, classement INT DEFAULT 1, PRIMARY KEY(id_grade)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervenir (id INT AUTO_INCREMENT NOT NULL, intervention_id INT NOT NULL, user_id INT NOT NULL, garde TINYINT(1) NOT NULL, astreinte TINYINT(1) NOT NULL, repos TINYINT(1) NOT NULL, INDEX IDX_DFB079928EAE3863 (intervention_id), INDEX IDX_DFB07992A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention_materiel (id INT AUTO_INCREMENT NOT NULL, intervention_id INT NOT NULL, materiel_id INT NOT NULL, stock INT NOT NULL, INDEX IDX_2541CB328EAE3863 (intervention_id), INDEX IDX_2541CB3216880AAF (materiel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE intervention_technique (id INT AUTO_INCREMENT NOT NULL, intervention_id INT NOT NULL, technique_id INT NOT NULL, INDEX IDX_98D9F9688EAE3863 (intervention_id), INDEX IDX_98D9F9681F8ACB26 (technique_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interventions (id INT AUTO_INCREMENT NOT NULL, motif INT DEFAULT NULL, ville INT DEFAULT NULL, adresse INT DEFAULT NULL, auteur INT DEFAULT NULL, num_intervention INT DEFAULT NULL, date DATE NOT NULL, recapitulatif TEXT DEFAULT NULL, INDEX IDX_5ADBAD7F55AB140 (auteur), INDEX adresse (adresse), INDEX motif (motif), INDEX ville (ville), UNIQUE INDEX num_intervention (num_intervention), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE asso_intervention_equipement (interventions_id INT NOT NULL, equipement_operationnel_id INT NOT NULL, INDEX IDX_E1B6D8EE334423FF (interventions_id), INDEX IDX_E1B6D8EE8A1631E1 (equipement_operationnel_id), PRIMARY KEY(interventions_id, equipement_operationnel_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieux_interventions (id_lieux_interventions INT AUTO_INCREMENT NOT NULL, nom_lieux_interventions VARCHAR(255) NOT NULL, PRIMARY KEY(id_lieux_interventions)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiels (id INT AUTO_INCREMENT NOT NULL, type INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, stock INT NOT NULL, INDEX type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE motif (idmotif INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(idmotif)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news (idnews INT AUTO_INCREMENT NOT NULL, auteur INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, date DATE NOT NULL, contenu TEXT NOT NULL, INDEX auteur (auteur), PRIMARY KEY(idnews)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_archive (id_news_archive INT AUTO_INCREMENT NOT NULL, news_nom VARCHAR(255) NOT NULL, news_chemin VARCHAR(255) NOT NULL, news_dossier VARCHAR(255) NOT NULL, PRIMARY KEY(id_news_archive)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nom_equipement_operationnel (id_nom_equipement INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id_nom_equipement)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE periodicite_equipement_operationnel (id_periodicite INT AUTO_INCREMENT NOT NULL, duree VARCHAR(255) NOT NULL, PRIMARY KEY(id_periodicite)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photos (idphoto INT AUTO_INCREMENT NOT NULL, idintervention INT DEFAULT NULL, date DATE DEFAULT NULL, chemin VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, dossier VARCHAR(255) DEFAULT NULL, INDEX idintervention_fk (idintervention), PRIMARY KEY(idphoto)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posseder (idutiliser INT AUTO_INCREMENT NOT NULL, id_equipement_operationnel INT DEFAULT NULL, idpersonnel INT DEFAULT NULL, taille VARCHAR(255) DEFAULT NULL, INDEX id (id_equipement_operationnel), INDEX idpersonnel (idpersonnel), PRIMARY KEY(idutiliser)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE regarder (idregarder INT AUTO_INCREMENT NOT NULL, news INT DEFAULT NULL, user INT DEFAULT NULL, vue TINYINT(1) NOT NULL, INDEX new (news), INDEX user (user), PRIMARY KEY(idregarder)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repertoire (idrepertoire INT AUTO_INCREMENT NOT NULL, categorie INT DEFAULT NULL, grade VARCHAR(255) DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, tel VARCHAR(255) DEFAULT NULL, TelephoneFixe VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, Specialite VARCHAR(255) DEFAULT NULL, site VARCHAR(255) DEFAULT NULL, commentaires TEXT DEFAULT NULL, adressePrincipale VARCHAR(255) DEFAULT NULL, adresseSecondaire VARCHAR(255) DEFAULT NULL, villePrincipale INT DEFAULT NULL, villeSecondaire INT DEFAULT NULL, INDEX categorie (categorie), INDEX villePrincipale_fk (villePrincipale), INDEX villeSecondaire_fk (villeSecondaire), PRIMARY KEY(idrepertoire)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut_fmpa (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technique (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE typemateriel (idtypemateriel INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(idtypemateriel)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_centres_secours (id_type_cs INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, classement INT DEFAULT 1, PRIMARY KEY(id_type_cs)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, csp_pro INT DEFAULT NULL, csp_vol INT DEFAULT NULL, csp_affiliation INT DEFAULT NULL, grade INT DEFAULT NULL, fonction INT DEFAULT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) DEFAULT \'$2y$10$mmC6gzjR5YrCZcvMyBlT4u7J5l/j0sut85KkGDnD2FEAHTGbDSdSe\' NOT NULL, telephone VARCHAR(255) DEFAULT NULL, adresse_domicile VARCHAR(255) DEFAULT NULL, INDEX IDX_8D93D6496770A7C6 (csp_affiliation), INDEX csp_pro (csp_pro), INDEX csp_vol (csp_vol), INDEX fonction (fonction), INDEX grade (grade), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE verification_equipement_operationnel (id_verification_equipement INT AUTO_INCREMENT NOT NULL, verificateur INT DEFAULT NULL, nom_equipement INT DEFAULT NULL, date DATE NOT NULL, commentaire VARCHAR(255) NOT NULL, INDEX nom_epi (nom_equipement), INDEX verificateur (verificateur), PRIMARY KEY(id_verification_equipement)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ville (idville INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(idville)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE centres_secours ADD CONSTRAINT FK_354EB4FE8CDE5729 FOREIGN KEY (type) REFERENCES types_centres_secours (id_type_cs)');
        $this->addSql('ALTER TABLE commander ADD CONSTRAINT FK_42D318BAC43FEE70 FOREIGN KEY (idcommande) REFERENCES commandes (idcommande)');
        $this->addSql('ALTER TABLE commander ADD CONSTRAINT FK_42D318BAB207F89C FOREIGN KEY (idmateriel) REFERENCES materiels (id)');
        $this->addSql('ALTER TABLE commander ADD CONSTRAINT FK_42D318BA1D3E4624 FOREIGN KEY (id_equipement) REFERENCES equipement_operationnel (id)');
        $this->addSql('ALTER TABLE commander ADD CONSTRAINT FK_42D318BAAA854176 FOREIGN KEY (idinter) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commandes ADD CONSTRAINT FK_35D4282C665DA613 FOREIGN KEY (demandeur) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commandes ADD CONSTRAINT FK_35D4282CC6A0EA75 FOREIGN KEY (centre) REFERENCES centres (idcentre)');
        $this->addSql('ALTER TABLE equipement_operationnel ADD CONSTRAINT FK_F38E91D26C6E55B5 FOREIGN KEY (nom) REFERENCES nom_equipement_operationnel (id_nom_equipement)');
        $this->addSql('ALTER TABLE equipement_operationnel ADD CONSTRAINT FK_F38E91D28CDE5729 FOREIGN KEY (type) REFERENCES typemateriel (idtypemateriel)');
        $this->addSql('ALTER TABLE equipement_operationnel ADD CONSTRAINT FK_F38E91D2D13D99F3 FOREIGN KEY (periodicite) REFERENCES periodicite_equipement_operationnel (id_periodicite)');
        $this->addSql('ALTER TABLE equipement_operationnel ADD CONSTRAINT FK_F38E91D23BDFB20B FOREIGN KEY (duree_vie) REFERENCES periodicite_equipement_operationnel (id_periodicite)');
        $this->addSql('ALTER TABLE equipement_operationnel ADD CONSTRAINT FK_F38E91D2354EB4FE FOREIGN KEY (centres_secours) REFERENCES centres_secours (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A5751C109075 FOREIGN KEY (personnel_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575463CD7C3 FOREIGN KEY (centre_id) REFERENCES centres_secours (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A5751EB4E62E FOREIGN KEY (ctd_id) REFERENCES ctd (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A575180AA129 FOREIGN KEY (filiere_id) REFERENCES filiere (id)');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa ADD CONSTRAINT FK_238A0CF81C109075 FOREIGN KEY (personnel_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa ADD CONSTRAINT FK_238A0CF8F6203804 FOREIGN KEY (statut_id) REFERENCES statut_fmpa (id)');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa ADD CONSTRAINT FK_238A0CF82228D6AE FOREIGN KEY (fmpa_id) REFERENCES fmpa (id)');
        $this->addSql('ALTER TABLE intervenir ADD CONSTRAINT FK_DFB079928EAE3863 FOREIGN KEY (intervention_id) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE intervenir ADD CONSTRAINT FK_DFB07992A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE intervention_materiel ADD CONSTRAINT FK_2541CB328EAE3863 FOREIGN KEY (intervention_id) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE intervention_materiel ADD CONSTRAINT FK_2541CB3216880AAF FOREIGN KEY (materiel_id) REFERENCES materiels (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE intervention_technique ADD CONSTRAINT FK_98D9F9688EAE3863 FOREIGN KEY (intervention_id) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE intervention_technique ADD CONSTRAINT FK_98D9F9681F8ACB26 FOREIGN KEY (technique_id) REFERENCES technique (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7F87D377BB FOREIGN KEY (motif) REFERENCES motif (idmotif)');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7F43C3D9C3 FOREIGN KEY (ville) REFERENCES ville (idville)');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7FC35F0816 FOREIGN KEY (adresse) REFERENCES lieux_interventions (id_lieux_interventions)');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7F55AB140 FOREIGN KEY (auteur) REFERENCES user (id)');
        $this->addSql('ALTER TABLE asso_intervention_equipement ADD CONSTRAINT FK_E1B6D8EE334423FF FOREIGN KEY (interventions_id) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE asso_intervention_equipement ADD CONSTRAINT FK_E1B6D8EE8A1631E1 FOREIGN KEY (equipement_operationnel_id) REFERENCES equipement_operationnel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE698CDE5729 FOREIGN KEY (type) REFERENCES typemateriel (idtypemateriel)');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD3995055AB140 FOREIGN KEY (auteur) REFERENCES user (id)');
        $this->addSql('ALTER TABLE photos ADD CONSTRAINT FK_876E0D997B76BA2 FOREIGN KEY (idintervention) REFERENCES interventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE posseder ADD CONSTRAINT FK_62EF7CBA9DE54682 FOREIGN KEY (id_equipement_operationnel) REFERENCES equipement_operationnel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE posseder ADD CONSTRAINT FK_62EF7CBAD8A75A2B FOREIGN KEY (idpersonnel) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE regarder ADD CONSTRAINT FK_CB13CFC01DD39950 FOREIGN KEY (news) REFERENCES news (idnews) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE regarder ADD CONSTRAINT FK_CB13CFC08D93D649 FOREIGN KEY (user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE repertoire ADD CONSTRAINT FK_3C367876F6F83C8A FOREIGN KEY (villePrincipale) REFERENCES ville (idville)');
        $this->addSql('ALTER TABLE repertoire ADD CONSTRAINT FK_3C367876942036BF FOREIGN KEY (villeSecondaire) REFERENCES ville (idville)');
        $this->addSql('ALTER TABLE repertoire ADD CONSTRAINT FK_3C367876497DD634 FOREIGN KEY (categorie) REFERENCES categorierepertoire (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64954CD06BF FOREIGN KEY (csp_pro) REFERENCES centres_secours (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649362547AB FOREIGN KEY (csp_vol) REFERENCES centres_secours (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6496770A7C6 FOREIGN KEY (csp_affiliation) REFERENCES centres_secours (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649595AAE34 FOREIGN KEY (grade) REFERENCES grade_user (id_grade)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649900D5BD FOREIGN KEY (fonction) REFERENCES fonction_user (id_fonction)');
        $this->addSql('ALTER TABLE verification_equipement_operationnel ADD CONSTRAINT FK_4AFF8D2AF7C79AFB FOREIGN KEY (verificateur) REFERENCES user (id)');
        $this->addSql('ALTER TABLE verification_equipement_operationnel ADD CONSTRAINT FK_4AFF8D2AB9FB4684 FOREIGN KEY (nom_equipement) REFERENCES equipement_operationnel (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE repertoire DROP FOREIGN KEY FK_3C367876497DD634');
        $this->addSql('ALTER TABLE commandes DROP FOREIGN KEY FK_35D4282CC6A0EA75');
        $this->addSql('ALTER TABLE equipement_operationnel DROP FOREIGN KEY FK_F38E91D2354EB4FE');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575463CD7C3');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64954CD06BF');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649362547AB');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6496770A7C6');
        $this->addSql('ALTER TABLE commander DROP FOREIGN KEY FK_42D318BAC43FEE70');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A5751EB4E62E');
        $this->addSql('ALTER TABLE commander DROP FOREIGN KEY FK_42D318BA1D3E4624');
        $this->addSql('ALTER TABLE asso_intervention_equipement DROP FOREIGN KEY FK_E1B6D8EE8A1631E1');
        $this->addSql('ALTER TABLE posseder DROP FOREIGN KEY FK_62EF7CBA9DE54682');
        $this->addSql('ALTER TABLE verification_equipement_operationnel DROP FOREIGN KEY FK_4AFF8D2AB9FB4684');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A575180AA129');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa DROP FOREIGN KEY FK_238A0CF82228D6AE');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649900D5BD');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649595AAE34');
        $this->addSql('ALTER TABLE commander DROP FOREIGN KEY FK_42D318BAAA854176');
        $this->addSql('ALTER TABLE intervenir DROP FOREIGN KEY FK_DFB079928EAE3863');
        $this->addSql('ALTER TABLE intervention_materiel DROP FOREIGN KEY FK_2541CB328EAE3863');
        $this->addSql('ALTER TABLE intervention_technique DROP FOREIGN KEY FK_98D9F9688EAE3863');
        $this->addSql('ALTER TABLE asso_intervention_equipement DROP FOREIGN KEY FK_E1B6D8EE334423FF');
        $this->addSql('ALTER TABLE photos DROP FOREIGN KEY FK_876E0D997B76BA2');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7FC35F0816');
        $this->addSql('ALTER TABLE commander DROP FOREIGN KEY FK_42D318BAB207F89C');
        $this->addSql('ALTER TABLE intervention_materiel DROP FOREIGN KEY FK_2541CB3216880AAF');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7F87D377BB');
        $this->addSql('ALTER TABLE regarder DROP FOREIGN KEY FK_CB13CFC01DD39950');
        $this->addSql('ALTER TABLE equipement_operationnel DROP FOREIGN KEY FK_F38E91D26C6E55B5');
        $this->addSql('ALTER TABLE equipement_operationnel DROP FOREIGN KEY FK_F38E91D2D13D99F3');
        $this->addSql('ALTER TABLE equipement_operationnel DROP FOREIGN KEY FK_F38E91D23BDFB20B');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa DROP FOREIGN KEY FK_238A0CF8F6203804');
        $this->addSql('ALTER TABLE intervention_technique DROP FOREIGN KEY FK_98D9F9681F8ACB26');
        $this->addSql('ALTER TABLE equipement_operationnel DROP FOREIGN KEY FK_F38E91D28CDE5729');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE698CDE5729');
        $this->addSql('ALTER TABLE centres_secours DROP FOREIGN KEY FK_354EB4FE8CDE5729');
        $this->addSql('ALTER TABLE commandes DROP FOREIGN KEY FK_35D4282C665DA613');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A5751C109075');
        $this->addSql('ALTER TABLE fiche_individuelle_fmpa DROP FOREIGN KEY FK_238A0CF81C109075');
        $this->addSql('ALTER TABLE intervenir DROP FOREIGN KEY FK_DFB07992A76ED395');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7F55AB140');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD3995055AB140');
        $this->addSql('ALTER TABLE posseder DROP FOREIGN KEY FK_62EF7CBAD8A75A2B');
        $this->addSql('ALTER TABLE regarder DROP FOREIGN KEY FK_CB13CFC08D93D649');
        $this->addSql('ALTER TABLE verification_equipement_operationnel DROP FOREIGN KEY FK_4AFF8D2AF7C79AFB');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7F43C3D9C3');
        $this->addSql('ALTER TABLE repertoire DROP FOREIGN KEY FK_3C367876F6F83C8A');
        $this->addSql('ALTER TABLE repertoire DROP FOREIGN KEY FK_3C367876942036BF');
        $this->addSql('DROP TABLE categorierepertoire');
        $this->addSql('DROP TABLE centres');
        $this->addSql('DROP TABLE centres_secours');
        $this->addSql('DROP TABLE commander');
        $this->addSql('DROP TABLE commandes');
        $this->addSql('DROP TABLE ctd');
        $this->addSql('DROP TABLE documents');
        $this->addSql('DROP TABLE equipement_operationnel');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE fiche_individuelle_fmpa');
        $this->addSql('DROP TABLE filiere');
        $this->addSql('DROP TABLE fmpa');
        $this->addSql('DROP TABLE fonction_user');
        $this->addSql('DROP TABLE grade_user');
        $this->addSql('DROP TABLE intervenir');
        $this->addSql('DROP TABLE intervention_materiel');
        $this->addSql('DROP TABLE intervention_technique');
        $this->addSql('DROP TABLE interventions');
        $this->addSql('DROP TABLE asso_intervention_equipement');
        $this->addSql('DROP TABLE lieux_interventions');
        $this->addSql('DROP TABLE materiels');
        $this->addSql('DROP TABLE motif');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE news_archive');
        $this->addSql('DROP TABLE nom_equipement_operationnel');
        $this->addSql('DROP TABLE periodicite_equipement_operationnel');
        $this->addSql('DROP TABLE photos');
        $this->addSql('DROP TABLE posseder');
        $this->addSql('DROP TABLE regarder');
        $this->addSql('DROP TABLE repertoire');
        $this->addSql('DROP TABLE statut_fmpa');
        $this->addSql('DROP TABLE technique');
        $this->addSql('DROP TABLE typemateriel');
        $this->addSql('DROP TABLE types_centres_secours');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE verification_equipement_operationnel');
        $this->addSql('DROP TABLE ville');
    }
}
