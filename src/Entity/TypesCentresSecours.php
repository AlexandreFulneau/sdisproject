<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TypesCentresSecours
 *
 * @ORM\Table(name="types_centres_secours")
 * @ORM\Entity
 * @UniqueEntity(fields={"nom"}, message="Ce nom éxiste déjà !")
 */
class TypesCentresSecours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_type_cs", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTypeCs;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="classement", type="integer", nullable=true, options={"default"="1"})
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     * @Assert\Positive(message = "Le classement doit être positif.")
     */
    private $classement = 1;

    public function getIdTypeCs(): ?int
    {
        return $this->idTypeCs;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getClassement(): ?int
    {
        return $this->classement;
    }

    public function setClassement(?int $classement): self
    {
        $this->classement = $classement;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNom();
    }

}
