<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Repertoire
 *
 * @ORM\Table(name="repertoire", indexes={@ORM\Index(name="categorie", columns={"categorie"}), @ORM\Index(name="villePrincipale_fk", columns={"villePrincipale"}), @ORM\Index(name="villeSecondaire_fk", columns={"villeSecondaire"})})
 * @ORM\Entity
 */
class Repertoire
{
    /**
     * @var int
     *
     * @ORM\Column(name="idrepertoire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrepertoire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="grade", type="string", length=255, nullable=true)
     */
    private $grade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     * @Assert\Length(min = 10, max = 10, minMessage = "min_lenght", maxMessage = "max_lenght")
     * @Assert\Regex(pattern="/^0[1-9][0-9]{8}$/", message="number_only")
     */
    private $tel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TelephoneFixe", type="string", length=255, nullable=true)
     * @Assert\Length(min = 10, max = 10, minMessage = "min_lenght", maxMessage = "max_lenght")
     * @Assert\Regex(pattern="/^0[1-9][0-9]{8}$/", message="number_only")
     */
    private $telephonefixe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(message = "L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Specialite", type="string", length=255, nullable=true)
     *
     */
    private $specialite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaires", type="text", length=65535, nullable=true)
     */
    private $commentaires;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adressePrincipale", type="string", length=255, nullable=true)
     */
    private $adresseprincipale;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresseSecondaire", type="string", length=255, nullable=true)
     */
    private $adressesecondaire;

    /**
     * @var \Ville
     *
     * @ORM\ManyToOne(targetEntity="Ville")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="villePrincipale", referencedColumnName="idville")
     * })
     */
    private $villeprincipale;

    /**
     * @var \Ville
     *
     * @ORM\ManyToOne(targetEntity="Ville")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="villeSecondaire", referencedColumnName="idville")
     * })
     */
    private $villesecondaire;

    /**
     * @var \Categorierepertoire
     *
     * @ORM\ManyToOne(targetEntity="Categorierepertoire")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     * })
     */
    private $categorie;

    public function getIdrepertoire(): ?int
    {
        return $this->idrepertoire;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(?string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getTelephonefixe(): ?string
    {
        return $this->telephonefixe;
    }

    public function setTelephonefixe(?string $telephonefixe): self
    {
        $this->telephonefixe = $telephonefixe;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSpecialite(): ?string
    {
        return $this->specialite;
    }

    public function setSpecialite(?string $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getAdresseprincipale(): ?string
    {
        return $this->adresseprincipale;
    }

    public function setAdresseprincipale(?string $adresseprincipale): self
    {
        $this->adresseprincipale = $adresseprincipale;

        return $this;
    }

    public function getAdressesecondaire(): ?string
    {
        return $this->adressesecondaire;
    }

    public function setAdressesecondaire(?string $adressesecondaire): self
    {
        $this->adressesecondaire = $adressesecondaire;

        return $this;
    }

    public function getVilleprincipale(): ?Ville
    {
        return $this->villeprincipale;
    }

    public function setVilleprincipale(?Ville $villeprincipale): self
    {
        $this->villeprincipale = $villeprincipale;

        return $this;
    }

    public function getVillesecondaire(): ?Ville
    {
        return $this->villesecondaire;
    }

    public function setVillesecondaire(?Ville $villesecondaire): self
    {
        $this->villesecondaire = $villesecondaire;

        return $this;
    }

    public function getCategorie(): ?Categorierepertoire
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorierepertoire $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }



}
