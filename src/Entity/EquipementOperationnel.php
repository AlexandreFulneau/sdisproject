<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EquipementOperationnel
 *
 * @ORM\Table(name="equipement_operationnel", indexes={@ORM\Index(name="centres_secours", columns={"centres_secours"}), @ORM\Index(name="duree_vie", columns={"duree_vie"}), @ORM\Index(name="nom", columns={"nom"}), @ORM\Index(name="periodicite", columns={"periodicite"}), @ORM\Index(name="type", columns={"type"})})
 * @ORM\Entity
 */
class EquipementOperationnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=255, nullable=true)
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="date", nullable=false)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_prochaine_verification", type="date", nullable=false)
     */
    private $dateProchaineVerification;

    /**
     * @var int|null
     *
     * @ORM\Column(name="min_pression", type="integer", nullable=true)
     */
    private $minPression;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pression", type="integer", nullable=true)
     */
    private $pression;

    /**
     * @var string|null
     *
     * @ORM\Column(name="QR_code", type="text", length=65535, nullable=true)
     */
    private $qrCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="disponible", type="boolean", nullable=false, options={"default"="1"})
     */
    private $disponible = true;

    /**
     * @var NomEquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="NomEquipementOperationnel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nom", referencedColumnName="id_nom_equipement")
     * })
     */
    private $nom;

    /**
     * @var Typemateriel
     *
     * @ORM\ManyToOne(targetEntity="Typemateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="idtypemateriel")
     * })
     */
    private $type;

    /**
     * @var PeriodiciteEquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="PeriodiciteEquipementOperationnel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="periodicite", referencedColumnName="id_periodicite")
     * })
     * @Assert\NotBlank
     */
    private $periodicite;

    /**
     * @var PeriodiciteEquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="PeriodiciteEquipementOperationnel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="duree_vie", referencedColumnName="id_periodicite")
     * })
     * @Assert\NotBlank
     */
    private $dureeVie;

    /**
     * @var CentresSecours
     *
     * @ORM\ManyToOne(targetEntity="CentresSecours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="centres_secours", referencedColumnName="id")
     * })
     */
    private $centresSecours;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="VerificationEquipementOperationnel",mappedBy="nomEquipement")
     */
    private $verifications;

     /**
     * @var PersistentCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Interventions", mappedBy="equipements")
     */
    private $interventions;

    /**
     * EquipementOperationnel constructor.
     */
    public function __construct()
    {
        $this->verifications = new ArrayCollection();
        

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(? \DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateProchaineVerification(): ?\DateTimeInterface
    {
        return $this->dateProchaineVerification;
    }

    public function setDateProchaineVerification(\DateTimeInterface $dateProchaineVerification): self
    {
        $this->dateProchaineVerification = $dateProchaineVerification;

        return $this;
    }

    public function getMinPression(): ?int
    {
        return $this->minPression;
    }

    public function setMinPression(?int $minPression): self
    {
        $this->minPression = $minPression;

        return $this;
    }

    public function getPression(): ?int
    {
        return $this->pression;
    }

    public function setPression(?int $pression): self
    {
        $this->pression = $pression;

        return $this;
    }

    public function getQrCode(): ?string
    {
        return $this->qrCode;
    }

    public function setQrCode(?string $qrCode): self
    {
        $this->qrCode = $qrCode;

        return $this;
    }

    public function getDisponible(): ?bool
    {
        return $this->disponible;
    }

    public function setDisponible(bool $disponible): self
    {
        $this->disponible = $disponible;

        return $this;
    }

    public function getNom(): ?NomEquipementOperationnel
    {
        return $this->nom;
    }

    public function setNom(?NomEquipementOperationnel $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?Typemateriel
    {
        return $this->type;
    }

    public function setType(?Typemateriel $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPeriodicite(): ?PeriodiciteEquipementOperationnel
    {
        return $this->periodicite;
    }

    public function setPeriodicite(?PeriodiciteEquipementOperationnel $periodicite): self
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    public function getDureeVie(): ?PeriodiciteEquipementOperationnel
    {
        return $this->dureeVie;
    }

    public function setDureeVie(?PeriodiciteEquipementOperationnel $dureeVie): self
    {
        $this->dureeVie = $dureeVie;

        return $this;
    }

    public function getCentresSecours(): ?CentresSecours
    {
        return $this->centresSecours;
    }

    public function setCentresSecours(?CentresSecours $centresSecours): self
    {
        $this->centresSecours = $centresSecours;

        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getVerifications(): PersistentCollection
    {
        return $this->verifications;
    }

    /**
     * @param VerificationEquipementOperationnel $verification
     * @return EquipementOperationnel
     */
    public function addVerification(VerificationEquipementOperationnel $verification): EquipementOperationnel
    {
        $this->verifications->add($verification);
        $verification->setNomEquipement($this);
        return $this;
    }

    /**
     * @param VerificationEquipementOperationnel $verification
     * @return EquipementOperationnel
     */
    public function removeVerification(VerificationEquipementOperationnel $verification): EquipementOperationnel
    {
        $this->verifications->removeElement($verification);
        $verification->setNomEquipement(null);
        return $this;
    }

    
      /**
     * @return PersistentCollection
     */
    public function getInterventions(): PersistentCollection
    {
        return $this->interventions;
    }

    /**
     * @param Interventions $intervention
     * @return $this
     */
    public function addIntervention(Interventions $intervention): self
    {
        $this->interventions->add($intervention);
        $intervention->addEquipement($this);
        return $this;
    }

    /**
     * @param Interventions $intervention
     * @return $this
     */
    public function removeIntervention(Interventions $intervention): self
    {
        $this->interventions->removeElement($intervention);
        $intervention->removeEquipement($this);
        return $this;
    }

    public function __toString() : string
    {
        $epi_name = $this->getNom()->getNom();

        if ($this->getNumero())
            $epi_name .= ' N° ' . $this->getNumero();

        return $epi_name;
    }
}
