<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CentresSecours
 *
 * @ORM\Table(name="centres_secours", indexes={@ORM\Index(name="type", columns={"type"})})
 * @ORM\Entity(repositoryClass="App\Repository\CentresSecoursRepository")
 */
class CentresSecours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $statut;

    /**
     * @var TypesCentresSecours
     *
     * @ORM\ManyToOne(targetEntity="TypesCentresSecours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="id_type_cs")
     * })
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $type;

      /**
     * @var string|null
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    public function getid(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(? string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getType(): ?TypesCentresSecours
    {
        return $this->type;
    }

    public function setType(?TypesCentresSecours $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNom();
    }


    /**
     * Get the value of adresse
     *
     * @return  string|null
     */ 
    public function getAdresse() : ?string
    {
        return $this->adresse;
    }

    /**
     * Set the value of adresse
     *
     * @param  string|null  $adresse
     *
     * @return  self
     */ 
    public function setAdresse(?string $adresse) : self
    {
        $this->adresse = $adresse;

        return $this;
    }
}
