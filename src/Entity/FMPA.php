<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FMPARepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=FMPARepository::class)
 */
class FMPA
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

     /**
     * @ORM\OneToMany(targetEntity=FicheIndividuelleFMPA::class, mappedBy="fmpa", orphanRemoval=true)
     */
    private $fichesIndividuellesFMPA;

    public function __construct()
    {
        $this->fichesIndividuellesFMPA = new ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

     /**
     * @return Collection|FicheIndividuelleFMPA[]
     */
    public function getFichesIndividuellesFMPA(): Collection
    {
        return $this->fichesIndividuellesFMPA;
    }

    public function addFichesIndividuellesFMPA(FicheIndividuelleFMPA $ficheIndividuelleFMPA): self
    {
        if (!$this->fichesIndividuellesFMPA->contains($ficheIndividuelleFMPA)) {
            $this->fichesIndividuellesFMPA[] = $ficheIndividuelleFMPA;
            $ficheIndividuelleFMPA->setFmpa($this);
        }

        return $this;
    }

    public function removeFichesIndividuellesFMPA(FicheIndividuelleFMPA $ficheIndividuelleFMPA): self
    {
        if ($this->fichesIndividuellesFMPA->removeElement($ficheIndividuelleFMPA)) {
            // set the owning side to null (unless already changed)
            if ($ficheIndividuelleFMPA->getFmpa() === $this) {
                $ficheIndividuelleFMPA->setFmpa(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }
}
