<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Materiels
 *
 * @ORM\Table(name="materiels", indexes={@ORM\Index(name="type", columns={"type"})})
 * @ORM\Entity
 */
class Materiels
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="stock", type="integer", nullable=false)
     * @Assert\Positive(message = "Veuillez saisir une valeur positive")
     */
    private $stock;

    /**
     * @var Typemateriel
     *
     * @ORM\ManyToOne(targetEntity="Typemateriel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="idtypemateriel")
     * })
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getType(): ?Typemateriel
    {
        return $this->type;
    }

    public function setType(?Typemateriel $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}


