<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FonctionUser
 *
 * @ORM\Table(name="fonction_user")
 * @ORM\Entity(repositoryClass="App\Repository\FonctionUserRepository")
 * @UniqueEntity(fields={"nom"}, message="Ce nom éxiste déjà !")
 */
class FonctionUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_fonction", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFonction;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="classement", type="integer", nullable=true, options={"default"="1"})
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     * @Assert\Positive(message = "Le classement doit être positif.")
     */
    private $classement = 1;

    public function getIdFonction(): ?int
    {
        return $this->idFonction;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getClassement(): ?int
    {
        return $this->classement;
    }

    public function setClassement(?int $classement): self
    {
        $this->classement = $classement;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNom();
    }

}
