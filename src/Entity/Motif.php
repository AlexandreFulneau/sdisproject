<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Motif
 *
 * @ORM\Table(name="motif")
 * @ORM\Entity
 * @UniqueEntity(fields={"libelle"}, message="Ce nom éxiste déjà !")
 */
class Motif
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmotif", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmotif;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     *
     */
    private $libelle;

    public function getIdmotif(): ?int
    {
        return $this->idmotif;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }
}
