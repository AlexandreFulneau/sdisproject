<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PeriodiciteEquipementOperationnel
 *
 * @ORM\Table(name="periodicite_equipement_operationnel")
 * @ORM\Entity
 */
class PeriodiciteEquipementOperationnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_periodicite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPeriodicite;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $duree;

    public function getIdPeriodicite(): ?int
    {
        return $this->idPeriodicite;
    }

    public function getDuree(): ?string
    {
        return $this->duree;
    }

    public function setDuree(?string $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getDuree();
    }

}
