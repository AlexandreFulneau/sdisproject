<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * VerificationEquipementOperationnel
 *
 * @ORM\Table(name="verification_equipement_operationnel", indexes={@ORM\Index(name="nom_epi", columns={"nom_equipement"}), @ORM\Index(name="verificateur", columns={"verificateur"})})
 * @ORM\Entity
 */
class VerificationEquipementOperationnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_verification_equipement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idVerificationEquipement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=false)
     */
    private $commentaire;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="verificateur", referencedColumnName="id")
     * })
     * @Assert\NotBlank
     */
    private $verificateur;

    /**
     * @var EquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="EquipementOperationnel", inversedBy="verifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nom_equipement", referencedColumnName="id")
     * })
     */
    private $nomEquipement;

    public function getIdVerificationEquipement(): ?int
    {
        return $this->idVerificationEquipement;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getVerificateur(): ?User
    {
        return $this->verificateur;
    }

    public function setVerificateur(?User $verificateur): self
    {
        $this->verificateur = $verificateur;

        return $this;
    }

    public function getNomEquipement(): ?EquipementOperationnel
    {
        return $this->nomEquipement;
    }

    public function setNomEquipement(?EquipementOperationnel $nomEquipement): self
    {
        $this->nomEquipement = $nomEquipement;

        return $this;
    }


    public function __toString() : string
    {
        return $this->getDate()->format('d/m/Y');
    }
}
