<?php

namespace App\Entity;

use App\Repository\EvaluationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EvaluationRepository::class)
 */
class Evaluation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="evaluations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnel;

    /**
     * @ORM\ManyToOne(targetEntity=CentresSecours::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $centre;

    /**
     * @ORM\ManyToOne(targetEntity=CTD::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $ctd;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbInterventions;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbHeuresFmpa;

    /**
     * @ORM\ManyToOne(targetEntity=Filiere::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $filiere;

    /**
     * @ORM\Column(type="text")
     */
    private $objectifAnneeEnCours;

    /**
     * @ORM\Column(type="text")
     */
    private $competenceOperationnelle;

    /**
     * @ORM\Column(type="text")
     */
    private $implication;

    /**
     * @ORM\Column(type="text")
     */
    private $critereAnneeEnCours;

    /**
     * @ORM\Column(type="text")
     */
    private $bilan;

    /**
     * @ORM\Column(type="text")
     */
    private $objectifAnneeSuivante;

    /**
     * @ORM\Column(type="text")
     */
    private $conditionReussiteAnneeSuivante;

    /**
     * @ORM\Column(type="text")
     */
    private $critereAnneeSuivante;

    /**
     * @ORM\Column(type="text")
     */
    private $delaisRealisationAnneeSuivante;

    /**
     * @ORM\Column(type="text")
     */
    private $commentaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonnel(): ?User
    {
        return $this->personnel;
    }

    public function setPersonnel(?User $personnel): self
    {
        $this->personnel = $personnel;

        return $this;
    }

    public function getCentre(): ?CentresSecours
    {
        return $this->centre;
    }

    public function setCentre(?CentresSecours $centre): self
    {
        $this->centre = $centre;

        return $this;
    }

    public function getCtd(): ?CTD
    {
        return $this->ctd;
    }

    public function setCtd(?CTD $ctd): self
    {
        $this->ctd = $ctd;

        return $this;
    }

    public function getNbInterventions(): ?int
    {
        return $this->nbInterventions;
    }

    public function setNbInterventions(int $nbInterventions): self
    {
        $this->nbInterventions = $nbInterventions;

        return $this;
    }

    public function getNbHeuresFmpa(): ?int
    {
        return $this->nbHeuresFmpa;
    }

    public function setNbHeuresFmpa(int $nbHeuresFmpa): self
    {
        $this->nbHeuresFmpa = $nbHeuresFmpa;

        return $this;
    }

    public function getFiliere(): ?Filiere
    {
        return $this->filiere;
    }

    public function setFiliere(?Filiere $filiere): self
    {
        $this->filiere = $filiere;

        return $this;
    }

    public function getObjectifAnneeEnCours(): ?string
    {
        return $this->objectifAnneeEnCours;
    }

    public function setObjectifAnneeEnCours(string $objectifAnneeEnCours): self
    {
        $this->objectifAnneeEnCours = $objectifAnneeEnCours;

        return $this;
    }

    public function getCompetenceOperationnelle(): ?string
    {
        return $this->competenceOperationnelle;
    }

    public function setCompetenceOperationnelle(string $competenceOperationnelle): self
    {
        $this->competenceOperationnelle = $competenceOperationnelle;

        return $this;
    }

    public function getImplication(): ?string
    {
        return $this->implication;
    }

    public function setImplication(string $implication): self
    {
        $this->implication = $implication;

        return $this;
    }

    public function getCritereAnneeEnCours(): ?string
    {
        return $this->critereAnneeEnCours;
    }

    public function setCritereAnneeEnCours(string $critereAnneeEnCours): self
    {
        $this->critereAnneeEnCours = $critereAnneeEnCours;

        return $this;
    }

    public function getBilan(): ?string
    {
        return $this->bilan;
    }

    public function setBilan(string $bilan): self
    {
        $this->bilan = $bilan;

        return $this;
    }

    public function getObjectifAnneeSuivante(): ?string
    {
        return $this->objectifAnneeSuivante;
    }

    public function setObjectifAnneeSuivante(string $objectifAnneeSuivante): self
    {
        $this->objectifAnneeSuivante = $objectifAnneeSuivante;

        return $this;
    }

    public function getConditionReussiteAnneeSuivante(): ?string
    {
        return $this->conditionReussiteAnneeSuivante;
    }

    public function setConditionReussiteAnneeSuivante(string $conditionReussiteAnneeSuivante): self
    {
        $this->conditionReussiteAnneeSuivante = $conditionReussiteAnneeSuivante;

        return $this;
    }

    public function getCritereAnneeSuivante(): ?string
    {
        return $this->critereAnneeSuivante;
    }

    public function setCritereAnneeSuivante(string $critereAnneeSuivante): self
    {
        $this->critereAnneeSuivante = $critereAnneeSuivante;

        return $this;
    }

    public function getDelaisRealisationAnneeSuivante(): ?string
    {
        return $this->delaisRealisationAnneeSuivante;
    }

    public function setDelaisRealisationAnneeSuivante(string $delaisRealisationAnneeSuivante): self
    {
        $this->delaisRealisationAnneeSuivante = $delaisRealisationAnneeSuivante;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate() : ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate(\DateTimeInterface $date) : self
    {
        $this->date = $date;

        return $this;
    }
}
