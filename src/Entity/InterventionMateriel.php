<?php

namespace App\Entity;

use App\Repository\InterventionMaterielRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InterventionMaterielRepository::class)
 */
class InterventionMateriel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive(message = "Veuillez saisir une valeur positive")
     */
    private $stock;

     /**
     * @var Interventions
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Interventions", inversedBy="materielsInter")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE" )
     * 
     */
    private $intervention;

    /**
     * @var Materiels
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Materiels")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * 
     */
    private $materiel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get the value of materiel
     *
     * @return  Materiels
     */ 
    public function getMateriel() : ?Materiels
    {
        return $this->materiel;
    }

    /**
     * Set the value of materiel
     *
     * @param  Materiels  $materiel
     *
     * @return  self
     */ 
    public function setMateriel(?Materiels $materiel) : self
    {
        $this->materiel = $materiel;

        return $this;
    }

    /**
     * Get the value of intervention
     *
     * @return  Interventions
     */ 
    public function getIntervention() : ?Interventions
    {
        return $this->intervention;
    }

    /**
     * Set the value of intervention
     *
     * @param  Interventions  $intervention
     *
     * @return  self
     */ 
    public function setIntervention(? Interventions $intervention) : self
    {
        $this->intervention = $intervention;

        return $this;
    }
}
