<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regarder
 *
 * @ORM\Table(name="regarder", indexes={@ORM\Index(name="new", columns={"news"}), @ORM\Index(name="user", columns={"user"})})
 * @ORM\Entity
 */
class Regarder
{
    /**
     * @var int
     *
     * @ORM\Column(name="idregarder", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idregarder;

    /**
     * @var bool
     *
     * @ORM\Column(name="vue", type="boolean", nullable=false)
     */
    private $vue;

    /**
     * @var \News
     *
     * @ORM\ManyToOne(targetEntity="News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news", referencedColumnName="idnews", onDelete="CASCADE")
     * })
     */
    private $news;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user", referencedColumnName="id")
     * })
     */
    private $user;

    public function getIdregarder(): ?int
    {
        return $this->idregarder;
    }

    public function getVue(): ?bool
    {
        return $this->vue;
    }

    public function setVue(bool $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getNews(): ?News
    {
        return $this->news;
    }

    public function setNews(?News $news): self
    {
        $this->news = $news;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


}
