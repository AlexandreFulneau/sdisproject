<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsArchive
 *
 * @ORM\Table(name="news_archive")
 * @ORM\Entity(repositoryClass="App\Repository\NewsArchiveRepository")
 */
class NewsArchive
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_news_archive", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idNewsArchive;

    /**
     * @var string
     *
     * @ORM\Column(name="news_nom", type="string", length=255, nullable=false)
     */
    private $newsNom;

    /**
     * @var string
     *
     * @ORM\Column(name="news_chemin", type="string", length=255, nullable=false)
     */
    private $newsChemin;

    /**
     * @var string
     *
     * @ORM\Column(name="news_dossier", type="string", length=255, nullable=false)
     */
    private $newsDossier;

    public function getIdNewsArchive(): ?int
    {
        return $this->idNewsArchive;
    }

    public function getNewsNom(): ?string
    {
        return $this->newsNom;
    }

    public function setNewsNom(string $newsNom): self
    {
        $this->newsNom = $newsNom;

        return $this;
    }

    public function getNewsChemin(): ?string
    {
        return $this->newsChemin;
    }

    public function setNewsChemin(string $newsChemin): self
    {
        $this->newsChemin = $newsChemin;

        return $this;
    }

    public function getNewsDossier(): ?string
    {
        return $this->newsDossier;
    }

    public function setNewsDossier(string $newsDossier): self
    {
        $this->newsDossier = $newsDossier;

        return $this;
    }


}
