<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commandes
 *
 * @ORM\Table(name="commandes", indexes={@ORM\Index(name="centre", columns={"centre"}), @ORM\Index(name="demandeur", columns={"demandeur"})})
 * @ORM\Entity
 */
class Commandes
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcommande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcommande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecommande", type="date", nullable=false)
     */
    private $datecommande;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="demandeur", referencedColumnName="id")
     * })
     */
    private $demandeur;

    /**
     * @var \Centres
     *
     * @ORM\ManyToOne(targetEntity="Centres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="centre", referencedColumnName="idcentre")
     * })
     */
    private $centre;

    public function getIdcommande(): ?int
    {
        return $this->idcommande;
    }

    public function getDatecommande(): ?\DateTimeInterface
    {
        return $this->datecommande;
    }

    public function setDatecommande(\DateTimeInterface $datecommande): self
    {
        $this->datecommande = $datecommande;

        return $this;
    }

    public function getDemandeur(): ?User
    {
        return $this->demandeur;
    }

    public function setDemandeur(?User $demandeur): self
    {
        $this->demandeur = $demandeur;

        return $this;
    }

    public function getCentre(): ?Centres
    {
        return $this->centre;
    }

    public function setCentre(?Centres $centre): self
    {
        $this->centre = $centre;

        return $this;
    }


}
