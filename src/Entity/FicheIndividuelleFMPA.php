<?php

namespace App\Entity;

use App\Repository\FicheIndividuelleFMPARepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FicheIndividuelleFMPARepository::class)
 */
class FicheIndividuelleFMPA
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="fichesIndividuellesFMPA")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personnel;

    /**
     * @ORM\ManyToOne(targetEntity=StatutFMPA::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $statut;

      /**
     * @ORM\ManyToOne(targetEntity=FMPA::class, inversedBy="fichesIndividuellesFMPA")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fmpa;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbHeures;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sujet;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lieux;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $tempsImmersion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonnel(): ?User
    {
        return $this->personnel;
    }

    public function setPersonnel(?User $personnel): self
    {
        $this->personnel = $personnel;

        return $this;
    }

    public function getStatut(): ?StatutFMPA
    {
        return $this->statut;
    }

    public function setStatut(?StatutFMPA $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getNbHeures(): ?int
    {
        return $this->nbHeures;
    }

    public function setNbHeures(int $nbHeures): self
    {
        $this->nbHeures = $nbHeures;

        return $this;
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): self
    {
        $this->sujet = $sujet;

        return $this;
    }

    public function getLieux(): ?string
    {
        return $this->lieux;
    }

    public function setLieux(string $lieux): self
    {
        $this->lieux = $lieux;

        return $this;
    }

    public function getTempsImmersion(): ?\DateTimeInterface
    {
        return $this->tempsImmersion;
    }

    public function setTempsImmersion(?\DateTimeInterface $tempsImmersion): self
    {
        $this->tempsImmersion = $tempsImmersion;

        return $this;
    }

    /**
     * Get the value of fmpa
     */ 
    public function getFmpa() : ?FMPA
    {
        return $this->fmpa;
    }

    /**
     * Set the value of fmpa
     *
     * @return  self
     */ 
    public function setFmpa($fmpa) :self
    {
        $this->fmpa = $fmpa;

        return $this;
    }
}
