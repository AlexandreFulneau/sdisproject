<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Interventions
 *
 * @ORM\Table(name="interventions", uniqueConstraints={@ORM\UniqueConstraint(name="num_intervention", columns={"num_intervention"})}, indexes={@ORM\Index(name="adresse", columns={"adresse"}), @ORM\Index(name="motif", columns={"motif"}), @ORM\Index(name="ville", columns={"ville"})})
 * @ORM\Entity
 * @UniqueEntity(fields={"numIntervention"}, message="Le numéro d'intervention est déjà utilisé")
 */
class Interventions
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="num_intervention", type="integer", nullable=true)
     * @Assert\Positive(message="Valeur supérieur à 0 requise")
     * @Assert\NotBlank(message="Valeur requise")
     */
    private $numIntervention;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var Motif
     *
     * @ORM\ManyToOne(targetEntity="Motif")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="motif", referencedColumnName="idmotif")
     * })
     * @Assert\NotBlank(message="Valeur requise")
     */
    private $motif;

    /**
     * @var Ville
     *
     * @ORM\ManyToOne(targetEntity="Ville")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ville", referencedColumnName="idville")
     * })
     * @Assert\NotBlank(message="Valeur requise")
     */
    private $ville;

    /**
     * @var LieuxInterventions
     *
     * @ORM\ManyToOne(targetEntity="LieuxInterventions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="adresse", referencedColumnName="id_lieux_interventions")
     * })
     *@Assert\NotBlank(message="Valeur requise")
     */
    private $adresse;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="App\Entity\InterventionTechnique", mappedBy="intervention",cascade={"persist"}, orphanRemoval="true")
     * @Assert\Valid
     */
    private $techniquesInter;

    /**
     * @var PersistentCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\EquipementOperationnel", inversedBy="interventions")
     * @ORM\JoinTable(name="asso_intervention_equipement")
     */
    private $equipements;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="App\Entity\InterventionMateriel", mappedBy="intervention",cascade={"persist"}, orphanRemoval="true")
     * 
     * @Assert\Valid
     */
    private $materielsInter;

     /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Intervenir", mappedBy="intervention",cascade={"persist"}, orphanRemoval="true")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "Intervenant requis"
     * )
     * @Assert\Valid
     */
    private $usersInter;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="Photos",mappedBy="idintervention")
     */
    private $photos;

      /**
     * @var string
     *
     * @ORM\Column(name="recapitulatif", type="text", length=65535, nullable=true)
     */
    private $recapitulatif;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="auteur", referencedColumnName="id")
     * })
     */
    private $auteur;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->equipements = new ArrayCollection();
        $this->techniquesInter = new ArrayCollection();
        $this->materielsInter = new ArrayCollection();
        $this->usersInter = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumIntervention(): ?int
    {
        return $this->numIntervention;
    }

    public function setNumIntervention(?int $numIntervention): self
    {
        $this->numIntervention = $numIntervention;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMotif(): ?Motif
    {
        return $this->motif;
    }

    public function setMotif(?Motif $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?LieuxInterventions
    {
        return $this->adresse;
    }

    public function setAdresse(?LieuxInterventions $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getRecapitulatif(): ?string
    {
        return $this->recapitulatif;
    }

    public function setRecapitulatif(string $recapitulatif): self
    {
        $this->recapitulatif = $recapitulatif;

        return $this;
    }

    public function getAuteur(): ?User
    {
        return $this->auteur;
    }

    public function setAuteur(?User $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getPhotos(): PersistentCollection
    {
        return $this->photos;
    }

    /**
     * @param Photos $photo
     * @return Interventions
     */
    public function addPhoto(Photos $photo): Interventions
    {
        $this->photos->add($photo);
        $photo->setIdintervention($this);
        return $this;
    }

    /**
     * @param Photos $photo
     * @return Interventions
     */
    public function removePhoto(Photos $photo): Interventions
    {
        $this->photos->removeElement($photo);
        $photo->setIdintervention(null);
        return $this;
    }

    
    /**
     * @return PersistentCollection
     */
    public function getEquipements(): Collection
    {
        return $this->equipements;
    }

    /**
     * @param EquipementOperationnel $equipement
     * @return $this
     */
    public function addEquipement(EquipementOperationnel $equipement): self
    {
        $this->equipements->add($equipement);
        return $this;
    }

    /**
     * @param EquipementOperationnel $equipement
     * @return $this
     */
    public function removeEquipement(EquipementOperationnel $equipement): self
    {
        $this->equipements->removeElement($equipement);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getUsersInter(): Collection
    {
        return $this->usersInter;
    }

    
    function setUsersInter(Collection $usersInter) {
        
        
        foreach($this->usersInter as $userInter){
            $this->removeUserInter($userInter);
            
        }

        foreach($usersInter as $userInter){
            if($this->removeNewUserIsAlreadyPersistOrNot($usersInter, $userInter) == false){
                $this->addUserInter($userInter);
            };
        }
         
        return $this;
    }


    /**
     * @param Intervenir $userInter
     * @return $this
     */
    public function addUserInter(Intervenir $userInter): self
    {
        $this->usersInter[] = $userInter;
        $userInter->setIntervention($this);
        
        return $this;
    }

    /**
     * @param Intervenir $userInter
     * @return $this
     */
    public function removeUserInter(Intervenir $userInter): self
    {
        if($this->usersInter->removeElement($userInter)){
            if ($userInter->getIntervention() === $this){
                $userInter->setIntervention(null);
            }
        }

        return $this;
    }


    private function removeNewUserIsAlreadyPersistOrNot(Collection $usersInter, Intervenir $userInter): bool
    {
        foreach($this->getUsersInter() as $userInterAlreadyPersist){
            if($userInterAlreadyPersist->getUser()->getId() == $userInter->getUser()->getId()) {    
                $usersInter->removeElement($userInter);
                return true; 
                 
            }
        }
         
       
        return false;
    } 


    /**
     * @return Collection
     */
    public function getTechniquesInter(): Collection
    {
        return $this->techniquesInter;
    }

    
    function setTechniquesInter(Collection $techniquesInter) {
        
        
        foreach($this->techniquesInter as $techniqueInter){
            $this->removeTechniqueInter($techniqueInter);
            
        }

        foreach($techniquesInter as $techniqueInter){
            if($this->removeNewTechniqueIsAlreadyPersistOrNot($techniquesInter, $techniqueInter) == false){
                $this->addTechniqueInter($techniqueInter);
            };
        }
         
        return $this;
    }


    /**
     * @param InterventionTechnique $techniqueInter
     * @return $this
     */
    public function addTechniqueInter(InterventionTechnique $techniqueInter): self
    {
        $this->techniquesInter[] = $techniqueInter;
        $techniqueInter->setIntervention($this);
        
        return $this;
    }

    /**
     * @param InterventionTechnique $techniqueInter
     * @return $this
     */
    public function removeTechniqueInter(InterventionTechnique $techniqueInter): self
    {
        if($this->techniquesInter->removeElement($techniqueInter)){
            if ($techniqueInter->getIntervention() === $this){
                $techniqueInter->setIntervention(null);
            }
        }

        return $this;
    }

    private function removeNewTechniqueIsAlreadyPersistOrNot(Collection $techniquesInter, InterventionTechnique $techniqueInter): bool
    {
        foreach($this->getTechniquesInter() as $techniqueInterAlreadyPersist){
            if($techniqueInterAlreadyPersist->getTechnique()->getId() == $techniqueInter->getTechnique()->getId()) {    
                $techniquesInter->removeElement($techniqueInter);
                return true; 
                 
            }
        }
         
       
        return false;
    } 


    /**
     * @return Collection
     */
    public function getMaterielsInter(): Collection
    {
        return $this->materielsInter;
    }

    
    function setMaterielsInter(Collection $materielsInter) {
        
        
        foreach($this->materielsInter as $materielInter){
            $this->removeMaterielInter($materielInter);
            
        }

        foreach($materielsInter as $materielInter){
            if($this->removeNewMaterielIsAlreadyPersistOrNot($materielsInter, $materielInter) == false){
                $this->addMaterielInter($materielInter);
            };
        }
         
        return $this;
    }


    /**
     * @param InterventionMateriel $materielInter
     * @return $this
     */
    public function addMaterielInter(InterventionMateriel $materielInter): self
    {
        $this->materielsInter[] = $materielInter;
        $materielInter->setIntervention($this);
        
        return $this;
    }

    /**
     * @param InterventionMateriel $materielInter
     * @return $this
     */
    public function removeMaterielInter(InterventionMateriel $materielInter): self
    {
        if($this->materielsInter->removeElement($materielInter)){
            if ($materielInter->getIntervention() === $this){
                $materielInter->setIntervention(null);
            }
        }

        return $this;
    }

    private function removeNewMaterielIsAlreadyPersistOrNot(Collection $materielsInter, InterventionMateriel $materielInter): bool
    {
        foreach($this->getMaterielsInter() as $materielInterAlreadyPersist){
            if($materielInterAlreadyPersist->getMateriel()->getId() == $materielInter->getMateriel()->getId()) {    
                $materielsInter->removeElement($materielInter);
                return true; 
                 
            }
        }
         
       
        return false;
    } 

    public function __toString(): string
    {
        return $this->getNumIntervention();
    }
}
