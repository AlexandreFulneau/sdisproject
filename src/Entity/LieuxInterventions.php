<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LieuxInterventions
 *
 * @ORM\Table(name="lieux_interventions")
 * @ORM\Entity
 */
class LieuxInterventions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_lieux_interventions", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLieuxInterventions;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_lieux_interventions", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Lieu requis")
     */
    private $nomLieuxInterventions;

    public function getIdLieuxInterventions(): ?int
    {
        return $this->idLieuxInterventions;
    }

    public function getNomLieuxInterventions(): ?string
    {
        return $this->nomLieuxInterventions;
    }

    public function setNomLieuxInterventions(string $nomLieuxInterventions): self
    {
        $this->nomLieuxInterventions = $nomLieuxInterventions;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNomLieuxInterventions();
    }
}
