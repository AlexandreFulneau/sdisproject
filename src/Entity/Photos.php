<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Photos
 *
 * @ORM\Table(name="photos", indexes={@ORM\Index(name="idintervention_fk", columns={"idintervention"})})
 * @ORM\Entity
 * @UniqueEntity(fields={"chemin"}, message="Cette image existe déjà !")
 */
class Photos
{
    /**
     * @var int
     *
     * @ORM\Column(name="idphoto", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idphoto;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="chemin", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}(\/[a-zA-Z0-9-_]{1,}){1,2}(\.png|\.jpg|\.JPG|\.PNG)$/", message="Image invalide")
     */
    private $chemin;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}$/", message="Nom de l'image invalide")
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dossier", type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}$/", message="Nom du dossier invalide")
     */
    private $dossier;

    /**
     * @var Interventions
     *
     * @ORM\ManyToOne(targetEntity="Interventions", inversedBy="photos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idintervention", referencedColumnName="id", onDelete="CASCADE")
     * })
     *
     */
    private $idintervention;

    public function getIdphoto(): ?int
    {
        return $this->idphoto;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDossier(): ?string
    {
        return $this->dossier;
    }

    public function setDossier(?string $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getIdintervention(): ?Interventions
    {
        return $this->idintervention;
    }

    public function setIdintervention(?Interventions $idintervention): self
    {
        $this->idintervention = $idintervention;

        return $this;
    }


}
