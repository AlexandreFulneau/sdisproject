<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Laminas\Json\Json;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\GradeUser;
/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8D93D649E7927C74", columns={"email"})}, indexes={@ORM\Index(name="csp_pro", columns={"csp_pro"}), @ORM\Index(name="csp_vol", columns={"csp_vol"}), @ORM\Index(name="fonction", columns={"fonction"}), @ORM\Index(name="grade", columns={"grade"})})
 * @ORM\Entity
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Prénom requis.")
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Nom requis.")
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=false)
     * @Assert\Email(message = "L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    /**
     * @var Array
     *
     * @ORM\Column(name="roles", type="json", nullable=false)
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false, options={"default"="$2y$10$mmC6gzjR5YrCZcvMyBlT4u7J5l/j0sut85KkGDnD2FEAHTGbDSdSe"})
     */
    private $password = '$2y$10$mmC6gzjR5YrCZcvMyBlT4u7J5l/j0sut85KkGDnD2FEAHTGbDSdSe';

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     * @Assert\Length(min = 10, max = 10, minMessage = "min_lenght", maxMessage = "max_lenght")
     * @Assert\Regex(pattern="/^0[1-9][0-9]{8}$/", message="Format 0XXXXXXXXX")
     */
    private $telephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse_domicile", type="string", length=255, nullable=true)
     */
    private $adresseDomicile;

    /**
     * @var CentresSecours
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CentresSecours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="csp_pro", referencedColumnName="id")
     * })
     */
    private $cspPro;

    /**
     * @var CentresSecours
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CentresSecours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="csp_vol", referencedColumnName="id")
     * })
     */
    private $cspVol;

    /**
     * @var CentresSecours
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CentresSecours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="csp_affiliation", referencedColumnName="id")
     * })
     */
    private $cspAffiliation;

    /**
     * @var GradeUser
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\GradeUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grade", referencedColumnName="id_grade")
     * })
     */
    private $grade;

    /**
     * @var FonctionUser
     *
     * @ORM\ManyToOne(targetEntity="FonctionUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fonction", referencedColumnName="id_fonction")
     * })
     */
    private $fonction;

    /**
     * @ORM\OneToMany(targetEntity=FicheIndividuelleFMPA::class, mappedBy="personnel", orphanRemoval=true)
     */
    private $fichesIndividuellesFMPA;

    /**
     * @ORM\OneToMany(targetEntity=Evaluation::class, mappedBy="personnel", orphanRemoval=true)
     */
    private $evaluations;

    public function __construct()
    {
        $this->fichesIndividuellesFMPA = new ArrayCollection();
        $this->evaluations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresseDomicile(): ?string
    {
        return $this->adresseDomicile;
    }

    public function setAdresseDomicile(?string $adresseDomicile): self
    {
        $this->adresseDomicile = $adresseDomicile;

        return $this;
    }

    public function getCspPro(): ?CentresSecours
    {
        return $this->cspPro;
    }

    public function setCspPro(?CentresSecours $cspPro): self
    {
        $this->cspPro = $cspPro;

        return $this;
    }

    public function getCspVol(): ?CentresSecours
    {
        return $this->cspVol;
    }

    public function setCspVol(?CentresSecours $cspVol): self
    {
        $this->cspVol = $cspVol;

        return $this;
    }

    public function getCspAffiliation(): ?CentresSecours
    {
        return $this->cspAffiliation;
    }

    public function setCspAffiliation(?CentresSecours $cspAffiliation): self
    {
        $this->cspAffiliation = $cspAffiliation;

        return $this;
    }

    public function getGrade(): ?GradeUser
    {
        return $this->grade;
    }

    public function setGrade(?GradeUser $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getFonction(): ?FonctionUser
    {
        return $this->fonction;
    }

    public function setFonction(?FonctionUser $fonction): self
    {
        $this->fonction = $fonction;

        return $this;
    }


    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function __toString(): string
    {
        return $this->getNom() .' '.$this->getPrenom();
    }

    /**
     * @return Collection|FicheIndividuelleFMPA[]
     */
    public function getFichesIndividuellesFMPA(): Collection
    {
        return $this->fichesIndividuellesFMPA;
    }

    public function addFichesIndividuellesFMPA(FicheIndividuelleFMPA $fichesIndividuellesFMPA): self
    {
        if (!$this->fichesIndividuellesFMPA->contains($fichesIndividuellesFMPA)) {
            $this->fichesIndividuellesFMPA[] = $fichesIndividuellesFMPA;
            $fichesIndividuellesFMPA->setPersonnel($this);
        }

        return $this;
    }

    public function removeFichesIndividuellesFMPA(FicheIndividuelleFMPA $fichesIndividuellesFMPA): self
    {
        if ($this->fichesIndividuellesFMPA->removeElement($fichesIndividuellesFMPA)) {
            // set the owning side to null (unless already changed)
            if ($fichesIndividuellesFMPA->getPersonnel() === $this) {
                $fichesIndividuellesFMPA->setPersonnel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setPersonnel($this);
        }

        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->removeElement($evaluation)) {
            // set the owning side to null (unless already changed)
            if ($evaluation->getPersonnel() === $this) {
                $evaluation->setPersonnel(null);
            }
        }

        return $this;
    }

 
}
