<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documents
 *
 * @ORM\Table(name="documents")
 * @ORM\Entity
 * @UniqueEntity(fields={"chemin"}, message="Ce document existe déjà !")
 */
class Documents
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chemin", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}(\/[a-zA-Z0-9-_]{1,}){1,2}\.pdf$/", message="Document invalide")
     */
    private $chemin;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}$/", message="Nom du document invalide")
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dossier", type="string", length=255, nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Z0-9-_]{1,}$/", message="Nom du dossier invalide")
     */
    private $dossier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDossier(): ?string
    {
        return $this->dossier;
    }

    public function setDossier(?string $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }


}
