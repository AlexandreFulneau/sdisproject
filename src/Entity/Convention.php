<?php

namespace App\Entity;

use App\Repository\ConventionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConventionRepository::class)
 */
class Convention
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=TypeConvention::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeConvention;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $site;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresseSite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomConvention;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cheminConvention;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomFichePreparatoire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cheminFichePreparatoire;

     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomFichePreparatoireVierge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cheminFichePreparatoireVierge;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeConvention(): ?TypeConvention
    {
        return $this->typeConvention;
    }

    public function setTypeConvention(?TypeConvention $typeConvention): self
    {
        $this->typeConvention = $typeConvention;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getAdresseSite(): ?string
    {
        return $this->adresseSite;
    }

    public function setAdresseSite(string $adresseSite): self
    {
        $this->adresseSite = $adresseSite;

        return $this;
    }


    public function getNomConvention(): ?string
    {
        return $this->nomConvention;
    }

    public function setNomConvention(string $nomConvention): self
    {
        $this->nomConvention = $nomConvention;

        return $this;
    }


    public function getCheminConvention()
    {
        return $this->cheminConvention;
    }

    public function setCheminConvention($cheminConvention): self
    {
        $this->cheminConvention = $cheminConvention;

        return $this;
    }

    public function getNomFichePreparatoire(): ?string
    {
        return $this->nomFichePreparatoire;
    }

    public function setNomFichePreparatoire(string $nomFichePreparatoire): self
    {
        $this->nomFichePreparatoire = $nomFichePreparatoire;

        return $this;
    }

    public function getCheminFichePreparatoire()
    {
        return $this->cheminFichePreparatoire;
    }

    public function setCheminFichePreparatoire($cheminFichePreparatoire): self
    {
        $this->cheminFichePreparatoire = $cheminFichePreparatoire;

        return $this;
    }

    public function getNomFichePreparatoireVierge(): ?string
    {
        return $this->nomFichePreparatoireVierge;
    }

    public function setNomFichePreparatoireVierge(string $nomFichePreparatoireVierge): self
    {
        $this->nomFichePreparatoireVierge = $nomFichePreparatoireVierge;

        return $this;
    }

    public function getCheminFichePreparatoireVierge()
    {
        return $this->cheminFichePreparatoireVierge;
    }

    public function setCheminFichePreparatoireVierge($cheminFichePreparatoireVierge): self
    {
        $this->cheminFichePreparatoireVierge = $cheminFichePreparatoireVierge;

        return $this;
    }
}
