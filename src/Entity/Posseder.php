<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Posseder
 *
 * @ORM\Table(name="posseder", indexes={@ORM\Index(name="id", columns={"id_equipement_operationnel"}), @ORM\Index(name="idpersonnel", columns={"idpersonnel"})})
 * @ORM\Entity
 */
class Posseder
{
    /**
     * @var int
     *
     * @ORM\Column(name="idutiliser", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idutiliser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taille", type="string", length=255, nullable=true)
     */
    private $taille;

    /**
     * @var EquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="EquipementOperationnel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipement_operationnel", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEquipementOperationnel;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpersonnel", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idpersonnel;

    public function getIdutiliser(): ?int
    {
        return $this->idutiliser;
    }

    public function getNummateriel(): ?int
    {
        return $this->nummateriel;
    }

    public function setNummateriel(?int $nummateriel): self
    {
        $this->nummateriel = $nummateriel;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(?string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getIdEquipementOperationnel(): ?EquipementOperationnel
    {
        return $this->idEquipementOperationnel;
    }

    public function setIdEquipementOperationnel(?EquipementOperationnel $idEquipementOperationnel): self
    {
        $this->idEquipementOperationnel = $idEquipementOperationnel;

        return $this;
    }

    public function getIdpersonnel(): ?User
    {
        return $this->idpersonnel;
    }

    public function setIdpersonnel(?User $idpersonnel): self
    {
        $this->idpersonnel = $idpersonnel;

        return $this;
    }


}
