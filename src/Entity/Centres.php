<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centres
 *
 * @ORM\Table(name="centres")
 * @ORM\Entity
 */
class Centres
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcentre", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcentre;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    public function getIdcentre(): ?int
    {
        return $this->idcentre;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


}
