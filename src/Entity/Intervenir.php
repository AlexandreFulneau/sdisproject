<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Intervenir
 *
 * @ORM\Table(name="intervenir")
 * @ORM\Entity
 */
class Intervenir
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="garde", type="boolean", nullable=false)
     */
    private $garde;

    /**
     * @var bool
     *
     * @ORM\Column(name="astreinte", type="boolean", nullable=false)
     */
    private $astreinte;

    /**
     * @var bool
     *
     * @ORM\Column(name="repos", type="boolean", nullable=false)
     */
    private $repos;

    /**
     * @var Interventions
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Interventions", inversedBy="usersInter")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE" )
     * 
     */
    private $intervention;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank(message="Intervenant requis")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGarde(): ?bool
    {
        return $this->garde;
    }

    public function setGarde(bool $garde): self
    {
        $this->garde = $garde;

        return $this;
    }

    public function getAstreinte(): ?bool
    {
        return $this->astreinte;
    }

    public function setAstreinte(bool $astreinte): self
    {
        $this->astreinte = $astreinte;

        return $this;
    }

    public function getRepos(): ?bool
    {
        return $this->repos;
    }

    public function setRepos(bool $repos): self
    {
        $this->repos = $repos;

        return $this;
    }

    public function getIntervention(): ?Interventions
    {
        return $this->intervention;
    }

    public function setIntervention(?Interventions $intervention): self
    {
        $this->intervention = $intervention;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


}
