<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NomEquipementOperationnel
 *
 * @ORM\Table(name="nom_equipement_operationnel")
 * @ORM\Entity
 */
class NomEquipementOperationnel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_nom_equipement", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idNomEquipement;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    public function getIdNomEquipement(): ?int
    {
        return $this->idNomEquipement;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNom();
    }

}
