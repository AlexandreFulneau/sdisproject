<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commander
 *
 * @ORM\Table(name="commander", indexes={@ORM\Index(name="id_equipement", columns={"id_equipement"}), @ORM\Index(name="idcommande", columns={"idcommande"}), @ORM\Index(name="idinter_fk", columns={"idinter"}), @ORM\Index(name="idmateriel", columns={"idmateriel"})})
 * @ORM\Entity
 */
class Commander
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcommander", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcommander;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="justification", type="text", length=65535, nullable=true)
     */
    private $justification;

    /**
     * @var Commandes
     *
     * @ORM\ManyToOne(targetEntity="Commandes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcommande", referencedColumnName="idcommande")
     * })
     */
    private $idcommande;

    /**
     * @var Materiels
     *
     * @ORM\ManyToOne(targetEntity="Materiels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmateriel", referencedColumnName="id")
     * })
     */
    private $idmateriel;

    /**
     * @var EquipementOperationnel
     *
     * @ORM\ManyToOne(targetEntity="EquipementOperationnel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipement", referencedColumnName="id")
     * })
     */
    private $idEquipement;

    /**
     * @var Interventions
     *
     * @ORM\ManyToOne(targetEntity="Interventions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idinter", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idinter;

    public function getIdcommander(): ?int
    {
        return $this->idcommander;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getJustification(): ?string
    {
        return $this->justification;
    }

    public function setJustification(?string $justification): self
    {
        $this->justification = $justification;

        return $this;
    }

    public function getIdcommande(): ?Commandes
    {
        return $this->idcommande;
    }

    public function setIdcommande(?Commandes $idcommande): self
    {
        $this->idcommande = $idcommande;

        return $this;
    }

    public function getIdmateriel(): ?Materiels
    {
        return $this->idmateriel;
    }

    public function setIdmateriel(?Materiels $idmateriel): self
    {
        $this->idmateriel = $idmateriel;

        return $this;
    }

    public function getIdEquipement(): ?EquipementOperationnel
    {
        return $this->idEquipement;
    }

    public function setIdEquipement(?EquipementOperationnel $idEquipement): self
    {
        $this->idEquipement = $idEquipement;

        return $this;
    }

    public function getIdinter(): ?Interventions
    {
        return $this->idinter;
    }

    public function setIdinter(?Interventions $idinter): self
    {
        $this->idinter = $idinter;

        return $this;
    }


}
