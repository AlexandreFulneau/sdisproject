<?php

namespace App\Entity;

use App\Repository\InterventionTechniqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterventionTechniqueRepository::class)
 */
class InterventionTechnique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

       /**
     * @var Interventions
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Interventions", inversedBy="techniquesInter")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE" )
     * 
     */
    private $intervention;

    /**
     * @var Technique
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Technique")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * 
     */
    private $technique;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getIntervention(): ?Interventions
    {
        return $this->intervention;
    }

    public function setIntervention(?Interventions $intervention): self
    {
        $this->intervention = $intervention;

        return $this;
    }

    public function getTechnique(): ?Technique
    {
        return $this->technique;
    }

    public function setTechnique(?Technique $technique): self
    {
        $this->technique = $technique;

        return $this;
    }

}
