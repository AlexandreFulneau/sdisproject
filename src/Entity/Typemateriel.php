<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Typemateriel
 *
 * @ORM\Table(name="typemateriel")
 * @ORM\Entity(repositoryClass="App\Repository\TypematerielRepository")
 * @UniqueEntity(fields={"nom"}, message="Ce nom éxiste déjà !")
 */
class Typemateriel
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtypemateriel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtypemateriel;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez remplir ce champ")
     */
    private $nom;

    public function getIdtypemateriel(): ?int
    {
        return $this->idtypemateriel;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function __toString() : string
    {
        return $this->getNom();
    }

}
