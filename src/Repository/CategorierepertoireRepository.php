<?php

namespace App\Repository;

use App\Entity\Categorierepertoire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categorierepertoire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categorierepertoire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categorierepertoire[]    findAll()
 * @method Categorierepertoire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorierepertoireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categorierepertoire::class);
    }

    public function getAllOrderByRank()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.classement','ASC')
            ->addOrderBy('c.nom','ASC')
            ->getQuery()
            ->getResult()
            ;
    }
}
