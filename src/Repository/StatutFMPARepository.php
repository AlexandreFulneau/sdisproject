<?php

namespace App\Repository;

use App\Entity\StatutFMPA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StatutFMPA|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutFMPA|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutFMPA[]    findAll()
 * @method StatutFMPA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutFMPARepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutFMPA::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('s')
            ->orderBy('s.nom')
            ->getQuery()
            ->getResult();
    }
}
