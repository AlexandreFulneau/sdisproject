<?php

namespace App\Repository;

use App\Entity\InterventionMateriel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InterventionMateriel|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterventionMateriel|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterventionMateriel[]    findAll()
 * @method InterventionMateriel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionMaterielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterventionMateriel::class);
    }

  
}
