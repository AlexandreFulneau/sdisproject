<?php

namespace App\Repository;

use App\Entity\Compterendu;
use App\Entity\Intervenir;
use App\Entity\Interventions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Interventions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Interventions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Interventions[]    findAll()
 * @method Interventions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionsRepository extends ServiceEntityRepository
{
     public function __construct(ManagerRegistry $registry)
     {
      parent::__construct($registry, Interventions::class);
     }

    public function counterOfAllInterventions($filter = null){

        if($filter)
        {
            return $this->createQueryBuilder('i')
                ->select('count(i.id)')
                ->where('SUBSTRING(i.date, 1, 4) = :val')
                ->setParameter('val', $filter)
                ->getQuery()
                ->getSingleScalarResult();
        }
        else
        {
            return $this->createQueryBuilder('i')
                ->select('count(i.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
    }


     public function findOneByIdIntervention(int $id)
     {
            return $this->createQueryBuilder('i')
            ->leftJoin('i.ville','v')
            ->leftJoin('i.motif','m')       
            ->leftJoin('i.photos','p')
            ->leftJoin('i.usersInter','u')
            ->leftJoin('i.techniquesInter','t')
            ->leftJoin('i.materielsInter','ma')
            ->where('i.id = :id')
            ->select('i,v,m,u,p,t,ma')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

     }

     public function findAuthorById(int $id){

         $conn = $this->getEntityManager()->getConnection();

         $sql = "SELECT nom,prenom FROM `user`
            LEFT JOIN interventions on interventions.auteur = user.id
            WHERE interventions.id = $id";

         $stmt = $conn->prepare($sql);
         $stmt->execute();

         return $stmt->fetchAll();

     }

        public function findInterByAuthor(int $id, string $filter = null){

            $conn = $this->getEntityManager()->getConnection();

            if($filter != null){
                $sql = "SELECT interventions.id as idIntervention,interventions.num_intervention as numIntervention,date,adresse,ville.nom as ville, motif.libelle as motif, recapitulatif, user.nom as nom, user.prenom as prenom FROM `interventions`
                LEFT JOIN ville on interventions.ville= ville.idville
                LEFT JOIN motif on interventions.motif = motif.idmotif
                LEFT JOIN user on interventions.auteur = user.id 
                WHERE interventions.auteur = $id and YEAR(interventions.date) = $filter
                ORDER BY interventions.id  DESC";
            }
            else{
                $sql = "SELECT interventions.id  as idIntervention,interventions.num_intervention as numIntervention,date,adresse,ville.nom as ville, motif.libelle as motif, recapitulatif, user.nom as nom, user.prenom as prenom FROM `interventions`
                LEFT JOIN ville on interventions.ville= ville.idville
                LEFT JOIN motif on interventions.motif = motif.idmotif
                LEFT JOIN user on interventions.auteur = user.id 
                WHERE interventions.auteur = $id
                ORDER BY interventions.id  DESC
               ";
            }



        }

     public function findAllInterventions(string $filter = null)
     {

         $conn = $this->getEntityManager()->getConnection();

         if($filter != null) {
             $sql = "SELECT interventions.id as idIntervention,interventions.num_intervention as numIntervention,date,lieux_interventions.nom_lieux_interventions as lieu,ville.nom as ville, motif.libelle as motif, recapitulatif, user.nom as nom, user.prenom as prenom FROM `interventions`
            LEFT JOIN lieux_interventions on interventions.adresse = lieux_interventions.id_lieux_interventions
            LEFT JOIN ville on interventions.ville = ville.idville
            LEFT JOIN motif on interventions.motif = motif.idmotif
            LEFT JOIN user on interventions.auteur = user.id  
            WHERE YEAR(interventions.date) = $filter
            ORDER BY interventions.id 
             DESC
            ";
         }
         else{
             $sql = "SELECT interventions.id as idIntervention,interventions.num_intervention as numIntervention,date,lieux_interventions.nom_lieux_interventions as lieu,ville.nom as ville, motif.libelle as motif, recapitulatif, user.nom as nom, user.prenom as prenom FROM `interventions`
            LEFT JOIN lieux_interventions on interventions.adresse = lieux_interventions.id_lieux_interventions
            LEFT JOIN ville on interventions.ville= ville.idville
            LEFT JOIN motif on interventions.motif = motif.idmotif
            LEFT JOIN user on interventions.auteur = user.id 
            ORDER BY interventions.id 
             DESC
            ";
         }

         $stmt = $conn->prepare($sql);
         $stmt->execute();
         return $stmt->fetchAll();

     }

     public function findYearsInter(){

         $conn = $this->getEntityManager()->getConnection();

         $sql = "SELECT DISTINCT YEAR(date) as year FROM interventions ORDER BY date ASC";

         $stmt = $conn->prepare($sql);
         $stmt->execute();
         return $stmt->fetchAll();

     }


        public function statsInterventionByCommune(String $filterOne = null, String $filterTwo = null){


            $qb = $this->createQueryBuilder('i')
                ->select('count(i.id)*100/ :qb as pourcentage')
                ->addSelect('count(i.id) as nbinterventions')
                ->addSelect('v.nom as ville')
                ->leftJoin('i.ville','v')
                ->groupBy('v.nom')
                ->orderBy('pourcentage', 'desc');

            if( $filterOne != null && $filterTwo == null) {

                $nbInterventionsWithOneFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithOneFilters)
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne);
            }
            else if( $filterOne != null && $filterTwo != null){

                $nbInterventionsWithTwoFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo)
                    ->getQuery()
                    ->getResult();


                $qb->setParameter('qb', $nbInterventionsWithTwoFilters)
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo);

            }
            else{
                $nbInterventionsWithoutFilter = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithoutFilter);
            }
                
         return $qb->getQuery()->getResult();
        }

        
        public function statsTechniquesUsedPerIntervention(String $filterOne = null, String $filterTwo = null){


            $qb = $this->createQueryBuilder('i')
                ->select('count(i.id)*100/ :qb as pourcentage')
                ->addSelect('count(i.id) as nbinterventions')
                ->addSelect('te.nom as technique')
                ->innerJoin('i.techniquesInter','t')
                ->innerJoin('t.technique','te')
                ->groupBy('te.nom')
                ->orderBy('pourcentage', 'desc');

            if( $filterOne != null && $filterTwo == null) {

                $nbInterventionsWithOneFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithOneFilters)
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne);
            }
            else if( $filterOne != null && $filterTwo != null){

                $nbInterventionsWithTwoFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo)
                    ->getQuery()
                    ->getResult();


                $qb->setParameter('qb', $nbInterventionsWithTwoFilters)
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo);

            }
            else{
                $nbInterventionsWithoutFilter = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithoutFilter);
            }
                
         return $qb->getQuery()->getResult();
        }
        

        public function statsMaterielsUsedPerIntervention(String $filterOne = null, String $filterTwo = null){


            $qb = $this->createQueryBuilder('i')
                ->select('count(i.id)*100/ :qb as pourcentage')
                ->addSelect('count(i.id) as nbinterventions')
                ->addSelect('ma.nom as materiel')
                ->innerJoin('i.materielsInter','m')
                ->innerJoin('m.materiel','ma')
                ->groupBy('ma.nom')
                ->orderBy('pourcentage', 'desc');

            if( $filterOne != null && $filterTwo == null) {

                $nbInterventionsWithOneFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithOneFilters)
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne);
            }
            else if( $filterOne != null && $filterTwo != null){

                $nbInterventionsWithTwoFilters = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo)
                    ->getQuery()
                    ->getResult();


                $qb->setParameter('qb', $nbInterventionsWithTwoFilters)
                    ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                    ->setParameter('filterTwo', $filterTwo);

            }
            else{
                $nbInterventionsWithoutFilter = $this->createQueryBuilder('i')
                    ->select('count(i.id)')
                    ->getQuery()
                    ->getSingleScalarResult();

                $qb->setParameter('qb', $nbInterventionsWithoutFilter);
            }
                
         return $qb->getQuery()->getResult();
        }


    public function counterOfInterventionsByAuthor(int $id, $filter = null){

        if($filter)
        {
            return $this->createQueryBuilder('i')
                ->select('count(i)')
                ->where('i.auteur = :author')
                ->setParameter('author', $id)
                ->andwhere('SUBSTRING(i.date, 1, 4) = :val')
                ->setParameter('val', $filter)
                ->getQuery()
                ->getSingleScalarResult();
        }
        else
        {
            return $this->createQueryBuilder('i')
                ->select('count(i)')
                ->where('i.auteur = :author')
                ->setParameter('author', $id)
                ->getQuery()
                ->getSingleScalarResult();
        }
    }


    public function findYearsInterByAuthor($id){

        return $this->createQueryBuilder('i')
            ->select('SUBSTRING(i.date, 1, 4) as year')
            ->distinct('year')
            ->where('i.auteur = :author')
            ->setParameter('author', $id)
            ->orderBy('year', 'ASC')
            ->getQuery()
            ->getResult();


    }

  
    public function statsInterventionsByUser(String $filterOne = null, String $filterTwo = null){

        $qb = $this->createQueryBuilder('i')
            ->select( 'count(int.id)*100/ :qb as pourcentage')
            ->addSelect('count(int.id) as nbinterventions')
            ->addSelect('concat(u.nom,u.prenom) as personne')
            ->addSelect('u.id')
            ->innerJoin('i.usersInter','int')
            ->innerJoin('int.user','u')
            ->groupBy('int.user')
            ->orderBy('pourcentage','desc');

        if( $filterOne != null && $filterTwo == null) {

                $allInterventionsWithOneFilter = $this->_em->createQueryBuilder()
                    ->from(Interventions::class,'intervention')
                    ->select('count(intervention.id) as nb')
                    ->where('SUBSTRING(intervention.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne)
                    ->getQuery()
                    ->getSingleScalarResult();
                
            $qb->setParameter('qb',$allInterventionsWithOneFilter)
                    ->where('SUBSTRING(i.date, 1, 4) = :filterOne')
                    ->setParameter('filterOne', $filterOne);

        }
        else if( $filterOne != null && $filterTwo != null){

            $allInterventionsWithTwoFilter = $this->_em->createQueryBuilder()
                ->from(Interventions::class,'intervention')
                ->select('count(intervention.id) as nb')
                ->where('SUBSTRING(intervention.date, 1, 4) >= :filterOne')
                ->setParameter('filterOne', $filterOne)
                ->andWhere('SUBSTRING(intervention.date, 1, 4) <= :filterTwo')
                ->setParameter('filterTwo',$filterTwo)
                ->getQuery()
                ->getSingleScalarResult();

            $qb->setParameter('qb',$allInterventionsWithTwoFilter)
                ->where('SUBSTRING(i.date, 1, 4) >= :filterOne')
                ->setParameter('filterOne', $filterOne)
                ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                ->setParameter('filterTwo',$filterTwo);

        }
        else{
          
            $allInterventionsWithoutFilter = $this->_em->createQueryBuilder()
                ->from(Interventions::class,'intervention')
                ->select('count(intervention.id) as nb')
                ->getQuery()
                ->getSingleScalarResult();
               
            $qb->setParameter('qb',$allInterventionsWithoutFilter) ;
        }

       
        return $qb->getQuery()->getResult();

    }

    public function penaltyByUsers(String $filterOne = null, String $filterTwo = null)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('count(int.astreinte) as nbpenalty , u.id')
            ->where('int.astreinte = :a')
            ->setParameter('a', true)
            ->innerJoin('i.usersInter','int')
            ->innerJoin('int.user','u')   
            ->groupBy('u.id');

        if( $filterOne != null && $filterTwo == null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) = :filterOne')
                ->setParameter('filterOne', $filterOne);

        }
        elseif ($filterOne != null && $filterTwo != null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) >= :filterOne')
                ->setParameter('filterOne', $filterOne)
                ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                ->setParameter('filterTwo', $filterTwo);

        }
        
        return $qb->getQuery()->getResult();
    }

    public function guardByUsers(String $filterOne = null, String $filterTwo = null)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('count(int.garde) as nbguard , u.id')
            ->where('int.garde = :a')
            ->setParameter('a', true)
            ->innerJoin('i.usersInter','int')
            ->innerJoin('int.user','u')   
            ->groupBy('u.id');

        if( $filterOne != null && $filterTwo == null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) = :filterOne')
                ->setParameter('filterOne', $filterOne);

        }
        elseif ($filterOne != null && $filterTwo != null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) >= :filterOne')
                ->setParameter('filterOne', $filterOne)
                ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                ->setParameter('filterTwo', $filterTwo);

        }
    
        return $qb->getQuery()->getResult();
    }

    public function restByUsers(String $filterOne = null, String $filterTwo = null)
    {
        $qb = $this->createQueryBuilder('i')
            ->select('count(int.repos) as nbrest , u.id')
            ->where('int.repos = :a')
            ->setParameter('a', true)
            ->innerJoin('i.usersInter','int')
            ->innerJoin('int.user','u')   
            ->groupBy('u.id');

        if( $filterOne != null && $filterTwo == null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) = :filterOne')
                ->setParameter('filterOne', $filterOne);

        }
        elseif ($filterOne != null && $filterTwo != null) {

            $qb->andWhere('SUBSTRING(i.date, 1, 4) >= :filterOne')
                ->setParameter('filterOne', $filterOne)
                ->andWhere('SUBSTRING(i.date, 1, 4) <= :filterTwo')
                ->setParameter('filterTwo', $filterTwo);

        }
        
        return $qb->getQuery()->getResult();
    }
}
