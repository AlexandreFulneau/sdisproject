<?php

namespace App\Repository;

use App\Entity\Compterendu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compterendu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compterendu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compterendu[]    findAll()
 * @method Compterendu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompterenduRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compterendu::class);
    }


    public function counterOfInterventionsByAuthor(int $id, $filter = null){

        if($filter)
        {
            return $this->createQueryBuilder('cr')
                ->leftJoin('cr.numintervention','i')
                ->select('count(i.idInterventions)')
                ->where('cr.auteur = :author')
                ->setParameter('author', $id)
                ->andwhere('SUBSTRING(i.date, 1, 4) = :val')
                ->setParameter('val', $filter)
                ->getQuery()
                ->getSingleScalarResult();
        }
        else
        {
            return $this->createQueryBuilder('cr')
                ->leftJoin('cr.numintervention','i')
                ->select('count(i.idInterventions)')
                ->where('cr.auteur = :author')
                ->setParameter('author', $id)
                ->getQuery()
                ->getSingleScalarResult();
        }
    }


    public function findYearsInterByAuthor($id){

        return $this->createQueryBuilder('cr')
            ->leftJoin('cr.numintervention','i')
            ->select('SUBSTRING(i.date, 1, 4) as year')
            ->distinct('year')
            ->where('cr.auteur = :author')
            ->setParameter('author', $id)
            ->orderBy('year', 'ASC')
            ->getQuery()
            ->getResult();


    }

}
