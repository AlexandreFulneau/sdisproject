<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }


    public function findUsersByRole($role)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%');

        return $qb->getQuery()->getResult();
    }

    public function Findinfos(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT `id`, `prenom`, `nom`, `email`, `roles`, `password` FROM `user`";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        // var_dump($stmt->fetchAll());die;
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function findinfosperso($idclient): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT `id`, `prenom`, `nom`, `email`, `roles`, `password`, `telephone`, `fonction` FROM `user`
                WHERE id=$idclient";

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idclient]);
        // var_dump($stmt->fetchAll());die;
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();
    }

    public function findMDPUser($idclient)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT `id`, `prenom`, `nom`, `email`, `roles`, `password`, `telephone`, `fonction` FROM `user`
                WHERE id=$idclient";

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idclient]);
        // var_dump($stmt->fetchAll());die;
        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetch();
    }


    public function countUser(){

        return $this->createQueryBuilder('u')
            ->select('count(u.id) as count')
            ->getQuery()
            ->getSingleScalarResult();

    }

    public function findUser(string $id){

        return $this->createQueryBuilder('u')
            ->leftJoin('u.cspPro','cp')
            ->leftJoin('cp.type','tp')
            ->leftJoin('u.cspVol','cv')
            ->leftJoin('u.cspVol','ca')
            ->leftJoin('cv.type','tv')
            ->leftJoin('u.fonction','f')
            ->leftJoin('u.grade','g')
            ->select('u,cp,cv,f,tp,tv,ca,g')
            ->where('u.id = :iduser')
            ->setParameter('iduser',$id)
            ->getQuery()
            ->getOneOrNullResult();

    }

    public function findAllUser(){

        return $this->createQueryBuilder('u')
            ->leftJoin('u.cspPro','cp')
            ->leftJoin('cp.type','tp')
            ->leftJoin('u.cspVol','cv')
            ->leftJoin('cv.type','tv')
            ->leftJoin('u.fonction','f')
            ->leftJoin('u.grade','g')
            ->select('u,cp,cv,f,tp,tv,g')
            ->orderBy('f.classement','ASC')
            ->addOrderBy('u.nom','ASC')
            ->getQuery()
            ->getResult();

    }

    public function findAllAdresses(){

        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.adresseDomicile IS NOT NULL')
            ->getQuery()
            ->getResult()
            ;
    }
}
