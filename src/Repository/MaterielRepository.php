<?php

namespace App\Repository;

use App\Entity\Materiels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Materiels|null find($id, $lockMode = null, $lockVersion = null)
 * @method Materiels|null findOneBy(array $criteria, array $orderBy = null)
 * @method Materiels[]    findAll()
 * @method Materiels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Materiels::class);
    }

    public function getAll($filter = null)
    {
        if($filter == "none")
            $filter = null;

        if($filter != null){

            return $this->createQueryBuilder('c')
                ->innerJoin('c.type','t')
                ->addSelect('t')
                ->where('t.nom = :val')
                ->setParameter('val', $filter)
                ->getQuery()
                ->getResult() ;

        }
        else{

            return $this->createQueryBuilder('c')
                ->innerJoin('c.type','t')
                ->addSelect('t')
                ->getQuery()
                ->getResult();
        }
    }

    public function findOneById($value): ?Materiels
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
