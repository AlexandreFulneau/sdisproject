<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findYearsNews(){

        return $this->createQueryBuilder('n')
            ->select('SUBSTRING(n.date, 1, 4) as year')
            ->distinct('year')
            ->orderBy('year', 'ASC')
            ->getQuery()
            ->getResult();


    }

    public function findAllWithFilter(String $filter = null){

        if($filter){

            return $this->createQueryBuilder('n')
                ->where('SUBSTRING(n.date, 1, 4) = :filter')
                ->setParameter('filter', $filter)
                ->orderBy('n.idnews', 'DESC')
                ->getQuery()
                ->getResult();
        }
        else{

            return $this->createQueryBuilder('n')
                ->orderBy('n.idnews', 'DESC')
                ->getQuery()
                ->getResult();
        }
    }
}
