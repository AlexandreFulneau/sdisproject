<?php

namespace App\Repository;

use App\Entity\Intervenir;
use App\Entity\Interventions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method Intervenir|null find($id, $lockMode = null, $lockVersion = null)
 * @method Intervenir|null findOneBy(array $criteria, array $orderBy = null)
 * @method Intervenir[]    findAll()
 * @method Intervenir[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntervenirRepository  extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Intervenir::class);
    }

    public function nbIntervenant($idInter)
    {
        return $this->createQueryBuilder('i')
            ->select('count(i) as nb' )
            ->where('i.id = :idinter')
            ->setParameter('idinter', $idInter)
            ->getQuery()
            ->getSingleScalarResult();
    }
 

}
