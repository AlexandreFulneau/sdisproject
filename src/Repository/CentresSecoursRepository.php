<?php

namespace App\Repository;

use App\Entity\CentresSecours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CentresSecours|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentresSecours|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentresSecours[]    findAll()
 * @method CentresSecours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentresSecoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CentresSecours::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('cs')
            ->leftJoin('cs.type','t')
            ->select('cs,t')
            ->orderBy('t.classement','asc')
            ->addOrderBy('cs.nom','asc')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllAdresses(){

        return $this->createQueryBuilder('cs')
            ->leftJoin('cs.type','t')
            ->select('cs,t')
            ->where('cs.adresse IS NOT NULL')
            ->getQuery()
            ->getResult()
            ;
    }
}
