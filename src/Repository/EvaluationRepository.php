<?php

namespace App\Repository;

use App\Entity\Evaluation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Evaluation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evaluation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evaluation[]    findAll()
 * @method Evaluation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evaluation::class);
    }

    public function findAllEvaluations(){
        
        return $this->createQueryBuilder('e')
                    ->select('e','p','f','c')
                    ->leftJoin('e.personnel','p')
                    ->leftJoin('e.filiere','f')
                    ->leftJoin('e.centre','c')
                    ->getQuery()
                    ->getResult()
                    ;
   }

   public function findAllEvaluationsById(int $id){
        
    return $this->createQueryBuilder('e')
                ->select('e','p','f','c')
                ->where('p.id = :id')
                ->setParameter('id', $id)
                ->leftJoin('e.personnel','p')
                ->leftJoin('e.filiere','f')
                ->leftJoin('e.centre','c')
                ->getQuery()
                ->getResult()
                ;
}
}
