<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\NewsArchive;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Dompdf\Dompdf;

/**
 * @method NewsArchive|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsArchive|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsArchive[]    findAll()
 * @method NewsArchive[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsArchiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsArchive::class);
    }


    public function uploadNews(Dompdf $pdf, NewsArchive $archive, $root_path)
    {

        $file_path = $archive->getNewsChemin();
        $folder_name = $archive->getNewsDossier();


        if (file_exists($root_path . $folder_name . '/') == false)
            mkdir($root_path . $folder_name . '/');

        if (isset($file_path))
            file_put_contents($file_path,$pdf->output());

    }

    public function findFoldersName()
    {
        return $this->createQueryBuilder('na')
            ->select('na.newsDossier as nom')
            ->where('na.newsDossier is not NULL')
            ->groupBy('na.newsDossier')
            ->orderBy('na.newsDossier','ASC')
            ->getQuery()
            ->getResult()
            ;
    }


    public function deleteTree($dir)
    {
        foreach (glob($dir . "/*") as $element) {
            if (is_dir($element)) {
                deleteTree($element); // On rappel la fonction deleteTree
                rmdir($element); // Une fois le dossier courant vidé, on le supprime
            } else { // Sinon c'est un fichier, on le supprime
                unlink($element);
            }
            // On passe à l'élément suivant
        }
    }

    public function countNews(){

        return $this->createQueryBuilder('na')
            ->select('count(na)')
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
}
