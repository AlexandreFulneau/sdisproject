<?php

namespace App\Repository;

use App\Entity\CTD;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CTD|null find($id, $lockMode = null, $lockVersion = null)
 * @method CTD|null findOneBy(array $criteria, array $orderBy = null)
 * @method CTD[]    findAll()
 * @method CTD[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CTDRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CTD::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('c')
            ->orderBy('c.nom')
            ->getQuery()
            ->getResult();
    }
}
