<?php

namespace App\Repository;

use App\Entity\Typemateriel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Typemateriel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Typemateriel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Typemateriel[]    findAll()
 * @method Typemateriel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypematerielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Typemateriel::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('tm')
            ->orderBy('tm.nom','ASC')
            ->getQuery()
            ->getResult()
            ;

    }
}
