<?php

namespace App\Repository;

use App\Entity\TypeConvention;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeConvention|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeConvention|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeConvention[]    findAll()
 * @method TypeConvention[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeConventionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeConvention::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('t')
            ->orderBy('t.nom')
            ->getQuery()
            ->getResult();
    }
}
