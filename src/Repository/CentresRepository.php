<?php

namespace App\Repository;

use App\Entity\Centres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Centres|null find($id, $lockMode = null, $lockVersion = null)
 * @method Centres|null findOneBy(array $criteria, array $orderBy = null)
 * @method Centres[]    findAll()
 * @method Centres[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Centres::class);
    }


}
