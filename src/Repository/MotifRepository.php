<?php

namespace App\Repository;

use App\Entity\Motif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Motif|null find($id, $lockMode = null, $lockVersion = null)
 * @method Motif|null findOneBy(array $criteria, array $orderBy = null)
 * @method Motif[]    findAll()
 * @method Motif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotifRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Motif::class);
    }

    public function getAllOrderByName(){

        return $this->createQueryBuilder('m')
            ->orderBy('m.libelle')
            ->getQuery()
            ->getResult();
    }
}
