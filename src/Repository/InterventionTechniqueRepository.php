<?php

namespace App\Repository;

use App\Entity\InterventionTechnique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InterventionTechnique|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterventionTechnique|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterventionTechnique[]    findAll()
 * @method InterventionTechnique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionTechniqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterventionTechnique::class);
    }

   
}
