<?php

namespace App\Repository;

use App\Entity\Repertoire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Repertoire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Repertoire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Repertoire[]    findAll()
 * @method Repertoire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RepertoireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Repertoire::class);
    }

    public function getAllRepertoire($filter = null){

        $qb = $this->createQueryBuilder('r')
            ->leftJoin('r.categorie','c')
            ->select('r');


        if ($filter){
            $qb->where('c.nom = :filter')
                ->setParameter('filter', $filter);
        }

        return $qb->orderBy('c.classement','ASC')
            ->addOrderBy('r.nom','ASC')
            ->getQuery()
            ->getResult();
    }

    public function findById(int $id){

        return $this->createQueryBuilder('r')
            ->leftJoin('r.villeprincipale','fv')
            ->leftJoin('r.villesecondaire','sv')
            ->leftJoin('r.categorie','c')
            ->select('r,fv,sv,c')
            ->where('r.idrepertoire = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getOneOrNullResult();

    }
}
