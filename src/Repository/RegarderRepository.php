<?php

namespace App\Repository;

use App\Entity\Regarder;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Regarder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Regarder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Regarder[]    findAll()
 * @method Regarder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegarderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Regarder::class);
    }

    public function findAllNewsByUserId(User $user)
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.user','u')
            ->leftJoin('r.news','n')
            ->select('r,u,n')
            ->where('r.user = :user')
            ->setParameter('user', $user)
            ->andWhere('r.vue = false')
            ->getQuery()
            ->getResult();
    }

    public function findNewsByUserId(User $user)
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.user','u')
            ->leftJoin('r.news','n')
            ->select('r,u,n')
            ->where('r.user = :user')
            ->setParameter('user', $user)
            ->andWhere('r.vue = false')
            ->orderBy('r.idregarder', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
