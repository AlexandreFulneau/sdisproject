<?php

namespace App\Repository;

use App\Entity\FicheIndividuelleFMPA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FicheIndividuelleFMPA|null find($id, $lockMode = null, $lockVersion = null)
 * @method FicheIndividuelleFMPA|null findOneBy(array $criteria, array $orderBy = null)
 * @method FicheIndividuelleFMPA[]    findAll()
 * @method FicheIndividuelleFMPA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheIndividuelleFMPARepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FicheIndividuelleFMPA::class);
    }

    public function getNbHoursFmpaByUser(int $id){

        return $this->createQueryBuilder('f')
                    ->select('sum(f.nbHeures) as nbHours')
                    ->leftJoin('f.personnel','p')
                    ->where('p.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleScalarResult()
                    ;

    }
}
