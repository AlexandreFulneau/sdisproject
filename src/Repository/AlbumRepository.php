<?php

namespace App\Repository;

use App\Entity\Photos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Photos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photos[]    findAll()
 * @method Photos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Photos::class);
    }


    public function uploadPhoto(Photos $document, array $file, string $root_path)
    {
        $file_path = $document->getChemin();
        $folder_name = $document->getDossier();
        $file_tmp = $file['tmp_name'];

        if ($folder_name && ! file_exists($root_path . $folder_name . '/'))
            mkdir($root_path . $folder_name . '/');

        if (isset($file_path))
            move_uploaded_file($file_tmp, $file_path);

    }

    public function findFoldersName()
    {
        return $this->createQueryBuilder('d')
            ->select('d.dossier as nom')
            ->where('d.dossier is not NULL')
            ->groupBy('d.dossier')
            ->orderBy('nom','ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findInterventionsName()
    {
        return $this->createQueryBuilder('i')
            ->select('i.numintervention as nom')
            ->where('i.numintervention is not NULL')
            ->groupBy('i.numintervention')
            ->orderBy('nom','ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    function deleteTree($dir)
    {
        foreach (glob($dir . "/*") as $element) {
            if (is_dir($element)) {
                deleteTree($element); // On rappel la fonction deleteTree
                rmdir($element); // Une fois le dossier courant vidé, on le supprime
            } else { // Sinon c'est un fichier, on le supprime
                unlink($element);
            }
            // On passe à l'élément suivant
        }
    }
}
