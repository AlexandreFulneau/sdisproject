<?php

namespace App\Repository;

use App\Entity\Documents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @method Documents|null find($id, $lockMode = null, $lockVersion = null)
 * @method Documents|null findOneBy(array $criteria, array $orderBy = null)
 * @method Documents[]    findAll()
 * @method Documents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Documents::class);
    }


    public function uploadDocument(Documents $document, array $file, string $root_path)
    {
        $file_path = $document->getChemin();
        $folder_name = $document->getDossier();
        $file_tmp = $file['tmp_name'];

        if ($folder_name && ! file_exists($root_path . $folder_name . '/'))
            mkdir($root_path . $folder_name . '/');

        if (isset($file_path))
            move_uploaded_file($file_tmp, $file_path);

    }

    function deleteTree($dir){
        foreach(glob($dir . "/*") as $element){
            if(is_dir($element)){
                deleteTree($element);
                rmdir($element);
            } else {
                unlink($element);
            }
            // On passe à l'élément suivant
        }
    }


    public function DeleteDossier($dossier)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "DELETE from documents
                where dossier=?";



        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1,$dossier) ;

        $stmt->execute();

    }

    public function findFoldersName()
    {
        return $this->createQueryBuilder('d')
            ->select('d.dossier as nom')
            ->where('d.dossier is not NULL')
            ->groupBy('d.dossier')
            ->orderBy('nom','ASC')
            ->getQuery()
            ->getResult()
            ;
    }


    public function DELETEDoc($doc)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "DELETE from documents
                where id=$doc";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        // var_dump($stmt->fetchAll());die;
        // returns an array of arrays (i.e. a raw data set)
    }
}
