<?php

namespace App\Repository;

use App\Entity\FMPA;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FMPA|null find($id, $lockMode = null, $lockVersion = null)
 * @method FMPA|null findOneBy(array $criteria, array $orderBy = null)
 * @method FMPA[]    findAll()
 * @method FMPA[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FMPARepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FMPA::class);
    }

   public function findOneById(int $id){
        
        return $this->createQueryBuilder('f')
                    ->select('f','fi','p')
                    ->where('f.id = :id')
                    ->setParameter('id', $id)
                    ->leftJoin('f.fichesIndividuellesFMPA','fi')
                    ->leftJoin('fi.personnel','p')
                    ->orderBy('p.nom', 'desc')
                    ->getQuery()
                    ->getOneOrNullResult()
                    ;
   }
}
