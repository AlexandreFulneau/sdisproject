<?php

namespace App\Repository;

use App\Entity\NomEquipementOperationnel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NomEquipementOperationnel|null find($id, $lockMode = null, $lockVersion = null)
 * @method NomEquipementOperationnel|null findOneBy(array $criteria, array $orderBy = null)
 * @method NomEquipementOperationnel[]    findAll()
 * @method NomEquipementOperationnel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NomEquipementOperationnelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NomEquipementOperationnel::class);
    }


}
