<?php

namespace App\Repository;

use App\Entity\VerificationEquipementOperationnel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VerificationEquipementOperationnel|null find($id, $lockMode = null, $lockVersion = null)
 * @method VerificationEquipementOperationnel|null findOneBy(array $criteria, array $orderBy = null)
 * @method VerificationEquipementOperationnel[]    findAll()
 * @method VerificationEquipementOperationnel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VerificationEquipementOperationnelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VerificationEquipementOperationnel::class);
    }


    public function getMaterialVerifications(int $id)
    {
        return $this->createQueryBuilder('v')
            ->leftJoin('v.nomEquipement','n')
            ->where('v.nomEquipement = :id')
            ->setParameter('id',$id)
            ->andWhere('v.commentaire != :val')
            ->setParameter('val', '[%INITIALISATION%]')
            ->getQuery()
            ->getResult()
            ;
    }
}
