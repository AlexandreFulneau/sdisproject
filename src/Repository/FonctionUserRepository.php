<?php

namespace App\Repository;

use App\Entity\FonctionUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FonctionUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method FonctionUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method FonctionUser[]    findAll()
 * @method FonctionUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FonctionUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FonctionUser::class);
    }

    public function getAllOrderByRank(){

        return $this->createQueryBuilder('f')
            ->orderBy('f.classement','ASC')
            ->addOrderBy('f.nom','ASC')
            ->getQuery()
            ->getResult();
    }
}
