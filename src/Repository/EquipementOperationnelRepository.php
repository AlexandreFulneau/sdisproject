<?php

namespace App\Repository;

use App\Entity\Consommable;
use App\Entity\EquipementOperationnel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipementOperationnel|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipementOperationnel|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipementOperationnel[]    findAll()
 * @method EquipementOperationnel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipementOperationnelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipementOperationnel::class);
    }

    public function getAll($filter = null)
    {

        $qb = $this->createQueryBuilder('e')
            ->leftJoin('e.nom','n')
            ->leftJoin('e.periodicite','p')
            ->leftJoin('e.dureeVie','dv')
            ->leftJoin('e.type','t')
            ->leftJoin('e.centresSecours','cs')
            ->leftJoin('e.verifications','v')
            ->select('e,v,n,p,dv,t,cs')
            ;

        if($filter == "none")
            $filter = null;

        if($filter != null)
            $qb->where('t.nom = :val')->setParameter('val', $filter);

        return $qb->getQuery()->getResult();
    }

    public function getStockByEquipment($filter = null)
    {

        $qb = $this->createQueryBuilder('e')
            ->leftJoin('e.nom','n')
            ->leftJoin('e.type','t')
            ->select('e.id as idEquipment,n.nom as nom, t.nom as type, count(e.nom) as stock')
            ->groupBy('n.nom')
            ->orderBy('n.nom','ASC')
        ;

        if($filter == "none")
            $filter = null;

        if($filter != null)
            $qb->where('t.nom = :val')->setParameter('val', $filter);

        return $qb->getQuery()->getResult();
    }

    public function findOneById($value): ?EquipementOperationnel
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.nom','n')
            ->leftJoin('e.periodicite','p')
            ->leftJoin('e.dureeVie','dv')
            ->leftJoin('e.type','t')
            ->leftJoin('e.centresSecours','cs')
            ->select('e,n,p,dv,t,cs')
            ->where('e.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function getMaterialsByName(string $name){

        return $this->createQueryBuilder('e')
            ->leftJoin('e.nom','n')
            ->leftJoin('e.periodicite','p')
            ->leftJoin('e.dureeVie','dv')
            ->leftJoin('e.type','t')
            ->leftJoin('e.centresSecours','cs')
            ->leftJoin('e.verifications','v')
            ->select('e,n,p,dv,t,cs,v')
            ->where('n.nom = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult()
            ;
    }

    public function getMaterialsWithoutEPI(){

        return $this->createQueryBuilder('e')
            ->leftJoin('e.nom','n')
            ->leftJoin('e.periodicite','p')
            ->leftJoin('e.dureeVie','dv')
            ->leftJoin('e.type','t')
            ->leftJoin('e.centresSecours','cs')
            ->leftJoin('e.verifications','v')
            ->select('e,n,p,dv,t,cs,v')
            ->where('t.idtypemateriel != 2')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getCountOfEquipment($id){

        return $this->createQueryBuilder('e')
            ->select('count(e.nom)')
            ->groupBy('e.nom')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();

    }

    public function getCountOfAvailableEquipment(){

        return $this->createQueryBuilder('e')
            ->select('count(e)')
            ->where('e.disponible = true')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getEquipmentById($idEquipment){

        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.id = :idEpi')
            ->setParameter('idEpi', $idEquipment)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
