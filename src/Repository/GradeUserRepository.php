<?php

namespace App\Repository;

use App\Entity\GradeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GradeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method GradeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method GradeUser[]    findAll()
 * @method GradeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GradeUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GradeUser::class);
    }

   public function getAllOrderByRank(){

        return $this->createQueryBuilder('g')
            ->orderBy('g.classement','ASC')
            ->addOrderBy('g.nom','ASC')
            ->getQuery()
            ->getResult();
   }
}
