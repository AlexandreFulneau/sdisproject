<?php

namespace App\Form;

use App\Entity\EquipementOperationnel;
use App\Entity\Posseder;
use App\Services\Encryption;
use Doctrine\ORM\EntityRepository;
use phpDocumentor\Reflection\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PossederType extends AbstractType
{
    private $encryption;

    public function __construct(Encryption $encryption)
    {
        $this->encryption = $encryption;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $epiID = $options['epiID'];


            $builder->add('taille', null, ["label" => "Taille (Optionnelle)"]);

            if ($epiID){
                $builder
                    ->add('idEPI', HiddenType::class, [
                    'data' => $this->encryption->encryptionValue(strval($epiID)),
                    'mapped' => false
                    ])
                    ->add('idEquipementOperationnel', null, [
                        'label' => "E.P.I",
                        'class' => EquipementOperationnel::class,
                        'query_builder' => function (EntityRepository $er) use ($epiID){
                            return $er->createQueryBuilder('e')
                                ->leftJoin('e.nom', 'n')
                                ->select('e,n')
                                ->where('e.disponible = true or e.id = :id')
                                ->setParameter('id', $epiID)
                                ->orderBy('e.nom', 'ASC');
                        }
                    ]);
            }
            else{
                $builder->add('idEquipementOperationnel', null, [
                        'label' => "E.P.I",
                        'class' => EquipementOperationnel::class,
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('e')
                                ->where('e.disponible = true')
                                ->orderBy('e.nom', 'ASC');
                        }
                    ]);
            }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Posseder::class,
            'allow_extra_fields' => true
        ])->setRequired('epiID');


    }
}
