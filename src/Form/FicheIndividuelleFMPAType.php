<?php

namespace App\Form;

use App\Entity\FMPA;
use App\Entity\StatutFMPA;
use Doctrine\ORM\EntityRepository;
use App\Entity\FicheIndividuelleFMPA;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FicheIndividuelleFMPAType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text', "label" => "Date de début", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('dateFin', DateType::class, [
                'widget' => 'single_text', "label" => "Date de Fin", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('nbHeures', ChoiceType::class, [
                'choices' => [
                     4 => '4',
                     8 => '8',
                    16 => '16',
                    24 => '24',
                    40 => '40'
                ],
            ])
            ->add('sujet')
            ->add('lieux')
            ->add('tempsImmersion', TimeType::class, [
                'required' => false,
                'widget' => 'single_text', 
                'with_seconds' => true, 
                "label" => "Temps d'immersion", 
                'placeholder' => ['hour' => 'Heure', 'minute' => 'Minute', 'second' => 'Seconde',
                ],
                'invalid_message' => 'Format hh:mm:ss',
            ])
            ->add('personnel')
            ->add('statut', null, [
                'class' => StatutFMPA::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.nom', 'ASC');
                }
            ])
            ->add('fmpa', null, [
                'class' => FMPA::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.nom', 'ASC');
                }
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FicheIndividuelleFMPA::class,
        ]);
    }
}
