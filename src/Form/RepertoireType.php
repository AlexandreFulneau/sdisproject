<?php

namespace App\Form;

use App\Entity\Repertoire;
use App\Entity\Ville;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class RepertoireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('grade')
            ->add('nom')
            ->add('prenom')
            ->add('tel')
            ->add('telephonefixe')
            ->add('email')
            ->add('specialite')
            ->add('site')
            ->add('commentaires')
            ->add('categorie')
            ->add('adresseprincipale')
            ->add('adressesecondaire')
            ->add('villeprincipale', null, [
            'class' => Ville::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('v')
                    ->orderBy('v.nom', 'ASC');
            }
            ])
            ->add('villesecondaire', null, [
                'class' => Ville::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('v')
                        ->orderBy('v.nom', 'ASC');
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Repertoire::class,
        ]);
    }
}
