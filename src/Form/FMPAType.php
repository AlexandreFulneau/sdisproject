<?php

namespace App\Form;

use App\Entity\FMPA;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class FMPAType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text', "label" => "Date de début", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('dateFin', DateType::class, [
                'widget' => 'single_text', "label" => "Date de fin", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FMPA::class,
        ]);
    }
}
