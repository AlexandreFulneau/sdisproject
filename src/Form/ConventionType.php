<?php

namespace App\Form;

use App\Entity\Convention;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ConventionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text', "label" => "Date de début", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('dateFin', DateType::class, [
                'widget' => 'single_text', "label" => "Date de fin", 'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('typeConvention')
            ->add('site')
            ->add('adresseSite')
            ->add('cheminConvention', FileType::class, [
                'label' => 'Convention papier',
                'mapped' => false,
                'required' => false
            ])
            ->add('cheminFichePreparatoire', FileType::class, [
                'label' => 'Fiche préparatoire exercice',
                'mapped' => false,
                'required' => false
            ])
            ->add('cheminFichePreparatoireVierge', FileType::class, [
                'label' => 'Fiche préparatoire exercice vierge',
                'mapped' => false,
                'required' => false
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convention::class,
        ]);
    }
}
