<?php

namespace App\Form;

use App\Entity\FonctionUser;
use App\Entity\GradeUser;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom')
            ->add('nom')
            ->add('email')
            ->add('telephone')
            ->add('adresseDomicile');
        if(in_array('ROLE_ADMIN',$options['role'])){
            $builder
                ->add('grade', null, [
                    'class' => GradeUser::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('g')
                            ->orderBy('g.classement', 'ASC')
                            ->addOrderBy('g.nom','ASC');
                    }
                ])
                ->add('fonction', null, [
                    'class' => FonctionUser::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('f')
                            ->orderBy('f.classement', 'ASC')
                            ->addOrderBy('f.nom','ASC');
                    }
                ])
                ->add('roles', ChoiceType::class, [ 'label' => 'Autorisation',
                'choices' => [
                    'Niveau 1' => 'ROLE_EQUIPIER',
                    'Niveau 2' => 'ROLE_CHEF',
                    'Niveau 3'=>'ROLE_ADMIN',
                ],]);

            }
        $builder->add('password', RepeatedType::class, [
            'first_name' => 'pass',
            'second_name' => 'confirm',
            'type'=> PasswordType::class,
            'invalid_message' => 'Les mots de passe doivent être identique',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => false,
            'first_options'=> ['label'=>'Mot de passe', 'empty_data'=>' '],
            'second_options'=> ['label'=>'Répéter le mot de passe', 'empty_data'=>' '],
            
        ]);
        

        if(in_array('ROLE_ADMIN',$options['role'])){
            
          // Data transformer
          $builder->get('roles')
          ->addModelTransformer(new CallbackTransformer(
              function ($rolesArray) {
                   // transform the array to a string
                   return  implode(', ', (array)$rolesArray);
              },
              function ($rolesString) {
                   // transform the string back to an array
                   return explode(', ', $rolesString);
              }
      ));}


  
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => USER::class,
            'role'=>['ROLE_ADMIN']
        ]);
    }
}
