<?php

namespace App\Form;

use App\Entity\EquipementOperationnel;
use http\Env\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquipementOperationnelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $options['request'];

        $builder
            ->add('nom')
            ->add('numero')
            ->add('dateCreation',DateType::class,['widget' => 'single_text',"label"=>"Date d'intervention",'placeholder' => [
                'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('dureeVie')
            ->add('periodicite')
            ->add('type')
            ->add('oxygeneFilter', CheckboxType::class,[
                'label' => 'Oxygène',
                'mapped' => false,
                'required'=>false
            ])
            ->add('qrcodeFilter', CheckboxType::class,[
                'label' => 'QR Code',
                'mapped' => false,
                'required'=>false
            ])
            ->add('csFilter', CheckboxType::class,[
                'label' => 'CS',
                'mapped' => false,
                'required'=>false
            ])
        ;

        $builder->get('oxygeneFilter')->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) use ($request){
                if (isset($request->request->get('equipement_operationnel')['oxygeneFilter'])){

                    $oxygeneFilter = $request->request->get('equipement_operationnel')['oxygeneFilter'];

                    if ($oxygeneFilter == "true") {
                        $event->getForm()->getParent()->add('minPression')->add('pression');
                    }

                    if($oxygeneFilter == "false"){
                        $event->getForm()->getParent()->remove('oxygeneFilter');
                        $event->getForm()->getParent()->add('oxygeneFilter', CheckboxType::class,[
                            'label' => 'Oxygène',
                            'mapped' => false,
                            'required'=>false
                        ]);

                    }

                }
            }
        );

        $builder->get('qrcodeFilter')->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) use ($request){
                if (isset($request->request->get('equipement_operationnel')['qrcodeFilter'])){
                    $check = $request->request->get('equipement_operationnel')['qrcodeFilter'];

                    if ($check == "true")
                        $event->getForm()->getParent()->add('qrCode',HiddenType::class);
                    elseif($check == "false"){
                        $event->getForm()->getParent()->remove('qrcodeFilter');
                        $event->getForm()->getParent()->add('qrcodeFilter', CheckboxType::class,[
                            'label' => 'QR Code',
                            'mapped' => false,
                            'required'=>false
                        ]);


                    }
                }
            }
        );

        $builder->get('csFilter')->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) use ($request){
                if (isset($request->request->get('equipement_operationnel')['csFilter'])) {
                    $check = $request->request->get('equipement_operationnel')['csFilter'];

                    if ($check == "true")
                        $event->getForm()->getParent()->add('centresSecours');
                    elseif($check == "false"){
                        $event->getForm()->getParent()->remove('csFilter');
                        $event->getForm()->getParent()->add('csFilter', CheckboxType::class,[
                            'label' => 'CS',
                            'mapped' => false,
                            'required'=>false
                        ]);


                    }
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EquipementOperationnel::class,
            'allow_extra_fields' => true
        ])->setRequired('request');;
    }
}
