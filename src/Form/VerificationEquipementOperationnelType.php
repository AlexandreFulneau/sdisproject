<?php

namespace App\Form;

use App\Entity\VerificationEquipementOperationnel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VerificationEquipementOperationnelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date',DateType::class,['widget' => 'single_text','placeholder' => [
                'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'],
                'invalid_message' => 'Format YYYY-MM-JJ',
            ])
            ->add('commentaire')
            ->add('verificateur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VerificationEquipementOperationnel::class,
        ]);
    }
}
