<?php

namespace App\Form;

use App\Entity\CentresSecours;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CentresSecoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('type')
            ->add('statut', ChoiceType::class, [
                'choices' => [
                    'Professionnel' => 'PRO',
                    'Volontaire' => 'VOL',
                    'Vétérinaire'=>'Vétérinaire',
                    'Affiliation'=>'Affiliation'
                ],])
            ->add('adresse');
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CentresSecours::class,
        ]);
    }
}
