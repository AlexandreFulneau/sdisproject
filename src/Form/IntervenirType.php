<?php

namespace App\Form;

use App\Entity\Intervenir;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IntervenirType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', null, [
                'label' => false
            ])
            ->add('garde', null, [
                'label' => 'G'
            ])
            ->add('astreinte', null, [
                'label' => 'A'
            ])
            ->add('repos', null, [
                'label' => 'R'
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Intervenir::class,
        ]);
    }
}
