<?php

namespace App\Form;

use App\Entity\Evaluation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvaluationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbInterventions', null , [
                'label'=>'Nombre interventions'
            ])
            ->add('nbHeuresFmpa', null , [
                'label'=>'Nombre heures en FMPA'
            ])
            ->add('objectifAnneeEnCours', null , [
                'label'=>'Rappel objectif'
            ])
            ->add('competenceOperationnelle', null , [
                'label'=>'Compétence opérationnelle'
            ])
            ->add('implication', null , [
                'label'=>'Implication au sein de l\'équipe'
            ])
            ->add('critereAnneeEnCours', null , [
                'label'=>'Rappel des critères d\'atteinte'
            ])
            ->add('bilan', null , [
                'label'=>'Bilan'
            ])
            ->add('objectifAnneeSuivante', null , [
                'label'=>'Objectif'
            ])
            ->add('conditionReussiteAnneeSuivante', null , [
                'label'=>'Conditions de réussite et moyens de réalisation'
            ])
            ->add('critereAnneeSuivante', null , [
                'label'=>'Critères d\'atteinte'
            ])
            ->add('delaisRealisationAnneeSuivante', null , [
                'label'=>'Délais de réalisation'
            ])
            ->add('commentaire', null , [
                'label'=>'Commentaire'
            ])
            ->add('centre', null , [
                'label'=>'Centre'
            ])
            ->add('ctd', null , [
                'label'=>'Nom du CTD'
            ])
            ->add('filiere', null , [
                'label'=>'Filière'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evaluation::class,
        ]);
    }
}
