<?php

namespace App\Form;

use App\Entity\EquipementOperationnel;
use App\Entity\LieuxInterventions;
use App\Entity\Motif;
use App\Entity\Photos;
use App\Entity\Interventions;
use App\Entity\Materiels;
use App\Entity\Technique;
use App\Entity\User;
use App\Entity\Ville;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class InterventionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numIntervention',IntegerType::class,['label'=>"N° intervention"])
            ->add('date',DateType::class,['widget' => 'single_text',"label"=>"Date d'intervention",'placeholder' => [
                'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'],
                'invalid_message' => 'Format YYYY-MM-JJ',
                ])
            ->add('adresse', null, [
                'class' => LieuxInterventions::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.nomLieuxInterventions', 'ASC');
                }
            ])
            ->add('motif', null, [
                'class' => Motif::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.libelle', 'ASC');
                }
            ])
            ->add('ville', null, [
                'class' => Ville::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('v')
                        ->orderBy('v.nom', 'ASC');
                }
            ])
            ->add('usersInter', CollectionType::class, [
                'entry_type'   => IntervenirType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
                ])
            ->add('techniquesInter', CollectionType::class, [
                'entry_type'   => InterventionTechniqueType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
                ])
            ->add('materielsInter', CollectionType::class, [
                'entry_type'   => InterventionMaterielType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true
                ])
            //->add('equipements')
            ->add('recapitulatif')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Interventions::class,
            
        ]);
    }
}
