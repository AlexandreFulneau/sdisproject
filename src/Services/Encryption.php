<?php


namespace App\Services;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\BadFormatException;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;
use Defuse\Crypto\Key;

class Encryption
{

    private $key;

    public function __construct($key)
    {
        $this->key = Key::loadFromAsciiSafeString($key);
    }

    public function encryptionValue($value) : string
    {
        try {
            return Crypto::encrypt($value, $this->key);
        }
        catch (EnvironmentIsBrokenException $e) {
            return $e->getCode();
        }

    }

    public function decryptionValue($value) : string
    {
        try {
            return Crypto::decrypt($value, $this->key);
        }
        catch (EnvironmentIsBrokenException | WrongKeyOrModifiedCiphertextException $e) {
            return $e->getCode();
        }

    }

}