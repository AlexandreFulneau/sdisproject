<?php


namespace App\Services;


class TranslatePeriodicityToEnglish
{

    //traduit date sour format = { X jours, X mois, X ans}
    public function translate(string $date) : string
    {

        $dateSplit = explode(' ', $date);

        if ($dateSplit[1] == "jour" || $dateSplit[1] == "jours")
            $date = $dateSplit[0]." days";
        elseif ($dateSplit[1] == "mois")
            $date = $dateSplit[0]." month";
        elseif ($dateSplit[1] == "an" || $dateSplit[1] == "ans")
            $date = $dateSplit[0]." year";

        return $date;
    }
}