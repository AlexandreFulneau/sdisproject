<?php


namespace App\Services;

use Symfony\Component\Validator\ConstraintViolationList;

class ContraintViolationToArray
{
    public function convert(ConstraintViolationList $violationList) : array
    {
        $errors = array();

        // Global
        foreach ($violationList as $key => $error) {
            if ($error->getPropertyPath() == 'chemin')
                $errors['1'] = $error->getMessage();
            elseif ($error->getPropertyPath() == 'nom')
                    $errors['2'] = $error->getMessage();
            elseif ($error->getPropertyPath() == 'dossier')
                $errors['3'] = $error->getMessage();
        }

        return $errors;
    }
}