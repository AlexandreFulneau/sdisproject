<?php

namespace App\Controller;

use App\Entity\EquipementOperationnel;
use App\Entity\Materiel;
use App\Entity\Posseder;
use App\Entity\User;
use App\Form\PossederType;
use App\Repository\EquipementOperationnelRepository;
use App\Repository\PossederRepository;
use App\Services\Encryption;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/EPI")
 * 
 */
class PossederController extends AbstractController
{
    /**
     * @Route("/{idUser}", name="posseder_index", methods={"GET"})
     * @param Encryption $encryption
     * @param EquipementOperationnelRepository $equipementOperationnelRepository
     * @param PossederRepository $possederRepository
     * @param $idUser
     * @return Response
     */
    public function index(Encryption $encryption, EquipementOperationnelRepository $equipementOperationnelRepository, PossederRepository $possederRepository ,$idUser): Response
    {

        $decrypted_id = $encryption->decryptionValue($idUser);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $epi_list = $possederRepository->findEPIByUser($decrypted_id);
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=>$decrypted_id]);

        return $this->render('posseder/index.html.twig', [
            'available_equipment' => $equipementOperationnelRepository->getCountOfAvailableEquipment() > 0,
            'epi_list' => $epi_list,
            'idUser'=>$idUser,
            'user' => $user
        ]);
    }

    /**
     * @Route("/{idUser}/ajout_epi", name="posseder_new", methods={"GET","POST"})
     * @param Request $request
     * @param $idUser
     * @return Response
     */
    public function new(Encryption $encryption, Request $request, $idUser): Response
    {

        $decrypted_id = $encryption->decryptionValue($idUser);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $posseder = new Posseder();
        $form = $this->createForm(PossederType::class, $posseder, ['epiID' => null]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $posseder->setIdpersonnel($entityManager->getRepository(User::class)->findOneBy(['id'=>$decrypted_id]));
            $entityManager->persist($posseder);

            $newMaterialStock = $entityManager->getRepository(EquipementOperationnel::class)->find($posseder->getIdEquipementOperationnel());

            $newMaterialStock->setDisponible(false);

            $entityManager->flush();

            $this->addFlash('add', 'E.P.I ajouté !');


            return $this->redirectToRoute('posseder_index',['idUser'=> $encryption->encryptionValue(strval($posseder->getIdpersonnel()->getId()))]);
        }
       
        return $this->render('posseder/new.html.twig', [
            'posseder' => $posseder,
            'user'=>$idUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idutiliser}/modification_epi", name="posseder_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Posseder $epi
     * @return Response
     */
    public function edit(Encryption $encryption, Request $request, $idutiliser): Response
    {
        $decrypted_idutiliser = $encryption->decryptionValue($idutiliser);

        $epi = $this->getDoctrine()->getRepository(Posseder::class)->findOneBy(['idutiliser' => $decrypted_idutiliser]);
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(PossederType::class, $epi, ['action' => $this->generateUrl('posseder_edit',['idutiliser' => $idutiliser]), 'epiID' => $epi->getIdEquipementOperationnel()->getid()]);
        $form->handleRequest($request);

        $decrypted_iduser = $encryption->decryptionValue($form->get('idEPI')->getData());

        if ($decrypted_iduser == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $lastEpiStock = $entityManager->getRepository(EquipementOperationnel::class)->findOneBy(['id' => $decrypted_iduser]);
            $lastEpiStock->setDisponible(true);

            $newEpiStock = $entityManager->getRepository(EquipementOperationnel::class)->find($epi->getIdEquipementOperationnel());
            $newEpiStock->setDisponible(false);

            $entityManager->flush();

            $this->addFlash('add', 'E.P.I modifié !');

            return $this->redirectToRoute('posseder_index',['idUser'=> $encryption->encryptionValue(strval($epi->getIdpersonnel()->getId()))]);
        }

        return $this->render('posseder/edit.html.twig', [
            'epi' => $epi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idutiliser}/supprimer", name="posseder_delete", methods={"DELETE"})
     * @param Request $request
     * @param Posseder $posseder
     * @return Response
     */
    public function delete(Encryption $encryption, Request $request, $idutiliser): Response
    {
        $decrypted_idutiliser = $encryption->decryptionValue($idutiliser);

        if ($decrypted_idutiliser == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $epi = $this->getDoctrine()->getRepository(Posseder::class)->findOneBy(['idutiliser' => $decrypted_idutiliser]);

        if ($this->isCsrfTokenValid('delete'.$decrypted_idutiliser, $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($epi);

            $newMaterialStock = $entityManager->getRepository(EquipementOperationnel::class)->find($epi->getIdEquipementOperationnel());

            $newMaterialStock->setDisponible(true);

            $entityManager->flush();
            $this->addFlash('add', 'E.P.I remis dans le stock !');
        }

        return $this->redirectToRoute('posseder_index', ['idUser' => $encryption->encryptionValue(strval($epi->getIdpersonnel()->getId()))]);

    }
}
