<?php

namespace App\Controller;

use App\Repository\InterventionsRepository;
use App\Repository\RepertoireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiController
 * @package App\Controller
 * @Route("/api")
 */
class ApiController extends AbstractController
{

    /**
     * @Route("/v1/contacts/collection", name="api_contacts_directory_get", methods= {"GET"})
     * @param RepertoireRepository $repertoireRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function contactsDirectory(RepertoireRepository $repertoireRepository, SerializerInterface $serializer): Response
    {
        return new JsonResponse(
            $serializer->serialize($repertoireRepository->findAll(),"json"),
            JsonResponse::HTTP_OK,
            ["content-type" => "application/json"],
            true
        );
    }

    /**
     * @Route("/v1/interventions/collection", name="api_interventions_collection_get", methods= {"GET"})
     * @param InterventionsRepository $interventionsRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function interventionsCollection(InterventionsRepository $interventionsRepository, SerializerInterface $serializer): Response
    {
        return new JsonResponse(
            $serializer->serialize($interventionsRepository->findAllInterventions(),"json"),
            JsonResponse::HTTP_OK,
            [],
            true
        );
    }
}
