<?php

namespace App\Controller;

use App\Entity\Centres;
use App\Entity\CentresSecours;
use App\Entity\Posseder;
use App\Entity\User;
use App\Form\UserType;

use App\Repository\CentresSecoursRepository;
use App\Repository\PossederRepository;
use App\Repository\UserRepository;
use App\Services\Encryption;
use Dompdf\Dompdf;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * @Route("/utilisateur")
 * @isGranted("ROLE_EQUIPIER")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @isGranted("ROLE_ADMIN")
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAllUser();

        return $this->render('user/index.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function new(CentresSecoursRepository $centresSecoursRepository, Request $request,UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $status = $request->get('status');
        $cspPro = $request->get('csp_pro');
        $cspVol = $request->get('csp_vol');
        $cspVeto = $request->get('csp_veto');
        $cspAffi = $request->get('csp_affi');
        $csList = $centresSecoursRepository->getAllOrderByName();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setPassword($encoder->encodePassword($user,$form->getData()->getPassword()));

            $this->setCsp($user, $status, $cspPro, $cspVol, $cspVeto, $cspAffi);

            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('add', 'Utilisateur ajouté !');

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'csList' => $csList
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(Encryption $encryption, UserRepository $userRepository, PossederRepository $possederRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $user = $userRepository->findUser($decrypted_id);
        $epi = $possederRepository->findEPIByUser($decrypted_id);
       
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'equipements'=>$epi
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     *
     */
    public function edit(Encryption $encryption, Request $request, $id, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository, CentresSecoursRepository $centresSecoursRepository): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $user = $userRepository->findUser($decrypted_id);
        $status = $request->get('status');
        $cspPro = $request->get('csp_pro');
        $cspVol = $request->get('csp_vol');
        $cspVeto = $request->get('csp_veto');
        $cspAffi = $request->get('csp_affi');
        $csList = $centresSecoursRepository->getAllOrderByName();

        $form = $this->createForm(UserType::class, $user,['role' => $this->getUser()->getRoles()]);
        $form->handleRequest($request);
            
        if ($form->isSubmitted() && $form->isValid()) {

            $baseUser=$userRepository->findMDPUser($user->getId());

            $oldpassword=$baseUser["password"];


            $password=$form->getData();
            $password=$password->getPassword();

            $newpassword=$passwordEncoder->encodePassword($user, $user->getPassword());

            
            if(($newpassword!=$oldpassword) && $password!= " " ){
                $user->setPassword($newpassword);

            }else{
                $user->setPassword($oldpassword);

            }

            $this->setCsp($user, $status, $cspPro, $cspVol, $cspVeto, $cspAffi);

            $em = $this->getDoctrine()->getManager();

            $em->flush();

            $this->addFlash('add', 'Utilisateur modifié !');
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'csList' => $csList
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
            $this->addFlash('add', 'Utilisateur supprimé !');
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/pdf/{id}", options={"expose"=true}, name="pdfInfoUser")
     * @IsGranted("ROLE_ADMIN")
     */
    public function pdfInfosUser(Encryption $encryption, UserRepository $userRepository, $id){


        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

            $user = $userRepository->findUser($decrypted_id);
            $epi= $this->getDoctrine()->getRepository(Posseder::class)->findBy(['idpersonnel'=>$user->getId()]);


            $filename = "Compte_" . $user->getNom() ."_". $user->getPrenom() .".pdf";

            $html = $this->renderView('user/_pdf.html.twig', ['user'=>$user,'equipements' => $epi]);

            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4');
            $dompdf->render();
            $dompdf->stream($filename, ['Attachment' => 0]);

        exit();
    }

    private function setCsp($user, $status, $cspPro, $cspVol, $cspVeto, $cspAffi)
    {

        if ($status == "pro" && $cspPro != "none" && $cspAffi != "none"){
            $user->setCspPro($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspPro]));
            $user->setCspAffiliation($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspAffi]));
            $user->setCspVol(null);
        }
        elseif ($status == "pro" && $cspPro != "none" && $cspAffi == "none"){
            $user->setCspPro($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspPro]));
            $user->setCspAffiliation(null);
            $user->setCspVol(null);
        }
        elseif ($status == "vol" && $cspVol != "none"){
            $user->setCspVol($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspVol]));
            $user->setCspPro(null);
            $user->setCspAffiliation(null);
        }
        elseif ($status == "provol" && $cspPro != "none" && $cspVol != "none"){

            $user->setCspPro($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspPro]));
            $user->setCspVol($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspVol]));
            $user->setCspAffiliation(null);
        }
        elseif ($status == "veterinaire" && $cspVeto != "none"){
            $user->setCspPro($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $cspVeto]));
            $user->setCspVol(null);
            $user->setCspAffiliation(null);
        }
        else{
            $user->setCspPro(null);
            $user->setCspVol(null);
            $user->setCspAffiliation(null);
        }
       
    }
}
