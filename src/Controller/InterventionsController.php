<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use App\Entity\Photos;
use App\Services\Encryption;
use App\Entity\Interventions;
use App\Form\InterventionsType;
use App\Repository\IntervenirRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CompterenduRepository;
use App\Repository\InterventionsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/interventions")
 * @isGranted("ROLE_EQUIPIER")
 */
class InterventionsController extends AbstractController
{
    /**
     * @Route("/", name="interventions_index", methods={"GET"})
     */
    public function index(InterventionsRepository $interventionsRepository, Request $request)
    {
        $interventions = null;
        $yearWithInter = null;
        $filter = $request->get('filter');
        $countInterventions = null;

        if ($filter == "none") {
            $filter = null;
        }

        if (($this->getUser()->getRoles()[0]) == "ROLE_ADMIN") {
            $interventions = $interventionsRepository->findAllInterventions($filter);
            $countInterventions = $interventionsRepository->counterOfAllInterventions($filter);
            $yearWithInter = $interventionsRepository->findYearsInter();
        } else {
            $interventions = $interventionsRepository->findInterByAuthor($this->getUser()->getId(), $filter);
            $countInterventions = $interventionsRepository->counterOfInterventionsByAuthor($this->getUser()->getId(), $filter);
            $yearWithInter = $interventionsRepository->findYearsInterByAuthor($this->getUser()->getId());
        }

        if ($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('interventions/_content.html.twig', ['interventions' => $interventions, 'countInterventions' => $countInterventions])
            ]);
        }

        return $this->render('interventions/index.html.twig', [
            'interventions' => $interventions,
            'yearInter' => $yearWithInter,
            'countInterventions' => $countInterventions
        ]);
    }



    /**
     * @Route("/{idInterventions}", name="interventions_show", methods={"GET"})
     */
    public function show(Encryption $encryption, InterventionsRepository $interventionsRepository, $idInterventions): Response
    {
        $decrypted_id = $encryption->decryptionValue($idInterventions);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $intervention = $interventionsRepository->findOneByIdIntervention($decrypted_id);

        return $this->render('interventions/show.html.twig', [
            'intervention' => $intervention,
        ]);
    }


    /**
     * @Route("/{id}", name="interventions_delete", methods={"DELETE"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $intervention = $this->getDoctrine()->getRepository(Interventions::class)->findOneBy(['id' => $decrypted_id]);

        if ($this->isCsrfTokenValid('delete' . $intervention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($intervention);
            $entityManager->flush();
            $this->addFlash('add', 'Compte rendu supprimé !');
        }

        return $this->redirectToRoute('interventions_index');
    }


    /**
     * @Route("/{idInterventions}/pdf", name="interventions_pdf", methods={"GET"})
     * @isGranted("ROLE_ADMIN")
     */
    public function rapportPdf(Encryption $encryption, InterventionsRepository $interventionsRepository, $idInterventions)
    {
        $decrypted_id = $encryption->decryptionValue($idInterventions);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $intervention = $interventionsRepository->findOneByIdIntervention($decrypted_id);

        $html = $this->renderView('interventions/_pdf.html.twig', ['intervention' => $intervention]);

        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $options->set('defaultFont', 'sans-serif');
        $options->set('debugKeepTemp', TRUE);
        $options->set('isHtml5ParserEnabled', TRUE);

        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();

        // Parameters
        $x = 520;
        $y = 815;
        $text = "{PAGE_NUM} sur {PAGE_COUNT}";
        $font = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
        $size = 10;
        $color = array(0, 0, 0);
        $word_space = 0.0;
        $char_space = 0.0;
        $angle = 0.0;

        $dompdf->getCanvas()->page_text(
            $x,
            $y,
            $text,
            $font,
            $size,
            $color,
            $word_space,
            $char_space,
            $angle
        );

        $dompdf->stream("Rapport_Intervention_" . $intervention->getNumIntervention(), ["Attachment" => 0]);
    }


    /**
     * @Route("/compte-rendu/envoyer", name="cr_envoi", methods={"GET","POST"})
     */
    public function new_report(Encryption $encryption, Request $request): Response
    {
        $intervention = new Interventions();

        $form = $this->createForm(InterventionsType::class, $intervention);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $intervention->setAuteur($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($intervention);
            $entityManager->flush();
            $this->addFlash('add', 'Compte-rendu enregistré !');


            if ($request->request->get('materiel') == null)
                return $this->redirectToRoute('interventions_index');
            else
                return $this->redirectToRoute('materiel', ['idinter' => $encryption->encryptionValue((string)$intervention->getId())]);
        }

        return $this->render('compte_rendu/new_update.html.twig', [

            'form' => $form->createView(),

        ]);
    }


    /**
     * @Route("/compte-rendu/modifier/{id}", name="cr_maj")
     */
    public function edit_report(Encryption $encryption, InterventionsRepository $interventionsRepository, Request $request, $id)
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $intervention = $interventionsRepository->findOneByIdIntervention($decrypted_id);

        $form = $this->createForm(InterventionsType::class, $intervention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('add', 'Compte rendu modifié !');

            if ($request->request->get('materiel') == null)
                return $this->redirectToRoute('interventions_index');
            else
                return $this->redirectToRoute('materiel', ['idinter' => $encryption->encryptionValue((string)$intervention->getId())]);
        }

        return $this->render('compte_rendu/new_update.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
