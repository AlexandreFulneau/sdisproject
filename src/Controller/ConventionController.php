<?php

namespace App\Controller;

use App\Entity\Convention;
use App\Form\ConventionType;
use App\Services\Encryption;
use App\Repository\ConventionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/convention")
 * @isGranted("ROLE_EQUIPIER")
 */
class ConventionController extends AbstractController
{
    /**
     * @Route("/", name="convention_index", methods={"GET"})
     */
    public function index(ConventionRepository $conventionRepository): Response
    {
        return $this->render('convention/index.html.twig', [
            'conventions' => $conventionRepository->findBy([],['dateFin'=> 'ASC']),
        ]);
    }

    /**
     * @Route("/ajouter", name="convention_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $convention = new Convention();
        $form = $this->createForm(ConventionType::class, $convention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $conventionPdf = $form->get('cheminConvention')->getData();
            $fichePreparatoirePdf = $form->get('cheminFichePreparatoire')->getData();
            $fichePreparatoireViergeWord = $form->get('cheminFichePreparatoireVierge')->getData();

            if ($conventionPdf) {
                $fichierConventionPdf = md5(uniqid()) . '.' . $conventionPdf->guessExtension();
                $conventionPdf->move($this->getParameter('app_conventions_directory'), $fichierConventionPdf);
                $convention->setNomConvention("Convention");
                $convention->setCheminConvention($fichierConventionPdf);
            }

            if ($fichePreparatoirePdf) {
                $fichierFichePreparatoirePdf = md5(uniqid()) . '.' . $fichePreparatoirePdf->guessExtension();
                $fichePreparatoirePdf->move($this->getParameter('app_conventions_directory'), $fichierFichePreparatoirePdf);
                $convention->setNomFichePreparatoire("Fiche_Preparatoire");
                $convention->setCheminFichePreparatoire($fichierFichePreparatoirePdf);
            }

            if ($fichePreparatoireViergeWord) {
                $fichierFichePreparatoireViergeWord = md5(uniqid()) . '.' . $fichePreparatoireViergeWord->guessExtension();
                $fichePreparatoireViergeWord->move($this->getParameter('app_conventions_directory'), $fichierFichePreparatoireViergeWord);
                $convention->setNomFichePreparatoireVierge("Fiche_Preparatoire_Vierge");
                $convention->setCheminFichePreparatoireVierge($fichierFichePreparatoireViergeWord);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($convention);
            $entityManager->flush();

            return $this->redirectToRoute('convention_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('convention/new.html.twig', [
            'convention' => $convention,
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/{id}", name="convention_show", methods={"GET"})
     */
    public function show(Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        return $this->render('convention/show.html.twig', [
            'convention' => $convention,
        ]);
    }


    /**
     * @Route("/{id}/modifier", name="convention_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        $form = $this->createForm(ConventionType::class, $convention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $conventionPdf = $form->get('cheminConvention')->getData();
            $fichePreparatoirePdf = $form->get('cheminFichePreparatoire')->getData();
            $fichePreparatoireViergeWord = $form->get('cheminFichePreparatoireVierge')->getData();

            if ($conventionPdf) {
                $fichierConventionPdf = md5(uniqid()) . '.' . $conventionPdf->guessExtension();
                $conventionPdf->move($this->getParameter('app_conventions_directory'), $fichierConventionPdf);
                $convention->setNomConvention("Convention");
                $convention->setCheminConvention($fichierConventionPdf);
            }

            if ($fichePreparatoirePdf) {
                $fichierFichePreparatoirePdf = md5(uniqid()) . '.' . $fichePreparatoirePdf->guessExtension();
                $fichePreparatoirePdf->move($this->getParameter('app_conventions_directory'), $fichierFichePreparatoirePdf);
                $convention->setNomFichePreparatoire("Fiche_Preparatoire");
                $convention->setCheminFichePreparatoire($fichierFichePreparatoirePdf);
            }

            if ($fichePreparatoireViergeWord) {
                $fichierFichePreparatoireViergeWord = md5(uniqid()) . '.' . $fichePreparatoireViergeWord->guessExtension();
                $fichePreparatoireViergeWord->move($this->getParameter('app_conventions_directory'), $fichierFichePreparatoireViergeWord);
                $convention->setNomFichePreparatoireVierge("Fiche_Preparatoire_Vierge");
                $convention->setCheminFichePreparatoireVierge($fichierFichePreparatoireViergeWord);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('convention_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('convention/edit.html.twig', [
            'convention' => $convention,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/convention-pdf", name="convention_pdf", methods={"GET"})
     */
    public function showConvention(Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        $file = $this->getParameter('app_conventions_directory') . '/' . $convention->getCheminConvention();

        // Type de contenu de l'en-tête
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="Convention.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');


        // Lire le fichier
        @readfile($file);

        return $this->redirectToRoute('convention_show', ['id' => $id]);
    }

     /**
     * @Route("/{id}/fiche-preparatoire-vierge-word", name="fiche_preparatoire_vierge_word", methods={"GET"})
     */
    public function showFichePreparatoireVierge(Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        $file = $this->getParameter('app_conventions_directory') . '/' . $convention->getCheminFichePreparatoireVierge();

        // Type de contenu de l'en-tête
        header('Content-type: application/docx');
        header('Content-Disposition: attachment; filename="Fiche_Preparatoire_Exercice_Vierge"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');


        // Lire le fichier
        @readfile($file);

        return $this->redirectToRoute('convention_show', ['id' => $id]);
    }

     /**
     * @Route("/{id}/fiche-preparatoire-pdf", name="fiche_preparatoire_pdf", methods={"GET"})
     */
    public function showFichePreparatoire(Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        $file = $this->getParameter('app_conventions_directory') . '/' . $convention->getCheminFichePreparatoire();

        // Type de contenu de l'en-tête
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="Fiche_Preparatoire_Exercice.pdf"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');


        // Lire le fichier
        @readfile($file);

        return $this->redirectToRoute('convention_show', ['id' => $id]);
    }


    /**
     * @Route("/{id}", name="convention_delete", methods={"POST"})
     */
    public function delete(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $convention = $this->getDoctrine()->getRepository(Convention::class)->findOneBy(['id' => $decrypted_id]);

        if ($this->isCsrfTokenValid('delete' . $convention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($convention);
            $entityManager->flush();
        }

        return $this->redirectToRoute('convention_index', [], Response::HTTP_SEE_OTHER);
    }
}
