<?php

namespace App\Controller;

use App\Entity\Interventions;
use App\Entity\Photos;
use App\Form\PhotosType;
use App\Repository\AlbumRepository;
use App\Repository\DocumentsRepository;
use App\Repository\InterventionsRepository;
use App\Repository\PhotosRepository;
use App\Services\ContraintViolationToArray;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/album")
 */
class AlbumController extends AbstractController
{
    /**
     * @Route("/", options={"expose"=true}, name="album")
     */
    public function index(PhotosRepository $photo)
    {
        $dossier = $photo->findNomDossier();
        $photos = $photo->findBy(["dossier" => null]);

        return $this->render('album/index.html.twig', [
            'controller_name' => 'AlbumController',
            'photos' => $photos,
            'dossiers' => $dossier
        ]);
    }

    /**
     * @Route("/dossier/{nomdossier}", name="albumdossier")
     */
    public function dossier(PhotosRepository $photo, $nomdossier)
    {
        $photos = $photo->findBy(['dossier' => $nomdossier]);

        return $this->render('album/index.html.twig', [

            'photos' => $photos,
            'dossiers' => $nomdossier
        ]);
    }

    /**
     * @Route("/ajout", name="album_new")
     * @param AlbumRepository $albumRepository
     * @param InterventionsRepository $interventionsRepository
     * @param Request $request
     * @param ContraintViolationToArray $contraintViolationToArray
     * @param ValidatorInterface $validator
     * @return Response
     */
    function new(AlbumRepository $albumRepository, InterventionsRepository $interventionsRepository, Request $request, ContraintViolationToArray $contraintViolationToArray, ValidatorInterface $validator): Response
    {

        $root_path = "photos/";
        $files = [];
        $file_entities = [];
        $nb_files = 1;
        $file_errors = [];

        if ($request->isMethod('POST')) {

            $interventions = $interventionsRepository->findAll();

            if ($request->request->count() % 4 == 0) // On vérifie si on a bien 3 inputs par document dans le POST
                $nb_files = $request->request->count() / 4;
            else
                $error_msg = "Formulaire non-valide";


            for ($i = 0; $i < $nb_files; $i++) {

                $file_entities[$i] = new Photos();
                $files[$i] = ['file_name' => null, 'folder_name' => null, 'file' => null]; //initialisation des tableaux

                //Récupération des données
                $files[$i]['file_name'] = $request->request->get("file-name_" . ($i + 1));

                if($request->request->get("folder-select_" . ($i + 1)) != "null" && $request->request->get("intervention-select_" . ($i + 1)) == "null")
                    $files[$i]['folder_name'] = $request->request->get("folder-select_" . ($i + 1));
                else if($request->request->get("folder-select_" . ($i + 1)) == "null" && $request->request->get("intervention-select_" . ($i + 1)) != "null")
                    $files[$i]['folder_name'] = $request->request->get("intervention-select_" . ($i + 1));
                else if($request->request->get("folder-select_" . ($i + 1)) != "null" && $request->request->get("intervention-select_" . ($i + 1)) != "null")
                    $files[$i]['folder_name'] = $request->request->get("intervention-select_" . ($i + 1));
                else
                    $files[$i]['folder_name'] = $request->request->get("folder-input_" . ($i + 1));

                if (isset($_FILES['file-path_' . ($i + 1)]))
                    $files[$i]['file'] = $_FILES['file-path_' . ($i + 1)];

                //Initialisation des entités avec les données récupérées
                $file_entities[$i]->setNom($files[$i]['file_name'] ? $files[$i]['file_name'] : "");
                $file_entities[$i]->setDossier($files[$i]['folder_name'] ? $files[$i]['folder_name'] : null);

                $file_name = "";
                $folder_name = $file_entities[$i]->getDossier();

                if ($files[$i]['file']) {
                    if ($file_entities[$i]->getNom() == "") {
                        $file_name = $files[$i]['file']['name'];
                        $file_entities[$i]->setNom((explode('.', $file_name))[0]);
                    }
                    else {

                        $file_name = $file_entities[$i]->getNom() . '.' . pathinfo($files[$i]['file']['name'], PATHINFO_EXTENSION);
                    }
                }

                foreach ($interventions as $intervention){

                    if ($intervention->getNumIntervention() == $files[$i]['folder_name'])
                        $file_entities[$i]->setIdintervention($intervention);
                }

                if ($folder_name == "")
                    $file_entities[$i]->setChemin($root_path . $file_name);
                else
                    $file_entities[$i]->setChemin($root_path . $folder_name . '/' . $file_name);


                $file_entities[$i]->setDate(new \DateTime());

                $error = $validator->validate($file_entities[$i]);


                if (count($error) > 0)
                    $file_errors[$i+1] = $contraintViolationToArray->convert($error);


            }

            if (count($file_errors) == 0) { // Persist des documents + upload sur le serveur

                $entityManager = $this->getDoctrine()->getManager();

                for ($i = 0; $i < $nb_files; $i++) {

                    $entityManager->persist($file_entities[$i]);
                    $albumRepository->uploadPhoto($file_entities[$i], $files[$i]['file'], $root_path);
                }

                $entityManager->flush();

                if ($nb_files > 1)
                    $this->addFlash('add', 'Vos images ont été ajoutées avec succès !');
                else
                    $this->addFlash('add', 'Votre image a été ajoutée avec succès !');

                return new JsonResponse(json_encode($file_errors));
            }
            else {

                return new JsonResponse(json_encode($file_errors));
            }

        }

        return $this->render('album/new.html.twig', [
            'folders' => $albumRepository->findFoldersName(),
            'interventions' => $interventionsRepository->findAll()
        ]);
    }


    /**
     * @Route("/dossier/{nomdossier}/suppression_dossier", name="deleteDossierAlbum")
     */
    public function deleteDossier(AlbumRepository $albumRepository, PhotosRepository $photo, $nomdossier)
    {

        $chemin = "photos/" . $nomdossier;
        $albumRepository->deleteTree($chemin); // On vide le contenu de notre dossier
       
        $suppr = rmdir($chemin);

        if ($suppr == true) {
            $photo->DeleteDossier($nomdossier);
            $this->addFlash('add', 'Dossier supprimé !');
        }


        return $this->redirectToRoute('album');

    }

    /**
     * @Route("/photo/{idphoto}/supprimer", name="DELETEPhotos")
     */
    public function deletePhoto($idphoto,PhotosRepository $photo)
    {
        
        $chemin = $this->getDoctrine()->getRepository("App:Photos")->find($idphoto)->getChemin();


        $verif = unlink($chemin);

        if ($verif == true) {
            $photo->DELETEPhotos($idphoto);
            $this->addFlash('add', 'Photo supprimée !');
        }


         return $this->redirectToRoute('album');

    }


}
