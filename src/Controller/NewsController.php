<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\NewsArchive;
use App\Entity\Regarder;
use App\Form\NewsType;
use App\Repository\DocumentsRepository;
use App\Repository\NewsArchiveRepository;
use App\Repository\NewsRepository;
use App\Repository\RegarderRepository;
use App\Repository\UserRepository;
use App\Services\Encryption;
use Doctrine\ORM\EntityManager;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/news")
 * @isGranted("ROLE_EQUIPIER")
 */
class NewsController extends AbstractController
{
    private const ROOT_PATH = "ArchiveNews/";

    /**
     * @Route("/", name="news_index", methods={"GET"})
     */
    public function index(NewsRepository $newsRepository, Request $request): Response
    {
        $filter = $request->get('filter');

        if ($filter == "none" )
            $filter = null;

        $news = $newsRepository->findAllWithFilter($filter);

        if(! $filter)
            $yearsNews = $newsRepository->findYearsNews();

        if ($request->get('ajax'))
        {
            return new JsonResponse([
                'content'=> $this->renderView('news/_content.html.twig', ['news' => $news ])
            ]);
        }


        return $this->render('news/index.html.twig', [
            'news' => $news,
            'yearsNews' => $yearsNews
        ]);
    }

    /**
     * @Route("/ajout", name="news_new", methods={"GET","POST"})
     */
    public function new(UserRepository $userRepository, Request $request): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        $users = $this->getDoctrine()->getRepository('App\Entity\User')->findAll();
        $usersCount = count($users);


        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            for($i=0; $i < $usersCount; $i++) {
                $newsViewArray[$i] = new Regarder();
                $newsViewArray[$i]->setUser($users[$i]);
                $newsViewArray[$i]->setNews($news);
                $newsViewArray[$i]->setVue(false);

                $entityManager->persist($newsViewArray[$i]);
            }

            $news->setAuteur($this->getUser());
            $news->setDate(new \DateTime());

            $entityManager->persist($news);
            $entityManager->flush();

            $this->addFlash('add', 'Actualité ajoutée !');

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/new.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/vue", options={"expose"=true}, name="newsView")
     * @param RegarderRepository $regarderRepository
     * @return Response
     */
    public function newsViewAction(RegarderRepository $regarderRepository){

        $newsNotView = $regarderRepository->findAllNewsByUserId($this->getUser());

        $em = $this->getDoctrine()->getManager();

        foreach ($newsNotView as $news)
        {
            $news->setVue(true);
        }

        $em->flush();

        exit();
    }

    /**
     * @Route("/archives", name="archives_index")
     * @param NewsArchiveRepository $archiveRepository
     * @return Response
     */
    public function indexArchives(NewsArchiveRepository $archiveRepository): Response
    {
        $dossiers = $archiveRepository->findFoldersName();

        return $this->render('news/archive.html.twig', [
            'dossiers' => $dossiers

        ]);
    }

    /**
     * @Route("/archives/{folder}", name="newsInFolder")
     * @param DocumentsRepository $documentsRepository
     * @param $nomdossier
     * @return Response
     */
    public function showFolderContent(NewsArchiveRepository $archiveRepository, $folder)
    {

        $news = $archiveRepository->findBy(['newsDossier' => $folder]);


        return $this->render('news/archive.html.twig', [

            'newsArchives' => $news

        ]);
    }


    /**
     * @Route("/archive/pdf/{id}", name="news_pdf", methods={"GET"})
     * @param NewsArchiveRepository $archiveRepository
     * @param Encryption $encryption
     * @param $id
     * @return Response
     */
    public function rapportPdf(NewsArchiveRepository $archiveRepository, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $news = $archiveRepository->findOneBy(['idNewsArchive'=>$decrypted_id]);

        if ($news){
            $file = $news->getNewsChemin();


            // Type de contenu de l'en-tête
            header('Content-type: application/pdf');

            header('Content-Disposition: inline; filename="' . $news->getNewsNom() . '"');

            header('Content-Transfer-Encoding: binary');

            header('Accept-Ranges: bytes');

            // Lire le fichier
            @readfile($file);
        }

        return $this->redirectToRoute('archives_index');
    }


    /**
     * @Route("/{idnews}", name="news_show", methods={"GET"})
     */
    public function show(Encryption $encryption, $idnews): Response
    {
        $decrypted_idnews = $encryption->decryptionValue($idnews);

        if ($decrypted_idnews == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $news = $this->getDoctrine()->getRepository(News::class)->findOneBy(['idnews'=>$decrypted_idnews]);

        return $this->render('news/show.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @Route("/{idnews}/modification", name="news_edit", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function edit(Encryption $encryption, Request $request, $idnews): Response
    {
        $decrypted_idnews = $encryption->decryptionValue($idnews);

        if ($decrypted_idnews == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $news = $this->getDoctrine()->getRepository(News::class)->findOneBy(['idnews'=>$decrypted_idnews]);

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('add', 'Actualité modifiée !');

            return $this->redirectToRoute('news_index');
        }

        return $this->render('news/edit.html.twig', [
            'news' => $news,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idnews}", name="news_delete", methods={"POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Encryption $encryption, Request $request, $idnews, NewsArchiveRepository $archiveRepository): Response
    {
        $decrypted_idnews = $encryption->decryptionValue($idnews);

        if ($decrypted_idnews == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $news = $this->getDoctrine()->getRepository(News::class)->findOneBy(['idnews'=>$decrypted_idnews]);

        if ($this->isCsrfTokenValid('delete'.$news->getIdnews(), $request->request->get('_token'))) {

            $newsArchive = new NewsArchive();
            $nbNewsUpload = $archiveRepository->countNews();

            $html = $this->renderView('news/_pdf.html.twig', ['news' => $news]);

            $options = new Options();
            $options->set('isRemoteEnabled',TRUE);
            $options->set('defaultFont', 'sans-serif');
            $options->set('debugKeepTemp', TRUE);
            $options->set('isHtml5ParserEnabled', TRUE);

            $dompdf = new Dompdf( $options );

            $dompdf->loadHtml($html);

            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();

            // Parameters
            $x = 520;
            $y = 815;
            $text = "{PAGE_NUM} sur {PAGE_COUNT}";
            $font = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
            $size = 10;
            $color = array(0, 0, 0);
            $word_space = 0.0;
            $char_space = 0.0;
            $angle = 0.0;

            $dompdf->getCanvas()->page_text(
                $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
            );

            $newsArchive->setNewsNom($news->getTitre());
            $newsArchive->setNewsDossier($news->getDate()->format('Y'));
            $newsArchive->setNewsChemin(self::ROOT_PATH.$news->getDate()->format('Y').'/news_'.($nbNewsUpload + 1).'.pdf');

            $archiveRepository->uploadNews($dompdf,$newsArchive,self::ROOT_PATH);

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($newsArchive);
            $entityManager->remove($news);
            $entityManager->flush();

            $this->addFlash('add', 'Actualité supprimée !');
        }

        return $this->redirectToRoute('news_index');
    }


}
