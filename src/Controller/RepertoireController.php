<?php

namespace App\Controller;

use App\Entity\Categorierepertoire;
use App\Entity\Repertoire;
use App\Form\RepertoireType;
use App\Repository\CategorierepertoireRepository;
use App\Repository\RepertoireRepository;
use App\Services\Encryption;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * @Route("/repertoire")
 * @isGranted("ROLE_EQUIPIER")
 */
class RepertoireController extends AbstractController
{
    /**
     * @Route("/", name="repertoire_index", methods={"GET"})
     */
    public function index(CategorierepertoireRepository $categorieRepository, RepertoireRepository $repertoireRepository, Request $request): Response
    {

        $repertoires = null;
        $categories = $categorieRepository->getAllOrderByRank();

        $filter = $request->get('filter');

        if(in_array($filter,$categories))
        {
            $repertoires = $repertoireRepository->getAllRepertoire($filter);
        }
        else{
            $repertoires = $repertoireRepository->getAllRepertoire();
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('repertoire/_content.html.twig', ['repertoires' => $repertoires ])
            ]);
        }
        return $this->render('repertoire/index.html.twig', [
            'repertoires' => $repertoires,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/ajout", name="repertoire_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $repertoire = new Repertoire();
        $form = $this->createForm(RepertoireType::class, $repertoire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($repertoire);
            $entityManager->flush();
            $this->addFlash('add', 'Contact enregistré !');

            return $this->redirectToRoute('repertoire_index');
        }

        return $this->render('repertoire/new.html.twig', [
            'repertoire' => $repertoire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pdf/{filter}", options={"expose"=true}, defaults={"filter"= "none"}, name="pdfRepertoire")
     * @IsGranted("ROLE_ADMIN")
     */
    public function generatePdfRepertoire(CategorierepertoireRepository $categorierepertoireRepository, RepertoireRepository $repertoireRepository, $filter)
    {

        $repertoires = null;
        $categories = $categorierepertoireRepository->getAllOrderByRank();

        if ($filter == "none" ){
            $filter = null;
        }

        if(in_array($filter,$categories))
        {
            $repertoires = $repertoireRepository->getAllRepertoire($filter);
        }
        else{
            $repertoires = $repertoireRepository->getAllRepertoire();
        }

        // On génère l'HTML et on créé le PDF avec.
        $html = $this->renderView('repertoire/_pdf.html.twig', ['repertoires' => $repertoires,]);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4',"landscape");
        $dompdf->render();

        // Parameters
        $x = 790;
        $y = 570;
        $text = "{PAGE_NUM} sur {PAGE_COUNT}";
        $font = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
        $size = 10;
        $color = array(0, 0, 0);
        $word_space = 0.0;
        $char_space = 0.0;
        $angle = 0.0;

        $dompdf->getCanvas()->page_text(
            $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
        );

        $dompdf->stream("Repertoire_Pro.pdf", ['Attachment' => 0]);

        exit();
    }

    /**
     * @Route("/{idrepertoire}/informations", name="repertoire_show", methods={"GET"})
     */
    public function show(Encryption $encryption, RepertoireRepository $repertoireRepository, $idrepertoire): Response
    {
        $decrypted_id = $encryption->decryptionValue($idrepertoire);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $info_people = $repertoireRepository->findById($decrypted_id);

        return $this->render('repertoire/show.html.twig', [
            'repertoire' => $info_people,
        ]);
    }

    /**
     * @Route("/{idrepertoire}/modifier", name="repertoire_edit", methods={"GET","POST"})
     */
    public function edit(Encryption $encryption, RepertoireRepository $repertoireRepository, Request $request, $idrepertoire): Response
    {
        $decrypted_id = $encryption->decryptionValue($idrepertoire);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $repertoire = $repertoireRepository->findById($decrypted_id);

        $form = $this->createForm(RepertoireType::class, $repertoire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('add', 'Contact modifié !');

            return $this->redirectToRoute('repertoire_index');
        }

        return $this->render('repertoire/edit.html.twig', [
            'repertoire' => $repertoire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idrepertoire}", name="repertoire_delete", methods={"DELETE"})
     */
    public function delete(Encryption $encryption, RepertoireRepository $repertoireRepository, Request $request, $idrepertoire): Response
    {
        $decrypted_id = $encryption->decryptionValue($idrepertoire);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $repertoire = $repertoireRepository->findById($decrypted_id);

        if ($this->isCsrfTokenValid('delete'.$repertoire->getIdrepertoire(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($repertoire);
            $entityManager->flush();
            $this->addFlash('add', 'Contact supprimé !');
        }

        return $this->redirectToRoute('repertoire_index');
    }

}
