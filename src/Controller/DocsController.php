<?php

namespace App\Controller;

use App\Entity\Documents;
use App\Form\DocumentsType;
use App\Repository\DocumentsRepository;
use App\Services\ContraintViolationToArray;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/documents")
 */
class DocsController extends AbstractController
{

    /**
     * @Route("/", options={"expose"=true}, name="docs")
     */
    public function index(DocumentsRepository $documentsRepository)
    {

        $dossier = $documentsRepository->findFoldersName();
        $docs = $documentsRepository->findBy(["dossier" => null]);

        return $this->render('documents/index.html.twig', [

            'dossiers' => $dossier,
            'docs' => $docs,
        ]);
    }

    /**
     * @Route("/dossier/{nomdossier}", name="docDossier")
     * @param DocumentsRepository $documentsRepository
     * @param $nomdossier
     * @return Response
     */
    public function dossier_show(DocumentsRepository $documentsRepository, $nomdossier)
    {

        $documents = $documentsRepository->findBy(['dossier' => $nomdossier]);


        return $this->render('documents/index.html.twig', [

            'docs' => $documents,
            'dossiers' => $nomdossier
        ]);
    }

    /**
     * @Route("/new", name="docs_new")
     */
    function new(DocumentsRepository $documentsRepository, Request $request, ContraintViolationToArray $contraintViolationToArray, ValidatorInterface $validator): Response
    {

        $root_path = "documents/";
        $files = [];
        $file_entities = [];
        $nb_files = 1;
        $file_errors = [];

        if ($request->isMethod('POST')) {


            if ($request->request->count() % 3 == 0) // On vérifie si on a bien 3 inputs par document dans le POST
                $nb_files = $request->request->count() / 3;
            else
                $error_msg = "Formulaire non-valide";

            for ($i = 0; $i < $nb_files; $i++) {

                $file_entities[$i] = new Documents();
                $files[$i] = ['file_name' => null, 'folder_name' => null, 'file' => null]; //initialisation des tableaux

                //Récupération des données
                $files[$i]['file_name'] = $request->request->get("file-name_" . ($i + 1));

                if ($request->request->get("folder-select_" . ($i + 1)) == "null")
                    $files[$i]['folder_name'] = $request->request->get("folder-input_" . ($i + 1));
                else
                    $files[$i]['folder_name'] = $request->request->get("folder-select_" . ($i + 1));

                if (isset($_FILES['file-path_' . ($i + 1)]))
                    $files[$i]['file'] = $_FILES['file-path_' . ($i + 1)];

                //Initialisation des entités avec les données récupérées
                $file_entities[$i]->setNom($files[$i]['file_name'] ? $files[$i]['file_name'] : "");
                $file_entities[$i]->setDossier($files[$i]['folder_name'] ? $files[$i]['folder_name'] : null);

                $file_name = "";
                $folder_name = $file_entities[$i]->getDossier();

                if ($files[$i]['file']) {
                    if ($file_entities[$i]->getNom() == "") {
                        $file_name = $files[$i]['file']['name'];
                        $file_entities[$i]->setNom((explode('.', $file_name))[0]);
                    }
                    else {

                        $file_name = $file_entities[$i]->getNom() . '.' . pathinfo($files[$i]['file']['name'], PATHINFO_EXTENSION);
                    }
                }

                if ($folder_name == "")
                    $file_entities[$i]->setChemin($root_path . $file_name);
                else
                    $file_entities[$i]->setChemin($root_path . $folder_name . '/' . $file_name);

                $error = $validator->validate($file_entities[$i]);

                if (count($error) > 0)
                    $file_errors[$i+1] = $contraintViolationToArray->convert($error);


            }

            if (count($file_errors) == 0) { // Persist des documents + upload sur le serveur

                $entityManager = $this->getDoctrine()->getManager();

                for ($i = 0; $i < $nb_files; $i++) {

                    $entityManager->persist($file_entities[$i]);
                    $documentsRepository->uploadDocument($file_entities[$i], $files[$i]['file'], $root_path);
                }

                $entityManager->flush();

                if ($nb_files > 0)
                    $this->addFlash('add', 'Vos documents ont été ajoutés avec succès !');
                else
                    $this->addFlash('add', 'Votre document a été ajouté avec succès !');

                return new JsonResponse(json_encode($file_errors));
            }
            else {

                return new JsonResponse(json_encode($file_errors));
            }

        }

        return $this->render('documents/new.html.twig', [
            'folders' => $documentsRepository->findFoldersName()
        ]);
    }


    /**
     * @Route("/{id}", name="docs_show")
     * @param DocumentsRepository $documentsRepository
     * @param string $id
     * @return Response
     */
    public function show(DocumentsRepository $documentsRepository, string $id): Response
    {
        $folder = $documentsRepository->find($id);

        if ($folder){
            $file = $folder->getChemin();


            // Type de contenu de l'en-tête
            header('Content-type: application/pdf');

            header('Content-Disposition: inline; filename="' . $folder->getNom() . '"');

            header('Content-Transfer-Encoding: binary');

            header('Accept-Ranges: bytes');

            // Lire le fichier
            @readfile($file);
        }

        return $this->redirectToRoute('docs');
    }

    /**
     * @Route("/{nomdossier}/delete", name="deleteDossier")
     */
    public function deleteDossier(DocumentsRepository $docRepo, $nomdossier)
    {

        $chemin = "Docs/" . $nomdossier;
        $docRepo->deleteTree($chemin);
        $suppr = rmdir($chemin);
        if ($suppr == true) {
            $docRepo->DeleteDossier($nomdossier);
            $this->addFlash('add', 'Dossier supprimé !');
        }
        return $this->redirectToRoute('docs');

    }

    /**
     * @Route("/documents/{doc}/delete", name="DELETEDoc")
     */
    public function deleteDoc(DocumentsRepository $docRepo, $doc)
    {

        $chemin = $this->getDoctrine()->getRepository("App:Documents")->find($doc)->getChemin();


        $verif = unlink($chemin);
        if ($verif == true) {
            $docRepo->DELETEDoc($doc, $chemin);
            $this->addFlash('add', 'Document supprimé !');
        }


        return $this->redirectToRoute('docs');

    }
}
