<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\IntervenirRepository;
use App\Repository\InterventionsRepository;
use Dompdf\Dompdf;
use Ob\HighchartsBundle\Highcharts\Highchart;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class StatsController extends AbstractController
{

    /**
     * @Route("/stats", name="stats")
     * @isGranted("ROLE_CHEF")
     */
    public function statAction(InterventionsRepository $interventionsRepository, IntervenirRepository $intervenirRepository, Request $request)
    {
        $yearWithInter = $interventionsRepository->findYearsInter();
        $filter = $request->get('filter');
        $filterOne = $request->get('filter_range_min');
        $filterTwo = $request->get('filter_range_max');


        if ($filter != null) {
            $filterOne = $filter;

        }

        if ($filterOne != null) {
            if ($filterOne == "none") {
                $filterOne = null;
            }

        }

        if ($filterTwo != null) {
            if ($filterTwo == "none") {
                $filterTwo = null;
            }

        }

        $interventionsbyCommune = $interventionsRepository->statsInterventionByCommune($filterOne, $filterTwo);
        $techniquesUsedPerIntervention = $interventionsRepository->statsTechniquesUsedPerIntervention($filterOne, $filterTwo);
        $materielsUsedPerIntervention = $interventionsRepository->statsMaterielsUsedPerIntervention($filterOne, $filterTwo);
        $statusStats = $this->getStatsInterventions($interventionsRepository, $filterOne, $filterTwo);

        if ($request->get('ajax')) {
            $content = $this->renderView('stats/_content.html.twig', ['interventionsbyCommune' => $interventionsbyCommune, 'techniquesUsedPerIntervention' => $techniquesUsedPerIntervention, 'materielsUsedPerIntervention' => $materielsUsedPerIntervention, 'statusStats' => $statusStats]);
            return new JsonResponse([
                'content' => $content]);
        }
        return $this->render('stats/index.html.twig', [
            'interventionsbyCommune' => $interventionsbyCommune,
            'techniquesUsedPerIntervention' => $techniquesUsedPerIntervention,
            'materielsUsedPerIntervention' => $materielsUsedPerIntervention,
            'statusStats' => $statusStats,
            'yearInter' => $yearWithInter
        ]);
    }


    /**
     * @Route("/pdf/{type}", options={"expose"=true}, defaults={"type"= "all"}, name="pdfStats")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     */
    public function generatePdfStats(InterventionsRepository $interventionsRepository, Request $request, $type)
    {

        $interventionsbyCommune = null;
        $techniquesUsedPerIntervention = null ;
        $materielsUsedPerIntervention = null ;
        $statusStats = null;

        $filename = "Stats";
        $filter = $request->get('filter');
        $filterOne = $request->get('filter_range_min');
        $filterTwo = $request->get('filter_range_max');

        if ($filter != null)
            $filterOne = $filter;

        if ($filterOne != null) {
            if ($filterOne == "none")
                $filterOne = null;
        }

        if ($filterTwo != null) {
            if ($filterTwo == "none")
                $filterTwo = null;
        }

        if ($type == 'inter') {
            $statusStats = $this->getStatsInterventions($interventionsRepository, $filterOne, $filterTwo);
            $filename .= "_Interventions.pdf";
        } 
        elseif ($type == 'commune') {
            $interventionsbyCommune = $interventionsRepository->statsInterventionByCommune($filterOne, $filterTwo);
            $filename .= "_Communes.pdf";
        }
        elseif ($type == 'materiel') {
            $materielsUsedPerIntervention = $interventionsRepository->statsMaterielsUsedPerIntervention($filterOne, $filterTwo);
            $filename .= "_Materiel.pdf";
        } 
        elseif ($type == 'technique') {
            $techniquesUsedPerIntervention = $interventionsRepository->statsTechniquesUsedPerIntervention($filterOne, $filterTwo);
            $filename .= "_Techniques.pdf";
        }  
        elseif ($type == 'all') {
            $statusStats = $this->getStatsInterventions($interventionsRepository, $filterOne, $filterTwo);
            $interventionsbyCommune = $interventionsRepository->statsInterventionByCommune($filterOne, $filterTwo);
            $techniquesUsedPerIntervention = $interventionsRepository->statsTechniquesUsedPerIntervention($filterOne, $filterTwo);
            $materielsUsedPerIntervention = $interventionsRepository->statsMaterielsUsedPerIntervention($filterOne, $filterTwo);
            $filename .= "_Interventions&Communes&Techniques&Materiels.pdf";
        }

        // On génère l'HTML et on créé le PDF avec.
        $html = $this->renderView('stats/_pdf.html.twig', ['interventionsbyCommune' => $interventionsbyCommune, 'statusStats' => $statusStats, 'techniquesUsedPerIntervention' => $techniquesUsedPerIntervention, 'materielsUsedPerIntervention' => $materielsUsedPerIntervention, 'type' => $type]);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4');
        $dompdf->render();

        // Parameters
        $x = 505;
        $y = 790;
        $text = "{PAGE_NUM} sur {PAGE_COUNT}";
        $font = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
        $size = 10;
        $color = array(0, 0, 0);
        $word_space = 0.0;
        $char_space = 0.0;
        $angle = 0.0;

        $dompdf->getCanvas()->page_text(
            $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
        );

        $dompdf->stream($filename, ['Attachment' => 0]);

        exit();
    }

    private function getStatsInterventions($interventionsRepository, $filterOne, $filterTwo): array
    {

        $allSpeaker = $this->getDoctrine()->getRepository(User::class)->findAll();
        $interventionsByUser = $interventionsRepository->statsInterventionsByUser($filterOne, $filterTwo);
        $restByUser = $interventionsRepository->restByUsers($filterOne, $filterTwo);
        $penaltiesByUser = $interventionsRepository->penaltyByUsers($filterOne, $filterTwo);
        $guardsByUser = $interventionsRepository->guardByUsers($filterOne, $filterTwo);
        $interventionsStats = null;

        foreach ($allSpeaker as $key => $speaker) {

            $interventionsStats[$key] = ['user' => $speaker, 'nbinterventions' => 0, 'pourcentage' => 0, 'rest' => 0, 'penalty' => 0, 'guard' => 0];
        }

        for ($i = 0; $i < count($allSpeaker); $i++) {

            for ($j = 0; $j < count($interventionsByUser); $j++) {

                if ($interventionsByUser[$j]['id'] == $interventionsStats[$i]['user']->getId()) {
                    $interventionsStats[$i]['nbinterventions'] = $interventionsByUser[$j]['nbinterventions'];
                    $interventionsStats[$i]['pourcentage'] = $interventionsByUser[$j]['pourcentage'];
                    break;
                }

            }
        
            for ($j = 0; $j < count($restByUser); $j++) {
                if ($restByUser[$j]['id'] == $interventionsStats[$i]['user']->getId()) {
                    $interventionsStats[$i]['rest'] = $restByUser[$j]['nbrest'];
                    break;
                }

            }

            for ($j = 0; $j < count($penaltiesByUser); $j++) {
                if ($penaltiesByUser[$j]['id'] == $interventionsStats[$i]['user']->getId()) {
                    $interventionsStats[$i]['penalty'] = $penaltiesByUser[$j]['nbpenalty'];
                    break;
                }

            }

            for ($j = 0; $j < count($guardsByUser); $j++) {
                if ($guardsByUser[$j]['id'] == $interventionsStats[$i]['user']->getId()) {
                    $interventionsStats[$i]['guard'] = $guardsByUser[$j]['nbguard'];
                    break;
                }
            }
        
        }
        array_multisort(array_column($interventionsStats, 'pourcentage'), SORT_DESC, $interventionsStats); // tri tableau -> pourcentage ordre décroissant

        return $interventionsStats;
    }

}