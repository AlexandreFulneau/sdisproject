<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\RegarderRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\SerializerInterface;

class SecurityController extends AbstractController
{

    /**
     * @Route(name="api_login", path="/api/login_check")
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function api_login(SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();

      if ($user) {
          return new JsonResponse([
              'email' => $user->getUsername(),
              'roles' => $user->getRoles(),
          ]);
      }

        return new JsonResponse($serializer->serialize([
            'code' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => "Invalid credentials."
        ],"json")
        ,
            JsonResponse::HTTP_UNAUTHORIZED,
            [],
            true
        );

    }

    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        if ($error)
            $error = "Identifiant ou mot de passe incorrect";
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        if($this->isGranted('IS_AUTHENTICATED_FULLY')==true){
            return $this->redirectToRoute('home');
            //render("home/index_order.html.twig");
        }else{
            return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        }

        
    }

    /**
     * @Route("/logout", options={"expose"=true}, name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }


    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {

        if ($request->isMethod('POST')) {
            $status[]= $request->request->get('status');
            $user = new User();
            $user->setEmail($request->request->get('email'));
            $user->setPrenom($request->request->get('prenom'));
            $user->setNom($request->request->get('nom'));
           
            $user->setRoles($status);
    
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $em = $this->getDoctrine()->getManager();
            $this->addFlash('add', 'Utilisateur enregistré !');

            $em->persist($user);
            $em->flush();
        
        }

        return $this->render('compte/ajout.html.twig');
    }

}
