<?php

namespace App\Controller;

use App\Entity\CTD;
use App\Entity\Motif;
use App\Entity\Ville;
use App\Form\CTDType;
use App\Entity\Filiere;
use App\Form\MotifType;
use App\Form\VilleType;
use App\Entity\GradeUser;
use App\Entity\Technique;
use App\Form\FiliereType;
use App\Entity\StatutFMPA;
use App\Form\GradeUserType;
use App\Form\TechniqueType;
use App\Entity\FonctionUser;
use App\Entity\Typemateriel;
use App\Form\StatutFMPAType;
use App\Services\Encryption;
use App\Entity\CentresSecours;
use App\Entity\TypeConvention;
use App\Form\FonctionUserType;
use App\Form\TypeMaterielType;
use App\Form\CentresSecoursType;
use App\Form\TypeConventionType;
use App\Repository\CTDRepository;
use App\Entity\LieuxInterventions;
use App\Entity\Categorierepertoire;
use App\Repository\MotifRepository;
use App\Repository\VilleRepository;
use App\Form\LieuxInterventionsType;
use App\Form\CategorieRepertoireType;
use App\Repository\FiliereRepository;
use App\Repository\GradeUserRepository;
use App\Repository\TechniqueRepository;
use App\Repository\StatutFMPARepository;
use App\Entity\NomEquipementOperationnel;
use App\Repository\FonctionUserRepository;
use App\Repository\TypematerielRepository;
use App\Form\NomEquipementOperationnelType;
use App\Repository\CentresSecoursRepository;
use App\Repository\TypeConventionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategorierepertoireRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\NomEquipementOperationnelRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{

    /**
     * @Route("/", name="admin_index")
     * @param Request $request
     * @param VilleRepository $villeRepository
     * @param MotifRepository $motifRepository
     * @param TypematerielRepository $typematerielRepository
     * @param CentresSecoursRepository $centresSecoursRepository
     * @param FonctionUserRepository $fonctionUserRepository
     * @param GradeUserRepository $gradeUserRepository
     * @param CategorierepertoireRepository $categorierepertoireRepository
     * @return Response
     */
    public function index(Request $request, VilleRepository $villeRepository, MotifRepository $motifRepository, TypematerielRepository $typematerielRepository,
                          CentresSecoursRepository $centresSecoursRepository, FonctionUserRepository $fonctionUserRepository, GradeUserRepository $gradeUserRepository
    , CategorierepertoireRepository $categorierepertoireRepository): Response
    {
        if($request->get('type_form') == "ville"){

            $ville = new Ville();
            $form_new = $this->createForm(VilleType::class, $ville, ['action' => $this->generateUrl('ville_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'villes'=> $villeRepository->getAllOrderByName()
            ]);
        }
        elseif ($request->get('type_form') == "motif"){
            $motif = new Motif();
            $form_new = $this->createForm(MotifType::class, $motif, ['action' => $this->generateUrl('motif_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'motifs'=> $motifRepository->getAllOrderByName()
            ]);
        }
        elseif ($request->get('type_form') == "type_materiel"){
            $type_materiel = new Typemateriel();
            $form_new = $this->createForm(TypeMaterielType::class, $type_materiel, ['action' => $this->generateUrl('type_materiel_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'types_materiel'=> $typematerielRepository->getAllOrderByName()
            ]);
        }
        elseif ($request->get('type_form') == "centres_secours"){
            $centres_secours = new CentresSecours();
            $form_new = $this->createForm(CentresSecoursType::class, $centres_secours, ['action' => $this->generateUrl('centres_secours_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'centres_secours'=> $centresSecoursRepository->getAllOrderByName()
            ]);
        }
        elseif ($request->get('type_form') == "fonction_user"){
            $fonction_user = new FonctionUser();
            $form_new = $this->createForm(FonctionUserType::class, $fonction_user, ['action' => $this->generateUrl('fonction_user_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'fonctions_user'=> $fonctionUserRepository->getAllOrderByRank()
            ]);
        }
        elseif ($request->get('type_form') == "grade_user"){
            $grade_user = new GradeUser();
            $form_new = $this->createForm(GradeUserType::class, $grade_user, ['action' => $this->generateUrl('grade_user_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'grades_user'=> $gradeUserRepository->getAllOrderByRank()
            ]);
        }
        elseif ($request->get('type_form') == "categories_repertoire"){
            $categorie_repertoire = new Categorierepertoire();
            $form_new = $this->createForm(CategorieRepertoireType::class, $categorie_repertoire, ['action' => $this->generateUrl('categories_repertoire_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'categories_repertoire'=> $categorierepertoireRepository->getAllOrderByRank()
            ]);
        }
        elseif ($request->get('type_form') == "nom_equipement"){
            $lieu_intervention = new NomEquipementOperationnel();
            $form_new = $this->createForm(NomEquipementOperationnelType::class, $lieu_intervention, ['action' => $this->generateUrl('nom_equipement_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'noms_equipement'=> $this->getDoctrine()->getRepository(NomEquipementOperationnel::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "lieux_interventions"){
            $lieu_intervention = new LieuxInterventions();
            $form_new = $this->createForm(LieuxInterventionsType::class, $lieu_intervention, ['action' => $this->generateUrl('lieux_interventions_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'lieux_interventions'=> $this->getDoctrine()->getRepository(LieuxInterventions::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "technique"){
            $technique = new Technique();
            $form_new = $this->createForm(TechniqueType::class, $technique, ['action' => $this->generateUrl('technique_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'techniques'=> $this->getDoctrine()->getRepository(Technique::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "statut_fmpa"){
            $statut_fmpa = new StatutFMPA();
            $form_new = $this->createForm(StatutFMPAType::class, $statut_fmpa, ['action' => $this->generateUrl('statut_fmpa_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'statut_fmpas'=> $this->getDoctrine()->getRepository(StatutFMPA::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "filiere"){
            $filiere = new Filiere();
            $form_new = $this->createForm(FiliereType::class, $filiere, ['action' => $this->generateUrl('filiere_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'filieres'=> $this->getDoctrine()->getRepository(Filiere::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "CTD"){
            $CTD = new CTD();
            $form_new = $this->createForm(CTDType::class, $CTD, ['action' => $this->generateUrl('CTD_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'CTDs'=> $this->getDoctrine()->getRepository(CTD::class)->findAll()
            ]);
        }
        elseif ($request->get('type_form') == "type_convention"){
            $type_convention = new TypeConvention();
            $form_new = $this->createForm(TypeConventionType::class, $type_convention, ['action' => $this->generateUrl('type_convention_new')]);
            $form_new->handleRequest($request);

            return $this->render('admin/index.html.twig', [
                'form_new' => $form_new->createView(),
                'type_form' => $request->get('type_form'),
                'type_conventions'=> $this->getDoctrine()->getRepository(TypeConvention::class)->findAll()
            ]);
        }

        return $this->render('admin/index.html.twig');
    }

    /**************************************************************************************************************************/
    /******************************************************   VILLE   *********************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/ville/ajout", options={"expose"=true}, name="ville_new", methods={"GET","POST"})
     */
    public function ville_new(Request $request, VilleRepository $villeRepository) :Response
    {
        $ville = new Ville();
        $form_new = $this->createForm(VilleType::class, $ville, ['action' => $this->generateUrl('ville_new')]);
        $form_new->handleRequest($request);

        $villes = $villeRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ville);
            $entityManager->flush();
            $this->addFlash('add', 'Ville enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "ville",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "ville",
                    'villes'=> $villes
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "ville",
            'villes'=> $villeRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/ville/modifier/{id}", options={"expose"=true}, name="ville_edit", methods={"GET","POST"})
     */
    public function ville_edit(Encryption $encryption, Request $request, VilleRepository $villeRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $villes = $this->getDoctrine()->getRepository(Ville::class)->findOneBy(['idville'=>$decrypted_id]);
        $form_edit = $this->createForm(VilleType::class, $villes, ['action' => $this->generateUrl('ville_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Ville modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "ville",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_ville_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(VilleType::class, new Ville(), ['action' => $this->generateUrl('ville_new')])->createView(),
            'type_form' => "ville",
            'villes'=> $villeRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/ville/supprimer/{id}", name="ville_delete", methods={"DELETE"})
     */
    public function delete_ville(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $ville = $this->getDoctrine()->getRepository(Ville::class)->findOneBy(['idville'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$ville->getIdville(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ville);
            $entityManager->flush();
            $this->addFlash('add', 'Ville supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "ville",
        ]);
    }


    /**************************************************************************************************************************/
    /******************************************************   MOTIF   *********************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/motif/ajout", options={"expose"=true}, name="motif_new", methods={"GET","POST"})
     */
    public function motif_new(Request $request, MotifRepository $motifRepository) :Response
    {
        $motif = new Motif();
        $form_new = $this->createForm(MotifType::class, $motif, ['action' => $this->generateUrl('motif_new')]);
        $form_new->handleRequest($request);

        $motifs = $motifRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($motif);
            $entityManager->flush();
            $this->addFlash('add', 'Motif enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "motif",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "motif",
                    'motifs'=> $motifs
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "motif",
            'motifs'=> $motifRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/motif/modifier/{id}", options={"expose"=true}, name="motif_edit", methods={"GET","POST"})
     */
    public function motif_edit(Encryption $encryption, Request $request, MotifRepository $motifRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $motif = $this->getDoctrine()->getRepository(Motif::class)->findOneBy(['idmotif'=>$decrypted_id]);
        $form_edit = $this->createForm(MotifType::class, $motif, ['action' => $this->generateUrl('motif_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Motif modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "motif",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_motif_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(MotifType::class, new Motif(), ['action' => $this->generateUrl('motif_new')])->createView(),
            'type_form' => "motif",
            'motifs'=> $motifRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/motif/supprimer/{id}", name="motif_delete", methods={"DELETE"})
     */
    public function delete_motif(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $motif = $this->getDoctrine()->getRepository(Motif::class)->findOneBy(['idmotif'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$motif->getIdmotif(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($motif);
            $entityManager->flush();
            $this->addFlash('add', 'Motif supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "motif",
        ]);
    }


    /**************************************************************************************************************************/
    /***************************************************    TYPES MATÉRIEL    *************************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/type_materiel/ajout", options={"expose"=true}, name="type_materiel_new", methods={"GET","POST"})
     */
    public function type_materiel_new(Request $request, TypematerielRepository $typematerielRepository) :Response
    {
        $type_materiel = new Typemateriel();
        $form_new = $this->createForm(TypeMaterielType::class, $type_materiel, ['action' => $this->generateUrl('type_materiel_new')]);
        $form_new->handleRequest($request);

        $types_materiel = $typematerielRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($type_materiel);
            $entityManager->flush();
            $this->addFlash('add', 'Type enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "type_materiel",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "type_materiel",
                    'types_materiel'=> $types_materiel
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "type_materiel",
            'types_materiel'=> $typematerielRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/type_materiel/modifier/{id}", options={"expose"=true}, name="type_materiel_edit", methods={"GET","POST"})
     */
    public function type_materiel_edit(Encryption $encryption, Request $request,TypematerielRepository $typematerielRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $type_materiel = $this->getDoctrine()->getRepository(Typemateriel::class)->findOneBy(['idtypemateriel'=>$decrypted_id]);
        $form_edit = $this->createForm(TypeMaterielType::class, $type_materiel, ['action' => $this->generateUrl('type_materiel_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Type modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "type_materiel",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_type_materiel_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(TypeMaterielType::class, new Typemateriel(), ['action' => $this->generateUrl('type_materiel_new')])->createView(),
            'type_form' => "type_materiel",
            'types_materiel'=> $typematerielRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/type_materiel/supprimer/{id}", name="type_materiel_delete", methods={"DELETE"})
     */
    public function delete_type_materiel(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $typemateriel = $this->getDoctrine()->getRepository(Typemateriel::class)->findOneBy(['idtypemateriel'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$typemateriel->getIdtypemateriel(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typemateriel);
            $entityManager->flush();
            $this->addFlash('add', 'Type supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "type_materiel",
        ]);
    }


    /**************************************************************************************************************************/
    /***********************************************    CENTRES DE SECOURS    *************************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/centres_secours/ajout", options={"expose"=true}, name="centres_secours_new", methods={"GET","POST"})
     */
    public function centres_secours_new(Request $request, CentresSecoursRepository $centresSecoursRepository) :Response
    {
        $centre_secours = new CentresSecours();
        $form_new = $this->createForm(CentresSecoursType::class, $centre_secours, ['action' => $this->generateUrl('centres_secours_new')]);
        $form_new->handleRequest($request);

        $centres_secours = $centresSecoursRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($centre_secours);
            $entityManager->flush();
            $this->addFlash('add', 'Centre enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "centres_secours",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "centres_secours",
                    'centres_secours'=> $centres_secours
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "centres_secours",
            'centres_secours'=> $centresSecoursRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/centres_secours/modifier/{id}", options={"expose"=true}, name="centres_secours_edit", methods={"GET","POST"})
     */
    public function centres_secours_edit(Encryption $encryption, Request $request, CentresSecoursRepository $centresSecoursRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $centre_secours = $this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(CentresSecoursType::class, $centre_secours, ['action' => $this->generateUrl('centres_secours_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Centre modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "centres_secours",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_centres_secours_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(CentresSecoursType::class, new CentresSecours(), ['action' => $this->generateUrl('centres_secours_new')])->createView(),
            'type_form' => "centres_secours",
            'centres_secours'=> $centresSecoursRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/centres_secours/supprimer/{id}", name="centres_secours_delete", methods={"DELETE"})
     */
    public function delete_centres_secours(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $centreSecours = $this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$centreSecours->getid(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($centreSecours);
            $entityManager->flush();
            $this->addFlash('add', 'Centre supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "centres_secours",
        ]);
    }


    /**************************************************************************************************************************/
    /************************************************    FONCTION UTILISATEUR    **********************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/fonction_utilisateur/ajout", options={"expose"=true}, name="fonction_user_new", methods={"GET","POST"})
     */
    public function fonction_user_new(Request $request, FonctionUserRepository $fonctionUserRepository) :Response
    {
        $fonction_user = new FonctionUser();
        $form_new = $this->createForm(FonctionUserType::class, $fonction_user, ['action' => $this->generateUrl('fonction_user_new')]);
        $form_new->handleRequest($request);

        $fonctions_user = $fonctionUserRepository->getAllOrderByRank();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fonction_user);
            $entityManager->flush();
            $this->addFlash('add', 'Fonction enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "fonction_user",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "fonction_user",
                    'fonctions_user'=> $fonctions_user
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "fonction_user",
            'fonctions_user'=> $fonctionUserRepository->getAllOrderByRank()
        ]);
    }


    /**
     * @Route("/fonction_utilisateur/modifier/{id}", options={"expose"=true}, name="fonction_user_edit", methods={"GET","POST"})
     */
    public function fonction_user_edit(Encryption $encryption, Request $request,FonctionUserRepository $fonctionUserRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $fonction_user = $this->getDoctrine()->getRepository(FonctionUser::class)->findOneBy(['idFonction'=>$decrypted_id]);
        $form_edit = $this->createForm(FonctionUserType::class, $fonction_user, ['action' => $this->generateUrl('fonction_user_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Fonction modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "fonction_user",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_fonction_user_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $this->createForm(FonctionUserType::class, new FonctionUser(), ['action' => $this->generateUrl('fonction_user_new')])->createView(),
            'form_edit' => $form_edit->createView(),
            'type_form' => "fonction_user",
            'fonctions_user'=> $fonctionUserRepository->getAllOrderByRank()
        ]);

    }

    /**
     * @Route("/fonction_utilisateur/supprimer/{id}", name="fonction_user_delete", methods={"DELETE"})
     */
    public function delete_fonction_user(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $fonctionUser = $this->getDoctrine()->getRepository(FonctionUser::class)->findOneBy(['idFonction' => $decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$fonctionUser->getIdFonction(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($fonctionUser);
            $entityManager->flush();
            $this->addFlash('add', 'Fonction supprimée !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "fonction_user",
        ]);
    }


    /**************************************************************************************************************************/
    /************************************************    GRADE UTILISATEUR    *************************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/grade_utilisateur/ajout", options={"expose"=true}, name="grade_user_new", methods={"GET","POST"})
     */
    public function grade_user_new(Request $request, GradeUserRepository $gradeUserRepository) :Response
    {
        $grade_user = new GradeUser();
        $form_new = $this->createForm(GradeUserType::class, $grade_user, ['action' => $this->generateUrl('grade_user_new')]);
        $form_new->handleRequest($request);

        $grades_user = $gradeUserRepository->getAllOrderByRank();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($grade_user);
            $entityManager->flush();
            $this->addFlash('add', 'Grade enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "grade_user",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "grade_user",
                    'grades_user'=> $grades_user
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "grade_user",
            'grades_user'=> $gradeUserRepository->getAllOrderByRank()
        ]);

    }


    /**
     * @Route("/grade_utilisateur/modifier/{id}", options={"expose"=true}, name="grade_user_edit", methods={"GET","POST"})
     */
    public function grade_user_edit(Encryption $encryption, Request $request, GradeUserRepository $gradeUserRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $grade_user = $this->getDoctrine()->getRepository(GradeUser::class)->findOneBy(['idGrade'=>$decrypted_id]);
        $form_edit = $this->createForm(GradeUserType::class, $grade_user, ['action' => $this->generateUrl('grade_user_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Grade modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "grade_user",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_grade_user_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(GradeUserType::class, new GradeUser(), ['action' => $this->generateUrl('grade_user_new')])->createView(),
            'type_form' => "grade_user",
            'grades_user'=> $gradeUserRepository->getAllOrderByRank()
        ]);

    }

    /**
     * @Route("/grade_utilisateur/supprimer/{id}", name="grade_user_delete", methods={"DELETE"})
     */
    public function delete_grade_user(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $gradeUser = $this->getDoctrine()->getRepository(GradeUser::class)->findOneBy(['idGrade'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$gradeUser->getIdGrade(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gradeUser);
            $entityManager->flush();
            $this->addFlash('add', 'Grade supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "grade_user",
        ]);
    }



    /**************************************************************************************************************************/
    /**********************************************    CATÉGORIES RÉPERTOIRE    ***********************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/categories_repertoire/ajout", options={"expose"=true}, name="categories_repertoire_new", methods={"GET","POST"})
     */
    public function categories_repertoire_new(Request $request, CategorierepertoireRepository $categorierepertoireRepository) :Response
    {
        $categorie_repertoire = new Categorierepertoire();
        $form_new = $this->createForm(CategorieRepertoireType::class, $categorie_repertoire, ['action' => $this->generateUrl('categories_repertoire_new')]);
        $form_new->handleRequest($request);

        $categories_repertoire = $categorierepertoireRepository->getAllOrderByRank();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorie_repertoire);
            $entityManager->flush();
            $this->addFlash('add', 'Catégorie enregistrée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "categories_repertoire",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "categories_repertoire",
                    'categories_repertoire'=> $categories_repertoire
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "grade_user",
            'grades_user'=> $categorierepertoireRepository->getAllOrderByRank()
        ]);

    }


    /**
     * @Route("/categories_repertoire/modifier/{id}", options={"expose"=true}, name="categories_repertoire_edit", methods={"GET","POST"})
     */
    public function categories_repertoire_edit(Encryption $encryption, Request $request, CategorierepertoireRepository $categorierepertoireRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $categorie_repertoire = $this->getDoctrine()->getRepository(Categorierepertoire::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(CategorieRepertoireType::class, $categorie_repertoire, ['action' => $this->generateUrl('categories_repertoire_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Catégorie modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "categories_repertoire",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_categories_repertoire_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(CategorieRepertoireType::class, new Categorierepertoire(), ['action' => $this->generateUrl('categories_repertoire_new')])->createView(),
            'type_form' => "categories_repertoire",
            'categories_repertoire'=> $categorierepertoireRepository->getAllOrderByRank()
        ]);

    }

    /**
     * @Route("/categories_repertoire/supprimer/{id}", name="categories_repertoire_delete", methods={"DELETE"})
     */
    public function categories_repertoire_user(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $categorierepertoire = $this->getDoctrine()->getRepository(Categorierepertoire::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$categorierepertoire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorierepertoire);
            $entityManager->flush();
            $this->addFlash('add', 'Catégorie supprimée !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "categories_repertoire",
        ]);
    }


    /**************************************************************************************************************************/
    /***********************************************    NOM MATÉRIEL    **********************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/nom_equipement/ajout", options={"expose"=true}, name="nom_equipement_new", methods={"GET","POST"})
     */
    public function nom_equipement_new(Request $request, NomEquipementOperationnelRepository $nomEquipementOperationnelRepository) :Response
    {
        $nom_equipement = new NomEquipementOperationnel();
        $form_new = $this->createForm(NomEquipementOperationnelType::class, $nom_equipement, ['action' => $this->generateUrl('nom_equipement_new')]);
        $form_new->handleRequest($request);

        $noms_equipement = $nomEquipementOperationnelRepository->findAll();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nom_equipement);
            $entityManager->flush();
            $this->addFlash('add', 'Nom enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "nom_equipement",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "nom_equipement",
                    'noms_equipement'=> $noms_equipement
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "nom_equipement",
            'noms_equipement'=> $noms_equipement
        ]);
    }


    /**
     * @Route("/nom_equipement/modifier/{id}", options={"expose"=true}, name="nom_equipement_edit", methods={"GET","POST"})
     */
    public function nom_equipement_edit(Encryption $encryption, Request $request, NomEquipementOperationnelRepository $nomEquipementOperationnelRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $nom_equipement = $this->getDoctrine()->getRepository(NomEquipementOperationnel::class)->findOneBy(['idNomEquipement'=>$decrypted_id]);
        $form_edit = $this->createForm(NomEquipementOperationnelType::class, $nom_equipement, ['action' => $this->generateUrl('nom_equipement_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Nom modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "nom_equipement",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_nom_equipement_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' =>$this->createForm(NomEquipementOperationnelType::class, new NomEquipementOperationnel(), ['action' => $this->generateUrl('nom_equipement_new')])->createView(),
            'type_form' => "nom_equipement",
            'noms_equipement'=> $nomEquipementOperationnelRepository->findAll()
        ]);
    }

    /**
     * @Route("/nom_equipement/supprimer/{id}", name="nom_equipement_delete", methods={"DELETE"})
     */
    public function nom_equipement_materiel(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $nomEquipementOperationnel = $this->getDoctrine()->getRepository(NomEquipementOperationnel::class)->findOneBy(['idNomEquipement'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$nomEquipementOperationnel->getIdNomEquipement(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($nomEquipementOperationnel);
            $entityManager->flush();
            $this->addFlash('add', 'Nom supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "nom_equipement",
        ]);
    }


    /**************************************************************************************************************************/
    /************************************************    Lieux Interventions    ***********************************************/
    /**************************************************************************************************************************/


    /**
     * @Route("/lieux_interventions/ajout", options={"expose"=true}, name="lieux_interventions_new", methods={"GET","POST"})
     */
    public function lieux_interventions_new(Request $request) :Response
    {
        $lieu_intervention = new LieuxInterventions();
        $form_new = $this->createForm(LieuxInterventionsType::class, $lieu_intervention, ['action' => $this->generateUrl('lieux_interventions_new')]);
        $form_new->handleRequest($request);

        $lieux_interventions = $this->getDoctrine()->getRepository(LieuxInterventions::class)->findAll();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lieu_intervention);
            $entityManager->flush();
            $this->addFlash('add', 'Lieu enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "lieux_interventions",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "lieux_interventions",
                    'lieux_interventions'=> $lieux_interventions
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "lieux_interventions",
            'lieux_interventions'=> $lieux_interventions
        ]);
    }


    /**
     * @Route("/lieux_interventions/modifier/{id}", options={"expose"=true}, name="lieux_interventions_edit", methods={"GET","POST"})
     */
    public function lieux_interventions_edit(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $lieu_intervention = $this->getDoctrine()->getRepository(LieuxInterventions::class)->findOneBy(['idLieuxInterventions'=>$decrypted_id]);
        $form_edit = $this->createForm(LieuxInterventionsType::class, $lieu_intervention, ['action' => $this->generateUrl('lieux_interventions_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Lieu modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "lieux_interventions",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_lieux_interventions_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' =>$this->createForm(LieuxInterventionsType::class, new LieuxInterventions(), ['action' => $this->generateUrl('lieux_interventions_new')])->createView(),
            'type_form' => "lieux_interventions",
            'lieux_interventions'=> $this->getDoctrine()->getRepository(LieuxInterventions::class)->findAll()
        ]);
    }

    /**
     * @Route("/lieux_interventions/supprimer/{id}", name="lieux_interventions_delete", methods={"DELETE"})
     */
    public function lieux_interventions_materiel(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $lieu_intervention = $this->getDoctrine()->getRepository(LieuxInterventions::class)->findOneBy(['idLieuxInterventions'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$lieu_intervention->getIdLieuxInterventions(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lieu_intervention);
            $entityManager->flush();
            $this->addFlash('add', 'Lieu supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "lieux_interventions",
        ]);
    }


    /**************************************************************************************************************************/
    /****************************************************   Technique   *******************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/technique/ajout", options={"expose"=true}, name="technique_new", methods={"GET","POST"})
     */
    public function technique_new(Request $request, TechniqueRepository $techniqueRepository) :Response
    {
        $technique = new Technique();
        $form_new = $this->createForm(TechniqueType::class, $technique, ['action' => $this->generateUrl('technique_new')]);
        $form_new->handleRequest($request);

        $techniques = $techniqueRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($technique);
            $entityManager->flush();
            $this->addFlash('add', 'Technique enregistrée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "technique",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "technique",
                    'techniques'=> $techniques
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "technique",
            'techniques'=> $techniqueRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/technique/modifier/{id}", options={"expose"=true}, name="technique_edit", methods={"GET","POST"})
     */
    public function technique_edit(Encryption $encryption, Request $request, TechniqueRepository $techniqueRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $techniques = $this->getDoctrine()->getRepository(Technique::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(TechniqueType::class, $techniques, ['action' => $this->generateUrl('technique_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Technique modifiée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "technique",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_technique_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(TechniqueType::class, new Technique(), ['action' => $this->generateUrl('technique_new')])->createView(),
            'type_form' => "technique",
            'techniques'=> $techniqueRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/technique/supprimer/{id}", name="technique_delete", methods={"DELETE"})
     */
    public function delete_technique(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $technique = $this->getDoctrine()->getRepository(Technique::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$technique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($technique);
            $entityManager->flush();
            $this->addFlash('add', 'Tehchnique supprimée !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "technique",
        ]);
    }

    /**************************************************************************************************************************/
    /***************************************************   Statut FMPA   ******************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/statut-fmpa/ajout", options={"expose"=true}, name="statut_fmpa_new", methods={"GET","POST"})
     */
    public function statut_fmpa_new(Request $request, StatutFMPARepository $statut_fmpaRepository) :Response
    {
        $statut_fmpa = new StatutFMPA();
        $form_new = $this->createForm(StatutFMPAType::class, $statut_fmpa, ['action' => $this->generateUrl('statut_fmpa_new')]);
        $form_new->handleRequest($request);

        $statut_fmpas = $statut_fmpaRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($statut_fmpa);
            $entityManager->flush();
            $this->addFlash('add', 'Statut enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "statut_fmpa",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "statut_fmpa",
                    'statut_fmpas'=> $statut_fmpas
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "statut_fmpa",
            'statut_fmpas'=> $statut_fmpaRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/statut-fmpa/modifier/{id}", options={"expose"=true}, name="statut_fmpa_edit", methods={"GET","POST"})
     */
    public function statut_fmpa_edit(Encryption $encryption, Request $request, StatutFMPARepository $statut_fmpaRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $statut_fmpas = $this->getDoctrine()->getRepository(StatutFMPA::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(StatutFMPAType::class, $statut_fmpas, ['action' => $this->generateUrl('statut_fmpa_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Statut modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "statut_fmpa",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_statut_fmpa_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(StatutFMPAType::class, new StatutFMPA(), ['action' => $this->generateUrl('statut_fmpa_new')])->createView(),
            'type_form' => "statut_fmpa",
            'statut_fmpas'=> $statut_fmpaRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/statut-fmpa/supprimer/{id}", name="statut_fmpa_delete", methods={"DELETE"})
     */
    public function delete_statut_fmpa(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $statut_fmpa= $this->getDoctrine()->getRepository(StatutFMPA::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$statut_fmpa->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($statut_fmpa);
            $entityManager->flush();
            $this->addFlash('add', 'Statut supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "statut_fmpa",
        ]);
    }

    /**************************************************************************************************************************/
    /*****************************************************   Filière   ********************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/filiere/ajout", options={"expose"=true}, name="filiere_new", methods={"GET","POST"})
     */
    public function filiere_new(Request $request, FiliereRepository $filiereRepository) :Response
    {
        $filiere = new Filiere();
        $form_new = $this->createForm(FiliereType::class, $filiere, ['action' => $this->generateUrl('filiere_new')]);
        $form_new->handleRequest($request);

        $filieres = $filiereRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($filiere);
            $entityManager->flush();
            $this->addFlash('add', 'Filière enregistrée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "filiere",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "filiere",
                    'filieres'=> $filieres
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "filiere",
            'filieres'=> $filiereRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/filiere/modifier/{id}", options={"expose"=true}, name="filiere_edit", methods={"GET","POST"})
     */
    public function filiere_edit(Encryption $encryption, Request $request, FiliereRepository $filiereRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $filieres = $this->getDoctrine()->getRepository(Filiere::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(FiliereType::class, $filieres, ['action' => $this->generateUrl('filiere_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Filière modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "filiere",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_filiere_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(FiliereType::class, new Filiere(), ['action' => $this->generateUrl('filiere_new')])->createView(),
            'type_form' => "filiere",
            'filieres'=> $filiereRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/filiere/supprimer/{id}", name="filiere_delete", methods={"DELETE"})
     */
    public function delete_filiere(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $filiere = $this->getDoctrine()->getRepository(Filiere::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$filiere->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($filiere);
            $entityManager->flush();
            $this->addFlash('add', 'Filière supprimée !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "filiere",
        ]);
    }

    /**************************************************************************************************************************/
    /*******************************************************   CTD   **********************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/CTD/ajout", options={"expose"=true}, name="CTD_new", methods={"GET","POST"})
     */
    public function CTD_new(Request $request, CTDRepository $CTDRepository) :Response
    {
        $CTD= new CTD();
        $form_new = $this->createForm(CTDType::class, $CTD, ['action' => $this->generateUrl('CTD_new')]);
        $form_new->handleRequest($request);

        $CTDs = $CTDRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($CTD);
            $entityManager->flush();
            $this->addFlash('add', 'Filière enregistrée !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "CTD",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "CTD",
                    'CTDs'=> $CTDs
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "CTD",
            'CTDs'=> $CTDRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/CTD/modifier/{id}", options={"expose"=true}, name="CTD_edit", methods={"GET","POST"})
     */
    public function CTD_edit(Encryption $encryption, Request $request, CTDRepository $CTDRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $CTDs = $this->getDoctrine()->getRepository(CTD::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(CTDType::class, $CTDs, ['action' => $this->generateUrl('CTD_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'CTD modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "CTD",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_CTD_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(CTDType::class, new CTD(), ['action' => $this->generateUrl('CTD_new')])->createView(),
            'type_form' => "CTD",
            'CTDs'=> $CTDRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/CTD/supprimer/{id}", name="CTD_delete", methods={"DELETE"})
     */
    public function delete_CTD(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $CTD = $this->getDoctrine()->getRepository(CTD::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$CTD->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($CTD);
            $entityManager->flush();
            $this->addFlash('add', 'CTD supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "CTD",
        ]);
    }

    /**************************************************************************************************************************/
    /**************************************************   Type Convention  ****************************************************/
    /**************************************************************************************************************************/

    /**
     * @Route("/type-convention/ajout", options={"expose"=true}, name="type_convention_new", methods={"GET","POST"})
     */
    public function type_convention_new(Request $request, TypeConventionRepository $type_conventionRepository) :Response
    {
        $type_convention= new TypeConvention();
        $form_new = $this->createForm(TypeConventionType::class, $type_convention, ['action' => $this->generateUrl('type_convention_new')]);
        $form_new->handleRequest($request);

        $type_conventions = $type_conventionRepository->getAllOrderByName();

        if ($form_new->isSubmitted() && $form_new->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($type_convention);
            $entityManager->flush();
            $this->addFlash('add', 'Type de convention enregistré !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "type_convention",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_content.html.twig', [
                    'form_new' => $form_new->createView(),
                    'type_form' => "type_convention",
                    'type_conventions'=> $type_conventions
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_new' => $form_new->createView(),
            'type_form' => "type_convention",
            'type_conventions'=> $type_conventionRepository->getAllOrderByName()
        ]);

    }


    /**
     * @Route("/type-convention/modifier/{id}", options={"expose"=true}, name="type_convention_edit", methods={"GET","POST"})
     */
    public function type_convention_edit(Encryption $encryption, Request $request, TypeConventionRepository $type_conventionRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $type_conventions = $this->getDoctrine()->getRepository(TypeConvention::class)->findOneBy(['id'=>$decrypted_id]);
        $form_edit = $this->createForm(TypeConventionType::class, $type_conventions, ['action' => $this->generateUrl('type_convention_edit',['id'=>$id])]);
        $form_edit->handleRequest($request);

        if ($form_edit->isSubmitted() && $form_edit->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('add', 'Type convention modifié !');

            return $this->redirectToRoute('admin_index', [
                'type_form' => "type_convention",
            ]);
        }

        if($request->get('ajax')){
            return new JsonResponse([
                'content'=> $this->renderView('admin/_form_type_convention_edit.html.twig', [
                    'form_edit' => $form_edit->createView(),
                ])
            ]);
        }

        return $this->render('admin/index.html.twig', [
            'form_edit' => $form_edit->createView(),
            'form_new' => $this->createForm(TypeConventionType::class, new TypeConvention(), ['action' => $this->generateUrl('type_convention_new')])->createView(),
            'type_form' => "type_convention",
            'type_conventions'=> $type_conventionRepository->getAllOrderByName()
        ]);

    }

    /**
     * @Route("/type-convention/supprimer/{id}", name="type_convention_delete", methods={"DELETE"})
     */
    public function delete_type_convention(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $type_convention= $this->getDoctrine()->getRepository(TypeConvention::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$type_convention->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($type_convention);
            $entityManager->flush();
            $this->addFlash('add', 'Type de convention supprimé !');
        }

        return $this->redirectToRoute('admin_index', [
            'type_form' => "type_convention",
        ]);
    }
}
