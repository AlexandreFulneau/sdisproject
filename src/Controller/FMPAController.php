<?php

namespace App\Controller;

use App\Entity\FicheIndividuelleFMPA;
use App\Entity\FMPA;
use App\Form\FMPAType;
use App\Repository\FicheIndividuelleFMPARepository;
use App\Services\Encryption;
use App\Repository\FMPARepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Route("/fmpa/liste")
 * @isGranted("ROLE_EQUIPIER")
 */
class FMPAController extends AbstractController
{
    /**
     * @Route("/", name="fmpa_index", methods={"GET"})
     */
    public function index(FMPARepository $fmpaRepository, FicheIndividuelleFMPARepository $ficheIndividuelleFMPARepository): Response
    {
        return $this->render('fmpa/index.html.twig', [
            'fmpas' => $fmpaRepository->findAll(),
            'fiches_individuelles_fmpas' => $ficheIndividuelleFMPARepository->findBy(['personnel'=> $this->getUser()])
        ]);
    }

    /**
     * @Route("/ajouter", name="fmpa_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $fmpa = new FMPA();
        $form = $this->createForm(FMPAType::class, $fmpa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($fmpa);
            $entityManager->flush();
            $this->addFlash('add', 'FMPA ajoutée !');

            return $this->redirectToRoute('fmpa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fmpa/new.html.twig', [
            'fmpa' => $fmpa,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fmpa_show", methods={"GET"})
     */
    public function show(Encryption $encryption, FMPARepository $fmpaRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $fmpa = $fmpaRepository->findOneById($decrypted_id);
      
        return $this->render('fmpa/show.html.twig', [
            'fmpa' => $fmpa,
        ]);
    }

    /**
     * @Route("/{id}/modifier", name="fmpa_edit", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $fmpa = $this->getDoctrine()->getRepository(FMPA::class)->findOneBy(['id' => $decrypted_id]);

        $form = $this->createForm(FMPAType::class, $fmpa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('update', 'FMPA modifiée !');

            return $this->redirectToRoute('fmpa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fmpa/edit.html.twig', [
            'fmpa' => $fmpa,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fmpa_delete", methods={"POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $fmpa = $this->getDoctrine()->getRepository(FMPA::class)->findOneBy(['id' => $decrypted_id]);

        if ($this->isCsrfTokenValid('delete' . $fmpa->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($fmpa);
            $entityManager->flush();
            $this->addFlash('delete', 'FMPA supprimée !');
        }

        return $this->redirectToRoute('fmpa_index', [], Response::HTTP_SEE_OTHER);
    }
}
