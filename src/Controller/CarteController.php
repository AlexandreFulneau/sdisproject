<?php

namespace App\Controller;

use App\Entity\CentresSecours;
use App\Repository\CentresSecoursRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CarteController extends AbstractController
{
    /**
     * @Route("/carte", name="carte_index")
     * @isGranted("ROLE_EQUIPIER")
     */
    public function index(HttpClientInterface $httpClient, UserRepository $userRepository, CentresSecoursRepository $centresSecoursRepository): Response
    {


        $centresSecours = $centresSecoursRepository->findAllAdresses();
        $users = $userRepository->findAllAdresses();
        
        foreach($centresSecours as $key => $centreSecours){

            $response = $httpClient->request(
                'GET',
                'https://nominatim.openstreetmap.org/search?q=' . urlencode($centreSecours->getAdresse()) . '&format=json&addressdetails=1&limit=1&polygon_svg=1'
            );
    
            $content = $response->getContent();
            $obj = json_decode($content, true);
    
            if($obj)
            {
                $latitude = $obj[0]['lat'];
                $longitude = $obj[0]['lon'];
    
                $centresSecoursArray[$key] = ['nom' => $centreSecours->getNom(), 
                                                'type' => $centreSecours->getType(), 
                                                'lat' => $latitude, 
                                                'lon' => $longitude];
            }
           
        }

        foreach($users as $key => $user){

            $response = $httpClient->request(
                'GET',
                'https://nominatim.openstreetmap.org/search?q=' . urlencode($user->getAdresseDomicile()) . '&format=json&addressdetails=1&limit=1&polygon_svg=1'
            );
    
            $content = $response->getContent();
            $obj = json_decode($content, true);
    
            if($obj)
            {
                $latitude = $obj[0]['lat'];
                $longitude = $obj[0]['lon'];
    
                $usersArray[$key] = ['nom' => $user->getNom().' '.$user->getPrenom(), 
                                                'lat' => $latitude, 
                                                'lon' => $longitude];
            }
           
        }

    
        return $this->render('carte/index.html.twig', [
            'centresSecours' => $centresSecoursArray,
            'users' => $usersArray,
        ]);
    }
}
