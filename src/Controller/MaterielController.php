<?php

namespace App\Controller;

use App\Entity\Centres;
use App\Entity\CentresSecours;
use App\Entity\EquipementOperationnel;
use App\Entity\Materiels;
use App\Entity\Typemateriel;
use App\Entity\VerificationEquipementOperationnel;
use App\Form\MaterielsType;
use App\Form\EquipementOperationnelType;
use App\Form\VerificationEquipementOperationnelType;
use App\Repository\CentresRepository;
use App\Repository\MaterielRepository;
use App\Repository\EquipementOperationnelRepository;
use App\Repository\VerificationEquipementOperationnelRepository;
use App\Services\Encryption;
use App\Services\TranslatePeriodicityToEnglish;
use Dompdf\Dompdf;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InterventionsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Commandes;
use App\Entity\Commander;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Date;

class MaterielController extends AbstractController
{
    /**
     * @Route("/stockage", name="materiel_index")
     * @IsGranted("ROLE_ADMIN")
     * @param EquipementOperationnelRepository $equipementOperationnelRepository
     * @param MaterielRepository $materielRepository
     * @param CentresRepository $centresRepository
     * @param Request $request
     * @param TranslatePeriodicityToEnglish $periodicityToEnglish
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function indexStock(EquipementOperationnelRepository $equipementOperationnelRepository, MaterielRepository $materielRepository, CentresRepository $centresRepository, Request $request, TranslatePeriodicityToEnglish $periodicityToEnglish, EntityManagerInterface $em): Response
    {

        $equipmentsVerifications = array();
        $todoVerifications = array();

        $material = new Materiels();
        $equipment = new EquipementOperationnel();

        $filter = $request->get('filter');

        if ($filter) {
            $materialsList = $materielRepository->getAll($filter);
            $equipmentsList = $equipementOperationnelRepository->getAll($filter);
            $equipmentsInfos = $equipementOperationnelRepository->getStockByEquipment($filter);
        }
        else{
            $materialsList = $materielRepository->getAll();
            $equipmentsList = $equipementOperationnelRepository->getAll();
            $equipmentsInfos = $equipementOperationnelRepository->getStockByEquipment();
        }

        $centre = $centresRepository->findAll()[0];
        $materialsTypes = $em->getRepository('App\Entity\Typemateriel')->findAll();

        $materialForm = $this->createForm(MaterielsType::class, $material);
        $equipmentForm = $this->createForm(EquipementOperationnelType::class, $equipment, ['request' => $request]);

        $materialForm->handleRequest($request);
        $equipmentForm->handleRequest($request);

        foreach ($equipmentsList as $key => $value){ // On trie les données de la requête sql (tableau avec : nom,count,type et tableau avec les vérifications)

            $verifications[] = $value->getVerifications()->toArray();

            foreach ($verifications[$key] as $verification){
                $equipmentsVerifications[] = $verification;
            }
        }

        foreach ($equipmentsVerifications as $key => $value){ // On regarde si des vérifications d'équipements sont à faire

            if( date('ymd') > $value->getNomEquipement()->getDateProchaineVerification()->format('ymd')){
                $todoVerifications[] = ['nom' => $value->getNomEquipement()->getNom()->getNom(), 'verification' => false ];
            }
        }

        $todoVerifications = array_map('unserialize', array_unique(array_map('serialize', $todoVerifications))); // On enleve les doublon

        if ($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('materiel/_content.html.twig', [
                    'materials' => $materialsList,
                    'equipments' => $equipmentsInfos,
                    'todoVerifications' => $todoVerifications,
                    'types' => $materialsTypes

                ])
            ]);
        }

        if ($equipmentForm->isSubmitted() && $equipmentForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            if (isset($request->get('equipement_operationnel')['minPression']))
                $equipment->setMinPression($request->get('equipement_operationnel')['minPression']);

            if (isset($request->get('equipement_operationnel')['pression']))
                $equipment->setPression($request->get('equipement_operationnel')['pression']);

            if (isset($request->get('equipement_operationnel')['centresSecours']))
                $equipment->setCentresSecours($this->getDoctrine()->getRepository(CentresSecours::class)->findOneBy(['id' => $request->get('equipement_operationnel')['centresSecours']]));

            if (isset($request->get('equipement_operationnel')['qrCode']))
                $equipment->setQrCode($request->get('equipement_operationnel')['qrCode']);

            $date_next_verification = date_create_from_format('d/m/y',$equipment->getDateCreation()->format('d/m/y'));
            $date_next_verification->modify('+ '.$periodicityToEnglish->translate($equipment->getPeriodicite()));

            $equipment->setDateProchaineVerification($date_next_verification);

            $entityManager->persist($equipment);
            $entityManager->flush();

            $equipmentVerification = new VerificationEquipementOperationnel();
            $equipmentVerification->setNomEquipement($equipment)
                ->setDate($equipment->getDateCreation())
                ->setVerificateur($this->getUser())
                ->setCommentaire('Initialisation des vérifications : Ajout du matériel');

            $entityManager->persist($equipmentVerification);
            $entityManager->flush();

            $this->addFlash('add', 'Matériel enregistré !');

            return $this->redirectToRoute('materiel_index');
        }

        if ($materialForm->isSubmitted() && $materialForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($material);
            $entityManager->flush();
            $this->addFlash('add', 'Matériel enregistré !');

            return $this->redirectToRoute('materiel_index');
        }

        if ($request->get('equipement_operationnel') && isset($request->get('equipement_operationnel')['_token']) == false) {
            return new JsonResponse(['content' => $this->renderView('materiel/_form_equipment.html.twig', [
                'equipmentForm' => $equipmentForm->createView()
            ])
            ]);
        }

        return $this->render('materiel/show_stock.html.twig', [
            'materials' => $materialsList,
            'equipments' => $equipmentsInfos,
            'todoVerifications' => $todoVerifications,
            'centre' => $centre,
            'types' => $materialsTypes,
            'materialForm' => $materialForm->createView(),
            'equipmentForm' => $equipmentForm->createView(),
            'filter' => $filter

        ]);
    }


    /**
     * @Route ("/stockage/modification_stock/{id}/{stock}", options={"expose"=true}, name="materiel_modifStock")
     * @param Encryption $encryption
     * @param MaterielRepository $materielRepository
     * @param $id
     * @param $stock
     * @return Response
     */
    public function updateStock(Encryption $encryption, MaterielRepository $materielRepository, $id, $stock)
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $materiel = $materielRepository->findOneById($decrypted_id);

        // Toujours check côté serveur

        if ($materiel != null) {
            $manager = $this->getDoctrine()->getManager();

            $materiel->setStock($stock);
            $manager->flush();
        }

        return new Response();
    }

    /**
     * @Route ("/stockage/modification_materiel/{id}/{type}", options={"expose"=true}, name="materiel_modifType")
     * @param MaterielRepository $materielRepository
     * @param $id
     * @param $type
     * @return Response
     */
    public function updateType(Encryption $encryption, MaterielRepository $materielRepository, $id, $type) : Response
    {
        $decrypted_id = $encryption->decryptionValue($id);
        $decrypted_type = $encryption->decryptionValue($type);
        if ($decrypted_id == '0' || $decrypted_type == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $materiel = $materielRepository->findOneById($decrypted_id);

        // Toujours check côté serveur
        if ($materiel != null) {
            $em = $this->getDoctrine()->getManager();

            $materialType = $em->getRepository('App\Entity\Typemateriel')->findOneBy(['idtypemateriel' => $decrypted_type]);

            if ($materialType)
                $materiel->setType($materialType);

            $em->flush();
        }

        return new Response();
    }

    /**
     * @Route ("/stockage/modification_centre", name="materiel_modifCentres")
     * @param CentresRepository $centresRepository
     * @param Request $request
     * @return Response
     */
    public function updateCenter(CentresRepository $centresRepository, Request $request) : Response
    {

        if ($request->request->get('centreemail') != null && filter_var($request->request->get('centreemail'), FILTER_VALIDATE_EMAIL)) {
            $centres = $centresRepository->findAll();
            $email = $request->request->get('centreemail');

            $manager = $this->getDoctrine()->getManager();

            for ($i = 0; $i < sizeof($centres); $i++) {
                $centre = $centres[$i];

                if ($centre != null) {
                    $centre->setEmail($email);
                } else {
                    throw new NotFoundHttpException("Ce centre n'existe pas !");
                }
            }

            $manager->flush();

            $this->addFlash('add', 'Email de la pharmacie modifiée !');
            return $this->redirectToRoute('materiel_index');
        }

        throw new NotFoundHttpException("L'email du centre n'est pas correcte.");
    }


    /**
     * @Route("/stockage/récapitulatif/{filter}", options={"expose"=true}, defaults={"filter"= "none"}, name="downloadrecap")
     * @IsGranted("ROLE_ADMIN")
     * @param MaterielRepository $materielRepository
     * @param $filter
     */
    public function showPDF(Encryption $encryption, EquipementOperationnelRepository $equipementOperationnelRepository, MaterielRepository $materielRepository, $filter)
    {
        $filter = urldecode($filter);

        if ($this->checkFilterIsTypeMat($filter)) {
            $materials = $materielRepository->getAll($filter);
            $equipments = $equipementOperationnelRepository->getStockByEquipment($filter);
            // On génère l'HTML et on créé le PDF avec.
            $html = $this->renderView('materiel/stock_pdf.html.twig', ['materials' => $materials, 'equipments' => $equipments]);

            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4');
            $dompdf->render();
            $dompdf->stream("Récapitulatif_stock_". (new \DateTime())->format('d_m_Y') .".pdf", ['Attachment' => 0]);
        }
        exit();
    }

    /**
     * @Route("/stockage/supprimer/{id}", name="materiel_delete")
     * @IsGranted("ROLE_ADMIN")
     * @param MaterielRepository $materielRepository
     * @param EntityManagerInterface $manager
     * @param $id
     * @return Response
     */
    public function delete(Encryption $encryption, MaterielRepository $materielRepository, EntityManagerInterface $manager, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $materiel = $materielRepository->findOneById($decrypted_id);

        if ($this->isCsrfTokenValid('delete'.$materiel->getid(), $request->request->get('_token'))) {
            $manager->remove($materiel);
            $manager->flush();
            $this->addFlash('supp', 'Matériel supprimé !');
            return $this->redirectToRoute('materiel_index');
        }

        $this->addFlash('supp', "Ce matériel n'existe pas");
        return $this->redirectToRoute('materiel_index');

    }


    /**
     *
     * @Route("/equipements/{name}", options={"expose"=true}, name="showEquipments")
     * @IsGranted("ROLE_ADMIN")
     * @param EquipementOperationnelRepository $equipementOperationnelRepository
     * @param $name
     * @return RedirectResponse|Response
     */
    public function showEquipments(EquipementOperationnelRepository $equipementOperationnelRepository, $name){

        $equipments = $equipementOperationnelRepository->getMaterialsByName($name);

        if ($equipments){

            return $this->render('materiel/show_equipments.html.twig', [ 'equipments' => $equipments]);

        }

        return $this->redirectToRoute('materiel_index');

    }


    /**
     * @Route("/equipements/supprimer/{id}", name="equipment_delete", methods={"DELETE"})
     * @isGranted("ROLE_ADMIN")
     * @param Request $request
     * @param EquipementOperationnel $equipementOperationnel
     * @return Response
     */
    public function deleteEquipment(Encryption $encryption, Request $request, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $equipementOperationnel = $this->getDoctrine()->getRepository(EquipementOperationnel::class)->findOneBy(['id'=>$decrypted_id]);

        if ($this->isCsrfTokenValid('delete'.$equipementOperationnel->getid(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($equipementOperationnel);
            $entityManager->flush();
            $this->addFlash('add', 'Equipement supprimé !');
        }

        return $this->redirectToRoute('materiel_index');
    }


    /**
     *
     * @Route("/equipements/verification/{id}", options={"expose"=true}, name="addVerification")
     * @param Request $request
     * @param Encryption $encryption
     * @param TranslatePeriodicityToEnglish $periodicityToEnglish
     * @param EquipementOperationnelRepository $equipementOperationnelRepository
     * @param $id
     * @return RedirectResponse|Response
     */
    public function addVerification(Request $request, Encryption $encryption, TranslatePeriodicityToEnglish $periodicityToEnglish, EquipementOperationnelRepository $equipementOperationnelRepository, $id) :Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $equipment = $this->getDoctrine()->getRepository(EquipementOperationnel::class)->findOneBy(['id' => $decrypted_id]);
        $equipments = $equipementOperationnelRepository->getMaterialsByName($equipment->getNom()->getNom());
        $verification = new VerificationEquipementOperationnel();
        $verification->setNomEquipement($equipment);

        $form = $this->createForm(VerificationEquipementOperationnelType::class, $verification, ['action' => $this->generateUrl('addVerification', ['id' => $id])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($verification);

            $date_next_verification = date_create_from_format('d/m/y',$verification->getDate()->format('d/m/y'));
            $date_next_verification->modify('+ '.$periodicityToEnglish->translate($equipment->getPeriodicite()));

            $equipment->setDateProchaineVerification($date_next_verification);

            $em->flush();

            $this->addFlash('add', 'Vérification ajoutée');

            return $this->redirectToRoute('showEquipments', ['name' => $equipment->getNom()->getNom(), 'equipments' => $equipments]);
        }

        if($request->get('ajax')) {
            return new JsonResponse($this->renderView('materiel/_form_verification.html.twig', ['form' => $form->createView()]));
        }

        return $this->render('materiel/show_equipments.html.twig', [ 'equipments' => $equipments, 'form' => $form->createView()]);
    }

    /**
     *
     * @Route("/equipements/archive_verifications/{id}", options={"expose"=true}, name="showVerifications")
     * @param Encryption $encryption
     * @param VerificationEquipementOperationnelRepository $verificationEquipementOperationnelRepository
     * @param $id
     * @return RedirectResponse|Response
     */
    public function showVerifications(Encryption $encryption, VerificationEquipementOperationnelRepository $verificationEquipementOperationnelRepository, $id) :Response
    {

        $decrypted_id = $encryption->decryptionValue($id);
        $equipment_name = $equipment_num = '';

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $verifications = $verificationEquipementOperationnelRepository->getMaterialVerifications($decrypted_id);

        if ($verifications) {
            $equipment_name = $verifications[0]->getNomEquipement()->getNom();
            $equipment_num = $verifications[0]->getNomEquipement()->getNumero();
        }
        return new JsonResponse($this->renderView('materiel/_show_verifications.html.twig',[
            'verifications' => $verifications,
            'equipment_name' => $equipment_name,
            'equipment_num' => $equipment_num
        ]));
    }

    /**
     * @Route("/matériels/commande/{idinter}", name="materiel")
     * @param MailerInterface $mailer
     * @param CentresRepository $centresRepository
     * @param EquipementOperationnelRepository $equipementOperationnelRepository
     * @param MaterielRepository $materielRepository
     * @param UserRepository $User
     * @param InterventionsRepository $Intervention
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $idinter
     * @return Response
     */
    public function materialOrder(Encryption $encryption, MailerInterface $mailer, CentresRepository $centresRepository, EquipementOperationnelRepository $equipementOperationnelRepository, MaterielRepository $materielRepository, UserRepository $User, InterventionsRepository $Intervention, Request $request, EntityManagerInterface $em, $idinter): Response
    {
        $decrypted_id = $encryption->decryptionValue($idinter);
        $equipment_name = $equipment_num = '';

        if ($decrypted_id == '0'){
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $inter = $Intervention->find($decrypted_id);

        if ($inter != null) {
            // On récupère tout le matériel pour le select
            $materials = $materielRepository->getAll();
            $equipments = $equipementOperationnelRepository->getStockByEquipment();
            $centers = $this->getDoctrine()->getRepository(Centres::class)->findAll();
            $materialTypes = $this->getDoctrine()->getRepository(Typemateriel::class)->findAll();

            // On récupère les données du formulaire
            $centerName = $request->request->get('centre');
            $orderedMaterials = $request->request->get('materials');
            $quantities = $request->request->get('quantities');
            $justifications = $request->request->get('justifications');

            // Toujours check côté serveur
            if ($request->request->count() > 0 && $this->orderValidate($orderedMaterials,$quantities,$centerName)) {
              // On créé une commande
                $centre = $centresRepository->findOneBy(['idcentre' => $centerName]);

                $order = new Commandes();
                $order->setDatecommande(new \DateTime())
                    ->setDemandeur($this->getUser())
                    ->setCentre($centre);

                $em->persist($order);


                $orderedMaterialsValidate = array();

                // On insert une ligne dans la table Commander pour chaque matériel de la commande
                foreach ($orderedMaterials as $key => $orderedMaterial) {

                    $commander = new Commander();

                    $materialSplitValue = explode("_", $orderedMaterial);
                    $materialType = $materialSplitValue[0];
                    $materialID = $materialSplitValue[1];
                    $stockEquipment = null;

                    if ($materialType == "equipment") {
                        $material = $equipementOperationnelRepository->findOneBy(['id' => $materialID]);
                        $stockEquipment = $equipementOperationnelRepository->getCountOfEquipment($materialID);
                        $commander->setIdEquipement($material);
                    }
                    else {
                        $material = $materielRepository->findOneBy(['id' => $materialID]);
                        $commander->setIdmateriel($material);

                    }

                    $commander->setIdcommande($order)
                        ->setIdInter($inter)
                        ->setQuantite(intval($quantities[$key]))
                        ->setJustification($justifications[$key]);


                    if ($material instanceof EquipementOperationnel && $material->getPression() != null && (($material->getPression() - $quantities[$key]) > $material->getMinPression())) {
                        $material->setPression($material->getPression() - $quantities[$key]);
                        $em->persist($material);
                    }
                    elseif ($material instanceof EquipementOperationnel && $material->getPression() != null && (($material->getPression() - $quantities[$key]) < $material->getMinPression())){
                        array_push($orderedMaterialsValidate, ['object' => $material, 'nbOrdered' => '1', 'justification' => $justifications[$key], 'stock' => $stockEquipment]);
                    }
                    else {
                        $em->persist($commander);
                        array_push($orderedMaterialsValidate, ['object' => $material, 'nbOrdered' => $quantities[$key], 'justification' => $justifications[$key], 'stock' => $material->getStock()]);
                    }

                }

                $em->flush();

                // On génère l'HTML et on créé le PDF avec.
                $html = $this->renderView('materiel/order_pdf.html.twig', [
                    'materialTypes' => $materialTypes,
                    'equipments' => $equipments,
                    'materials' => $materials,
                    'orderedMaterials' => $orderedMaterialsValidate,
                    'centre' => $centre,
                    'numInter' => $inter->getNumIntervention()
                ]);

                $dompdf = new Dompdf();
                $dompdf->loadHtml($html);
                $dompdf->setPaper('A4');

                // Parameters
                $x = 520;
                $y = 815;
                $text = "{PAGE_NUM} sur {PAGE_COUNT}";
                $font = $dompdf->getFontMetrics()->get_font('Helvetica', 'normal');
                $size = 10;
                $color = array(0, 0, 0);
                $word_space = 0.0;
                $char_space = 0.0;
                $angle = 0.0;

                $dompdf->getCanvas()->page_text(
                    $x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle
                );

                $dompdf->render();

               $message = (new Email())
                    ->subject('Bon de commande SDIS86 secours animalier')
                    ->from('noreply.serversdis86@gmail.com')
                    // On envoi le mail au créateur de la demande, à l'admin, au centre et à la pharmacie(tous les centres ont pour email celui de la pharmacie)
                    ->to("lightshadow29@outlook.fr")
                    // On joint le PDF au mail
                    ->attach($dompdf->output(),'Commande_MITOS@N_SDIS86', 'Application/pdf')
                    ->text('Une commande a été passée au centre de ' . $centre->getNom() . '.' . PHP_EOL . 'Vous trouverez ci-joint le bon de commande au format PDF.');
                //$mailer->send($message);
                
                $this->addFlash('add', 'Email envoyé !');
                return $this->redirectToRoute('cr_maj', ['id' => $idinter]);
            }

            return $this->render('materiel/index_order.html.twig', [
                'materials' => $materials,
                'equipments' => $equipments,
                'idinter' => $idinter,
                'centres' => $centers
            ]);
        }

        throw new NotFoundHttpException("Cette intervention n'existe pas !");

    }

    private function checkFilterIsTypeMat(string $filter): bool
    {

        $typeMateriel = $this->getDoctrine()->getRepository(Typemateriel::class)->findAll();

        if ($filter == 'none')
            return true;

        foreach ($typeMateriel as $value) {

            if ($filter == $value->getNom())
                return true;
        }

        return false;
    }

    private function orderValidate($materials, $quantities, $center): bool
    {

        foreach ($materials as $material){
            if (! $material && ! preg_match('/[a-zA-Z0-9-_\/]+/', $material))
                return false;
        }

        foreach ($quantities as $quantity){
            if (! is_int($quantity) && ! $quantity > 1 )
                return false;
        }

        if (! preg_match('/[a-zA-Z0-9-_\/]+/', $center))
            return false;

        return true;
    }
}
