<?php

namespace App\Controller;

use App\Entity\User;
use dateTimeInterface;
use App\Entity\Evaluation;
use App\Entity\Intervenir;
use App\Form\EvaluationType;
use App\Services\Encryption;
use App\Entity\FicheIndividuelleFMPA;
use App\Repository\EvaluationRepository;
use App\Repository\InterventionsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\FicheIndividuelleFMPARepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/evaluation")
 * @isGranted("ROLE_EQUIPIER")
 */
class EvaluationController extends AbstractController
{
    /**
     * @Route("/", name="evaluation_index", methods={"GET"})
     */
    public function index(EvaluationRepository $evaluationRepository): Response
    {
        return $this->render('evaluation/index.html.twig', [
            'evaluations' => $evaluationRepository->findAllEvaluations(),
        ]);
    }

    /**
     * @Route("/{id}/ajouter", name="evaluation_new", methods={"GET","POST"})
     */
    public function new(Request $request, Encryption $encryption, FicheIndividuelleFMPARepository $ficheIndividuelleFMPARepository, InterventionsRepository $interventionsRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $evaluation = new Evaluation();
        $nbHeuresFmpa = $ficheIndividuelleFMPARepository->getNbHoursFmpaByUser($decrypted_id);
        $nbInterventions = $interventionsRepository->counterOfInterventionsByAuthor($decrypted_id, date("Y"));
        $evaluation->setPersonnel($this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $decrypted_id]));
        $evaluation->setNbHeuresFmpa((int)$nbHeuresFmpa);
        $evaluation->setNbInterventions((int)$nbInterventions);
        $evaluation->setDate(new \DateTime(date("Y-m-d")));

        $form = $this->createForm(EvaluationType::class, $evaluation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evaluation);
            $entityManager->flush();

            return $this->redirectToRoute('user_show', ['id' => $encryption->encryptionValue((string)$evaluation->getPersonnel()->getId())], Response::HTTP_SEE_OTHER);
        }

        return $this->render('evaluation/new.html.twig', [
            'evaluation' => $evaluation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evaluation_show", methods={"GET"})
     */
    public function show(Encryption $encryption, EvaluationRepository $evaluationRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $evaluation = $evaluationRepository->findOneById($decrypted_id);

        return $this->render('evaluation/show.html.twig', [
            'evaluation' => $evaluation,
        ]);
    }

    /**
     * @Route("/{id}/excel", name="evaluation_convert", methods={"GET"})
     * @isGranted("ROLE_ADMIN")
     */
    public function convertDataToExcel(Request $request, Encryption $encryption, EvaluationRepository $evaluationRepository, $id): Response
    {

        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $evaluations = $evaluationRepository->findAllEvaluationsById($decrypted_id);

        $evaluationsCSV = "Date; Personnel; Filière; Centre; Nom du CTD; Nombre d'interventions; Nombre d'heures en FMPA; Compétence opérationnelle; Implication au sein de l'équipe; Rappel objectif " . date('Y') . "; Rappel des critères d'atteinte; Bilan; Objectif " . (date('Y') + 1) . "; Critères d'atteinte; Conditions de réussite et moyens de réalisation; Délais de réalisation; Commentaire;\n";

        foreach ($evaluations as $evaluation) {
       
            $evaluationsCSV .= $evaluation->getDate()->format('Y').';'.$evaluation->getPersonnel().';'.$evaluation->getFiliere()
            .';'.$evaluation->getCentre().';'.$evaluation->getCtd().';'.$evaluation->getNbInterventions()
            .';'.$evaluation->getNbHeuresFmpa().';'.$evaluation->getCompetenceOperationnelle().';'.$evaluation->getImplication()
            .';'.$evaluation->getObjectifAnneeEnCours().';'.$evaluation->getCritereAnneeEnCours()
            .';'.$evaluation->getBilan().';'.$evaluation->getObjectifAnneeSuivante().';'.$evaluation->getCritereAnneeSuivante()
            .';'.$evaluation->getConditionReussiteAnneeSuivante().';'.$evaluation->getDelaisRealisationAnneeSuivante()
            .';'.$evaluation->getCommentaire().";\n"
            ;
        }

        $evaluationsCSV = mb_convert_encoding($evaluationsCSV, 'UCS-2LE', 'utf-8');

        return new Response(
            $evaluationsCSV,
            200,
            [
                'Content-Type' => 'application/vnd.ms-excel',
                "Content-disposition" => "attachment; filename=Tutoriel.csv",
            ]
        );
    }

    /**
     * @Route("/{id}/modifier", name="evaluation_edit", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Encryption $encryption, EvaluationRepository $evaluationRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $evaluation = $evaluationRepository->findOneById($decrypted_id);

        $form = $this->createForm(EvaluationType::class, $evaluation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_show', ['id' => $encryption->encryptionValue((string)$evaluation->getPersonnel()->getId())], Response::HTTP_SEE_OTHER);
        }

        return $this->render('evaluation/edit.html.twig', [
            'evaluation' => $evaluation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evaluation_delete", methods={"POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Encryption $encryption, EvaluationRepository $evaluationRepository, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $evaluation = $evaluationRepository->findOneById($decrypted_id);

        if ($this->isCsrfTokenValid('delete' . $evaluation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evaluation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_show', ['id' => $encryption->encryptionValue((string)$evaluation->getPersonnel()->getId())], Response::HTTP_SEE_OTHER);
    }
}
