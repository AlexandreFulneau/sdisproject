<?php

namespace App\Controller;

use App\Repository\InterventionsRepository;
use App\Repository\RegarderRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
/**
 * Class HomeController
 * @package App\Controller
 *
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/accueil", name="home")
     * @isGranted("ROLE_EQUIPIER")
     */

    public function index(RegarderRepository $regarderRepository)
    {
        $newsNotView = $regarderRepository->findNewsByUserId($this->getUser());


        return $this->render('home/index.html.twig',['newsNotViewList'=>$newsNotView]);
    }
}
