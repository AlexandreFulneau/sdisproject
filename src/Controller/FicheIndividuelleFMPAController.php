<?php

namespace App\Controller;

use App\Entity\FicheIndividuelleFMPA;
use App\Form\FicheIndividuelleFMPAType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\FicheIndividuelleFMPARepository;
use App\Services\Encryption;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Route("/fmpa/fiche-individuelle")
 * @isGranted("ROLE_EQUIPIER")
 */
class FicheIndividuelleFMPAController extends AbstractController
{
    /**
     * @Route("/", name="fiche_individuelle_fmpa_index")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('fmpa_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/ajouter", name="fiche_individuelle_fmpa_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ficheIndividuelleFMPA = new FicheIndividuelleFMPA();
        $form = $this->createForm(FicheIndividuelleFMPAType::class, $ficheIndividuelleFMPA);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ficheIndividuelleFMPA);
            $entityManager->flush();
            $this->addFlash('add', 'Fiche individuelle ajoutée !');

            return $this->redirectToRoute('fiche_individuelle_fmpa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fiche_individuelle_fmpa/new.html.twig', [
            'fiche_individuelle_fmpa' => $ficheIndividuelleFMPA,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fiche_individuelle_fmpa_show", methods={"GET"})
     */
    public function show(Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $ficheIndividuelleFMPA = $this->getDoctrine()->getRepository(FicheIndividuelleFMPA::class)->findOneBy(['id' => $decrypted_id]);

        return $this->render('fiche_individuelle_fmpa/show.html.twig', [
            'fiche_individuelle_fmpa' => $ficheIndividuelleFMPA,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="fiche_individuelle_fmpa_edit", methods={"GET","POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $ficheIndividuelleFMPA = $this->getDoctrine()->getRepository(FicheIndividuelleFMPA::class)->findOneBy(['id' => $decrypted_id]);

        $form = $this->createForm(FicheIndividuelleFMPAType::class, $ficheIndividuelleFMPA);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('add', 'Fiche individuelle modifiée !');

            return $this->redirectToRoute('fiche_individuelle_fmpa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('fiche_individuelle_fmpa/edit.html.twig', [
            'fiche_individuelle_fmpa' => $ficheIndividuelleFMPA,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="fiche_individuelle_fmpa_delete", methods={"POST"})
     * @isGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Encryption $encryption, $id): Response
    {
        $decrypted_id = $encryption->decryptionValue($id);

        if ($decrypted_id == '0') {
            throw new NotFoundHttpException("La page n'existe pas ou plus");
        }

        $ficheIndividuelleFMPA = $this->getDoctrine()->getRepository(FicheIndividuelleFMPA::class)->findOneBy(['id' => $decrypted_id]);

        if ($this->isCsrfTokenValid('delete' . $ficheIndividuelleFMPA->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ficheIndividuelleFMPA);
            $entityManager->flush();

            $this->addFlash('delete', 'Fiche individuelle suprimée !');
        }

        return $this->redirectToRoute('fiche_individuelle_fmpa_index', [], Response::HTTP_SEE_OTHER);
    }
}
